$(document).ready(function(){


    $('.respostaSolicitacao span').click(function(){
        var data = "&solicitacaoGrupo="+ $(this).data('resposta')+ '-' + $(this).data('noti')+ '-' + $(this).data('idalunonoti');
        var url  = '.././index.php/Notificacao?p=1'+data;
        $("#loading").fadeIn('1').delay('900').fadeOut('1');
        $(".respostaSolicitacao").fadeOut('100');

        $.ajax({
            type: 'POST',
            url: url,
            data: data,
            dataType: 'json',
            success: function (data) {
                            
            },
            error: function(data){
            }
        });
    });


    $('.nova-notificacao').click(function(){
        var data = "notificacaosemilida=1";
        var url  = '.././index.php/Notificacao?p=1&'+data;
        $('.nova-notificacao span').css('display', 'none');
        $('.glyphicon-bell').css('color', '#025e63 !important');
        $('.glyphicon-bell').removeClass('nova-notificacao');
        $('.campo-notificacao').css('background-color', '#147f85');
        $.ajax({
            type: 'POST',
            url: url,
            data: data,
            dataType: 'json',
            success: function (data) {
                            
            },
            error: function(data){
            }
        });
    });


    $('.notificacoes li').click(function(){
        var data = "notificacaolida="+ $(this).data('idnotificacao');
        var url  = '.././index.php/Notificacao?p=1&'+data;
        $(this).css('background-color','white');

        $.ajax({
            type: 'POST',
            url: url,
            data: data,
            dataType: 'json',
            success: function (data) {
                            
            },
            error: function(data){
            }
        });
    });

    $('.row span').hover(function(){
        var data = "notificacaolida="+ $(this).data('id');
        var url  = '.././index.php/Notificacao?p=1&'+data;
        $(this).find('.notificacao').css('background-color','#FFF');

        $.ajax({
            type: 'POST',
            url: url,
            data: data,
            dataType: 'json',
            success: function (data) {
                            
            },
            error: function(data){
            }
        });
    });
});
