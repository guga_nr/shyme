function alterarImagemShow(){
	$('.alterar-imagem').fadeIn('200');
}

function alterarImagemHide(){
	$('.alterar-imagem').fadeOut('200');
}
var url_atual = location.search.slice(1);
var items = url_atual.split("&");
var textant = '';
$(document).ready(function(){

    $("img").click(function() {
        if($(this).attr("alt") !== 'perfil-foto' && $(this).attr("alt") !== 'logo'){
          var url = $(this).attr("src");
          $("#modalimagem img").attr("src", url);
          $("#modalimagem").modal("show");
        }
    });

    $("#perfil-foto").hover(function() {
        alterarImagemShow();
    }, function() {
        alterarImagemHide();
    });

    $(".login .lista-fake li img").hover(function() {
        var textant = $('.nome-alunofake').text();
        var nome = $(this).attr('alt');
        $('.nome-alunofake p').text(nome);

    }, function() {
        $('.nome-alunofake p').text(textant);
        
    });

    $(".login .lista-fake li img").click(function() {
        $(".login .lista-fake li img").css('filter' , 'grayscale(100%)');
        $(this).css('filter' , 'grayscale(0%)');
        textant = $(this).attr('alt');
        idfake = $(this).data('idalunofake');
        $('#loginfake').css('opacity', '1');
        $('#loginfake').css('cursor', 'pointer');
        $("#idfake").val(idfake) ;

    });




    $(".filetoUpload").on('change', function () {
        if (typeof (FileReader) != "undefined") {
     
            var image_holder = $(".image-holder");
            image_holder.empty();
     
            var reader = new FileReader();
            reader.onload = function (e) {
                $("<img />", {
                    "src": e.target.result,
                    "class": "thumb-image"
                }).appendTo(image_holder);
            }
            image_holder.show();
            reader.readAsDataURL($(this)[0].files[0]);
        } else{
            alert("Este navegador nao suporta FileReader.");
        }
    });

    $('.ic-grupo input').click(function(event) {
        if ($("#publico").is(":checked")){
        	$("#publico").parent().css('background-color', '#147f85');
        	$("#publico").parent().css('color', '#FFF');
        	$("#privado").parent().css('background-color', '#FFF');
        	$("#privado").parent().css('color', '#147f85');
        } 
        if ($("#privado").is(":checked")){
        	$("#privado").parent().css('background-color', '#147f85');
        	$("#privado").parent().css('color', '#FFF');
        	$("#publico").parent().css('background-color', '#FFF');
        	$("#publico").parent().css('color', '#147f85');
        } 
    });

    $('#criar-grupo').click(function(e) {
        if ($('#nmgrupo').val() == '') {
            e.preventDefault();
            alert("Dê um nome para o grupo");
        }
        if ($('#dsgrupo').val() === '') {
            e.preventDefault();
            alert("Descreva o grupo");
        }
        if (!$('#privado').is(':checked') && !$('#publico').is(':checked') ) {
            e.preventDefault();
            alert("Selecionie a privacidade do grupo");
        }

    });

    $('.responderperfil').click(function(e){
            if ($('.ds_resposta').text() !== ''){
                e.preventDefault();
                var ds = $('.ds_resposta').val();
                var id = $('#id_duvida').val();
                var data = "&submitR=1"+"&txt_content_post="+ds+"&id_duvida="+id;
                var url  = '.././index.php/Duvida/outside?'+data;
                    $("#loading").css('display', 'block');
            
                $.ajax({
                    type: 'POST',
                    url: url,
                    data: data,
                    dataType: 'json',
                    success: function (data) {
                                    
                    },
                    error: function(data) {
                        $('.todas-respostas-modal').load(window.location.href  + ' .load-respostas-modal');
                        $("#loading").css('display', 'none');
                        $(".ds_resposta").val('');
                    }
                });
            }else {
                e.preventDefault();
                alert("digite algo para postadr");
            }      
    
    });

    $(".escolher").click(function() {
        url = ".././index.php/duvida?"+items[0];
        data = "&id=" + $(this).data("idrespostaescolha")
        url = url+data;
        $.ajax({
            type: 'POST',
            url: url,
            data: data,
            dataType: 'json',
            success: function (data) {
                            
            },
            error: function(data) {
                // $('.todas-respostas-modal').load(window.location.href  + ' .campo-resposta');
                // $("#loading").css('display', 'none');
                // $(".ds_resposta").val('');
                window.location.reload(); 
            }
        });
    });


    $
});