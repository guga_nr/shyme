var add_membro_id = '0';
var url_atual = location.search.slice(1);
var items = url_atual.split("&");
        
$(document).ready(function(){
   $("#upload").click(function(event){
        var file = $('#material_arquivo').val();
        var texto = $('textarea#txt_content_material').val();
        if(file === ''){
            alert('Você precisa subir algum arquivo para postar um material!');
            event.preventDefault();
        }if(texto === ''){        
            alert('Não deixe a postagem vazia =/');
            event.preventDefault();
        }
    });

   $("#duvida").click(function(event){
        var file = jQuery("input[name=moeda_duvida]:checked").val();
        var texto = $('textarea#txt_content_post_duvida').val();
        if(file === undefined){
            alert('Você precisa escolher uma quantidade de pontos para postar uma dúvida!');
            event.preventDefault();
        }else if(texto === ''){        
            alert('Não deixe a postagem vazia =/');
            event.preventDefault();
        }
    });

   $("#comunicado").click(function(event){
        var texto = $('textarea#txt_content_post').val();
         if(texto === ''){        
            alert('Não deixe a postagem vazia =/');
            event.preventDefault();
        }
    });

    $('#alterar-info').click(function(){
        var privacidade = $('#icprivado').find(":selected").val();
        var nome = $('#nome_grupo').val();
        var ds = $('#ds_grupo').val();
        var data = "&nomegru="+nome+"&dsgru="+ds+"&privacidade="+privacidade;
        var url  = '.././index.php/Grupo?'+items[0]+'&'+items[1]+data;
    
        $.ajax({
            type: 'POST',
            url: url,
            data: data,
            dataType: 'json',
            success: function (data) {
                            
                }
            });
    
        $('#nomeg').text(nome);
        $('#descricao').text(ds);  
    });

    $('#solicitacaoGrupo').click(function(){
        var data = "&solicitacaoGrupo=1";
        var url  = '.././index.php/Grupo?'+items[0]+'&'+items[1]+data;
    
        $.ajax({
            type: 'POST',
            url: url,
            data: data,
            dataType: 'json',
            success: function (data) {
                            
            },
            error: function(data){
                alert("Sua solicitação foi enviada!");
                $("#solicitacaoGrupo").fadeOut('300');
            }
        });
    });

    


    
    $('.icone-mini').click(function(event) {
        alert('message?: DOMString');
        /* Act on the event */
    });
    // $( 'form#responder' ).submit( function( e ) {
    //     alert('message?: DOMString');
    //     var url  = '.././index.php/Duvida?'+items[0];
    //     var form = $('#responder')[0];
    //     alert(form);
    //     $.ajax( {
    //       url: url,
    //       type: 'POST',
    //       data: new FormData( form ),
    //       processData: false,
    //       mimeType: "multipart/form-data",
    //       contentType: false
    //     });
    //     e.preventDefault();
    // });
    
    $('#sairGrupo').click(function(event){
        
        var r = confirm('Deseja mesmo sair do grupo?!');
        if (r == true) {
            var data = "sairGrupo=1"; 
            var url  = '.././index.php/Grupo?'+items[0]+'&'+items[1]+'&'+data;
            $.ajax({
                type: 'POST',
                url: url,
                data: data,
                dataType: 'json',
                success: function (data) {
                    alert('Ocorreu algum erro...Tente novamente mais tarde');
                },
                error: function(data){
                    location.reload();

                }
            });
        }
    });
    
    $('#excluirGrupo').click(function(event){
        
        var r = confirm('Deseja mesmo excluir este grupo?!');
        if (r == true) {
            var data = "excluirGrupo=1"; 
            var url  = '.././index.php/Grupo?'+items[0]+'&'+items[1]+'&'+data;
            $.ajax({
                type: 'POST',
                url: url,
                data: data,
                dataType: 'json',
                success: function (data) {
                    alert('Ocorreu algum erro...Tente novamente mais tarde');
                },
                error: function(data){
                    location.reload();

                }
            });
        }
    });
    
    $('#name_user').keyup(function(){
        var type = '&qType=usuario';
        var dado = 'q='+$('#name_user').val()+type;
        var lista ='';
        $.ajax({
            type: 'POST',
            url: '.././index.php/SearchControl',
            data: dado,
            dataType: 'json',
            success: function(data){
                if(data){
                    var nome = "";
                    var a = 0;
                    var i;
                    $('#pessoas-ul').empty();
                    for(i=0;i<data.length;i++){
                      lista = lista +  "<li  id='add-"+data[i].cd_matricula+"' onclick='listar("+data[i].cd_matricula+")' style='margin-top: 10px;cursor: pointer;'><img class='image-add' src='http://localhost/shyme/assets/"+data[i].img_aluno+"' style='width:40px;' alt='"+data[i].cd_matricula+"'>"+data[i].nm_aluno+"</li>";
                    }
                    $('#pessoas-ul').html(lista);
                }
            }
       });      
   });

    $("#btn-add-membro").click(function(){
        
        var data = "&addmembro="+add_membro_id;
        var url  = '.././index.php/Grupo?'+items[0]+'&'+items[1]+data;
        $("#loading").css('display', 'block');
        $.ajax({
            type: 'POST',
            url: url,
            data: data,
            dataType: 'json',
            success: function (response) {
                
            },
            error: function (data) {
                $("#loading").css('display', 'none');
                $( "#lista-membros-grupo" ).load(url + ' #load-membros');
                
            }
        });
    });
    

    $('.objeto-tornar-adm label').click(function() {
        
        var cdAdm = $(this).data("idadm");
        var r = confirm('Deseja torna-lo administrador do grupo?');
        if (r == true) {

        $("#loading").css('display', 'block');  
        var data = "addadm="+cdAdm; 
        var url  = '.././index.php/Grupo?'+items[0]+'&'+items[1]+'&'+data;
        $.ajax({
            type: 'POST',
            url: url,
            data: data,
            dataType: 'json',
            success: function (data) {
                alert('Ocorreu algum erro...Tente novamente mais tarde');
            },
            error: function(data){
                $("#loading").css('display', 'none');       
                $("#possivel-adm-"+cdAdm).css('display', 'none');
                $( "#lista-membros-grupo" ).load(url + ' #load-membros');

            }
        });
        }
    });

    $("#busca-possivel-adm").click(function(event) {
        event.preventDefault();
        var busca = $('#campo-busca-adm').val().toLowerCase();
        // $( ".membro-objeto" ).find( ".name_usr" ).css( "background-color", "red" );
        $(".objeto-tornar-adm").css('display', 'none');
        $(".busca-nome-adm:contains('"+busca+"')").parent().parent().parent().css("display", "inline-block");
        
    });

    $("#busca-membro").click(function(event) {
        if ($("#busca-membro").attr("data-statusbuscamembro") == "on") {
            var busca = $('#campo-busca-membro').val().toLowerCase();
            // $( ".membro-objeto" ).find( ".name_usr" ).css( "background-color", "red" );
            $(".membro-objeto").css('display', 'none');
            $(".busca-nome-membro:contains('"+busca+"')").parent().parent().css("display", "inline-block");
            $("#limpa-busca").css('opacity', '1');
        }else{
            $('#campo-busca-membro').css('opacity','1');
            if($(window).width() > 1000){
                $('#campo-busca-membro').css('width','50%');
            }
            else{
                $('#campo-busca-membro').css('width','90%');
            }
            $('#campo-busca-membro').css('border-radius', '15px');
            $('#campo-busca-membro').css('padding','5px');
            $("#limpa-busca").css('opacity', '1');
            $('#campo-busca-membro').focus();
            $("#busca-membro").attr("data-statusbuscamembro", "on");
        }
    });

    $("#campo-busca-membro").keypress(function(e) {
        if(e.which == 13) 
             $("#busca-membro").click();
    });

    $("#limpa-busca").click(function(event) {
        $('#campo-busca-membro').val('');
        $(".membro-objeto").css('display', 'inline-block');
        $("#limpa-busca").css('opacity', '0');
        $('#campo-busca-membro').css('width','0%');
        $('#campo-busca-membro').css('opacity','0');
        $('#campo-busca-membro').css('border','0');
        $('#campo-busca-membro').css('padding','0');
            $("#busca-membro").attr("data-statusbuscamembro", "off");
        
    });

    $('#imagem_post').click(function(){
        $('#form_imagem_post').click();
    });

    $('#form_imagem_post').change(function(){
        $('#imagem_post').parent().css('background', "#61b8bd");
        $('#imagem_post').parent().css('display', "table");
    });


    $('#material_post').click(function(){
        $('#form_material_post').click();
    });

    $('#form_material_post').change(function(){
        $('#material_post').parent().css('background', "#61b8bd");
        $('#material_post').parent().css('display', "table");
    });


    $('.moeda_duvida input').click(function(event) {
        if ($("#cinco").is(":checked")){
            $("#cinco").parent().css('background-color', '#147f85');
            $("#cinco").parent().css('color', '#FFF');
            $("#dez").parent().css('background-color', '#FFF');
            $("#dez").parent().css('color', '#147f85');
            $("#quize").parent().css('background-color', '#FFF');
            $("#quize").parent().css('color', '#147f85');
        } 
        if ($("#dez").is(":checked")){
            $("#dez").parent().css('background-color', '#147f85');
            $("#dez").parent().css('color', '#FFF');
            $("#cinco").parent().css('background-color', '#FFF');
            $("#cinco").parent().css('color', '#147f85');
            $("#quize").parent().css('background-color', '#FFF');
            $("#quize").parent().css('color', '#147f85');
        } 
        if ($("#quinze").is(":checked")){
            $("#quinze").parent().css('background-color', '#147f85');
            $("#quinze").parent().css('color', '#FFF');
            $("#cinco").parent().css('background-color', '#FFF');
            $("#cinco").parent().css('color', '#147f85');
            $("#dez").parent().css('background-color', '#FFF');
            $("#dez").parent().css('color', '#147f85');
        } 
    });


});


function comun(){
    $(".form-posts").slideDown('slow');
    $(".form-posts").css('display', 'table');
    $(".upload_form").css('display', 'none');
    $(".moeda_duvida").css('display', 'none');
    $(".form-acao button").attr('name', 'comunicado');
    $(".form-acao button").text('Postar Comunicado');
    
    $("#comun").css('background-color', '#147f85');
    $("#comun").css('color', '#FFF');
    $("#duvid").css('background-color', '#FFF');
    $("#duvid").css('color', '#147f85');
    $("#mater").css('background-color', '#FFF');
    $("#mater").css('color', '#147f85');
}
function duvid(){
    $(".form-posts").slideDown('slow');
    $(".form-posts").css('display', 'table');
    $(".upload_form").css('display', 'none');
    $(".moeda_duvida").css('display', 'block');
    $(".form-acao button").attr('name', 'duvida');
    $(".form-acao button").text('Postar Duvida');

    $("#duvid").css('background-color', '#147f85');
    $("#duvid").css('color', '#FFF');
    $("#comun").css('background-color', '#FFF');
    $("#comun").css('color', '#147f85');
    $("#mater").css('background-color', '#FFF');
    $("#mater").css('color', '#147f85');
}

function mater(){
    $(".form-posts").slideDown('slow');
    $(".form-posts").css('display', 'table');
    $(".upload_form").css('display', 'block');
    $(".moeda_duvida").css('display', 'none');
    $(".form-acao button").attr('name', 'upload');
    $(".form-acao button").text('Postar Material');

    $("#mater").css('background-color', '#147f85');
    $("#mater").css('color', '#FFF');
    $("#duvid").css('background-color', '#FFF');
    $("#duvid").css('color', '#147f85');
    $("#comun").css('background-color', '#FFF');
    $("#comun").css('color', '#147f85');
}

function preencherGrupoInfo() {
    var nm = document.getElementById("nomeg").innerHTML;
    document.getElementById("nome_grupo").value = nm;
    var ds = document.getElementById("descricao").innerHTML;
    document.getElementById("ds_grupo").innerHTML = ds;
}

function listar(id){
    var imagem = $('.image-add[alt="'+id+'"').attr('src');
    var nome = $('#add-'+id).text();
    var content = "<div style='display:inline-block'><div class='matched' id='remove-"+id+"'><img src='"+imagem+"' id='"+id+"' style='height:30px;width: 30px; margin-right:10px'><p>"+nome+"</p><span onclick='removerLista("+id+")'>-</span></div></div>";
    $('#matched').html($('#matched').html()+ content);
    add_membro_id = add_membro_id + ',' + id;

}
function removerLista(id){
    $("#remove-"+id).parent().html('');
    add_membro_id = add_membro_id.replace(','+id, '');
}
function removerMembro(idAluno){
    
    var r = confirm('Deseja mesmo este membro?!');
    if (r == true) {
        var data = "removerMembro="+idAluno; 
        var url  = '.././index.php/Grupo?'+items[0]+'&'+items[1]+'&'+data;
        $.ajax({
            type: 'POST',
            url: url,
            data: data,
            dataType: 'json',
            success: function (data) {
                alert('Ocorreu algum erro...Tente novamente mais tarde');
            },
            error: function(data){
                $("#aluno-"+idAluno).fadeOut('slow');
            }
        });
    }
}

function downloadporra(){
        data = 'download_material';
        var url  = '.././index.php/Grupo?'+items[0];
        $.ajax({
            type: 'POST',
            url: url,
            data: data,
            dataType: 'json',
            success: function (url) {
                            
            },
            error: function(url) {
            }
        });
}