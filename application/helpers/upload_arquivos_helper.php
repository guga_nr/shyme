<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

function extensaoArquivo($extensao){ 
    if($extensao == 'text/plain')
        return '.txt';
    if($extensao == 'image/png')
        return '.png';
    if($extensao == 'application/zip')
        return '.zip';
    if($extensao == 'image/jpeg')
        return '.jpg';
    if($extensao == 'application/vnd.openxmlformats-officedocument.wordprocessingml.document')
        return '.xml';
    if($extensao == 'image/vnd.adobe.photoshop')
        return '.psd';
    if($extensao == 'application/octet-stream')
        return '.sql';
    if($extensao == 'text/html')
        return '.html';
    if($extensao == 'application/pdf')
        return '.pdf';
    if($extensao == 'application/vnd.ms-excel')
        return '.csv';
    if($extensao == 'text/php')
        return '.php';
}

function upload($file,$pasta){
    $db = new Conn();
    $target_dir = "/img".$pasta;
    $target_file = $target_dir . basename($file["name"]);
    $uploadOk = 1;
    $imageFileType = $file["type"];
    if(isset($_POST["submit"])) {
        $check = getimagesize($file["tmp_name"]);
        if($check !== false) {
            $uploadOk = 1;
        } else {
            $uploadOk = 0;
        }
    }   $extensao = $this->extensaoArquivo($file['type']);
        $target_file = str_replace($extensao, "", $target_file);
    if (file_exists($target_file)) {
        $target_file = $target_file.date("m.d.y");
    }
     $target_file = $target_file.$extensao;
    if ($uploadOk == 0) {
        echo "<script> alert('Não foi possível subir o arquivo =/'); </script>";
    } else {
        if (move_uploaded_file($file["tmp_name"], $target_file)) {
            if($pasta == "/perfil/")
                $db->uploadFotoPerfil($_SESSION['id'], $target_file);
            if($pasta == "/comunicado/")
                return $target_file;
            if($pasta == "/duvida/")
                return $target_file;
            if($pasta == "/material/")
                return $target_file;
            if($pasta == "/material/upload")
                return $target_file;
            if($pasta == "/grupo/")
                return $target_file;
            if($pasta == "/spotted/")
                return $target_file;
        }else{
            echo "<script> alert('Não foi possível subir o arquivo =/'); </script>";
        }
    }
}