<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

function extensaoArquivo($extensao){ 
    if($extensao == 'text/plain')
        return '.txt';
    else if($extensao == 'image/png')
        return '.png';
    else if($extensao == 'application/zip')
        return '.zip';
    else if($extensao == 'image/jpeg')
        return '.jpeg';
    else if($extensao == 'image/jpg')
        return '.jpg';
    else if($extensao == 'application/vnd.openxmlformats-officedocument.wordprocessingml.document')
        return '.xml';
    else if($extensao == 'image/vnd.adobe.photoshop')
        return '.psd';
    else if($extensao == 'application/octet-stream')
        return '.sql';
    else if($extensao == 'text/html')
        return '.html';
    else if($extensao == 'application/pdf')
        return '.pdf';
    else if($extensao == 'application/vnd.ms-excel')
        return '.csv';
    else if($extensao == 'text/php')
        return '.php';
}

function verificaExtensao($extensao, $postagem){ 
    if ($postagem != '/material/upload/' && ($extensao == 'png' || $extensao == 'jpg' || $extensao == 'gif' || $extensao == 'jpeg')) {
        return true;
    }else if($postagem == '/material/upload/'){
        return true;
    }else{
        return false;
    }
}

function upload($file,$pasta){
    // if ($pasta != '/material/upload' && $file["size"] > 2000000) {
    //     alert('As imagens tem que ter no máximo 2MB'); 
    //     return false;
    // }
    // if ($pasta == '/material/upload' && $file["size"] > 200000000) {
    //     alert('Os arquivos tem que ter no máximo 20MB');
    //     return false;
    // }

    $db = new Conn();
    $caminho = str_replace(__DIR__, 'application/controllers/', '') . "assets/";
    // var_dump($file);
    // alert($file['name']);
    $file["name"] =str_replace(' ', '-',  $file["name"]);
    $nome_extensao = explode('.', $file["name"]);
    $target_dir =  $caminho."img".$pasta;
    $target_file = $target_dir . md5($nome_extensao[0].date("Y-d-m H:i:s")) . '.' .$nome_extensao[sizeof($nome_extensao) - 1];
    $uploadOk = 1;

    if(verificaExtensao($nome_extensao[sizeof($nome_extensao) - 1],$pasta)){


        if ($uploadOk == 0) {
            echo "<script> alert('Não foi possível subir o arquivo =/'); </script>";
        }else {
            if (move_uploaded_file($file["tmp_name"], $target_file)) {
                $target_file = str_replace($caminho,'',$target_file);
                if($pasta == "/perfil/"){
                    $db->uploadfotoperfil($_SESSION['id'], $target_file);
                    $_SESSION['imagem'] = $target_file;
                    header("Refresh:0"); 
                }
                if($pasta == "/parceiros/"){
                    $db->uploadfotoparceiro($_SESSION['id_parceiro'], $target_file);
                 	$_SESSION['imagem_parceiro'] = $target_file;
                }

                if($pasta == "/comunicado/")
                    return str_replace(asset_url() , '' , $target_file);
                if($pasta == "/parceiros/postagens/")
                    return str_replace(asset_url() , '' , $target_file);
                if($pasta == "/duvida/")
                    return str_replace(asset_url() , '' , $target_file);
                if($pasta == "/duvida/resposta/")
                    return str_replace(asset_url() , '' , $target_file);
                if($pasta == "/material/")
                    return str_replace(asset_url() , '' , $target_file);
                if($pasta == "/material/upload/")
                    return str_replace(asset_url() , '' , $target_file);
                if($pasta == "/grupo/foto/")
                    return str_replace(asset_url() , '' , $target_file);
                if($pasta == "/grupo/"){
                    $db->uploadfotogrupo($_GET['id'], $target_file);
                    header("Refresh:0"); 
                }
                if($pasta == "/spotted/")
                    return str_replace(asset_url() , '' , $target_file);
            }else{
                echo "<script> alert('Não foi possível subir o arquivo =/'); </script>";
            }
        }
    }else{
        echo "<script> alert('Esse arquivo não é suportado.'); </script>";
    }
}

function alert($text){
    echo "<script> alert('".$text."'); </script>";
}