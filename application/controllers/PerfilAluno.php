<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once 'Usuario.php';
require_once 'Grupo.php';
require_once 'Notificacao.php';
class PerfilAluno extends CI_Controller {


	
	public function index()
	{
            if(isset($_SESSION['id'])){
                $this->load->helper('url');
           //     if($_SESSION['primeira']==1){
        //       echo "bem-vindo";
          //      }

                
                
                $notificacao = new Notificacao();
                $data['notificacao']=  $notificacao->listarNotificacao($_SESSION['id']);
                
                if (isset($_GET['ldp'])){
                    $perfil_aluno = $_GET['ldp'];
                    $db = new Conn();
                    $grupo = new Grupo();
                    $data['perfil']= $db->listarInfoPerfil($perfil_aluno);
                    $data['grupoPrimario']= $db->listarGruposPrimariosPerfil($perfil_aluno);
                    $data['grupoSecundario']= $db->listarGruposSecundariosPerfil($perfil_aluno);
                    $data['postPerfil']= $db->listarPostagemPerfil($_SESSION['id']);

                    $user = new Usuario();
                $data['moeda']=  $user->listarMoeda($_SESSION['id']);
                $data['ponto']=  $user->listarPontuacao($_SESSION['id']);

                }
                $this->load->view('perfil-aluno',$data);

            }else{
            header('Location:Login');
        }
                
	} 
}

