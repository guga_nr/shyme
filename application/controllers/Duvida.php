<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once 'Usuario.php';
require_once 'Conn.php';
require_once 'Notificacao.php';
require_once 'upload_arquivos.php';
class Duvida extends CI_Controller {
    
    public function index()
    {       
        $db = new Conn();

        $data['resultadoP'] = $db->listarumapostagem($_GET['id']);
        $data['resultadoR'] = $db->listarresposta($_GET['id']);
        $data['grupoduvida'] = $db->grupoduvida($_GET['id']);

        $data['nome_da_pagina']=  "Dúvida - Shyme";
                
        $db = new Conn();
            $data['semanal']= $db->rankingSemanalPessoal($_SESSION['id']);
            $data['mensal']= $db->rankingMensallPessoal($_SESSION['id']);
            $data['total']= $db->rankingTotalPessoal($_SESSION['id']);
                $moeda= $db->listarapenasmoeda($_SESSION['id'])->fetchAll();
                $_SESSION['moeda'] = $moeda[0][0];

        $notificacao = new Notificacao();
            $data['notificacao']=  $notificacao->listarNotificacao($_SESSION['id'],0);
            $data['paginacao']=  $notificacao->listarnotificacaototal($_SESSION['id']);
            $data['novas']=  $notificacao->novasnotificacoes($_SESSION['id']);
            $data['pontuacao']=  $db->listarpontuacoes($_SESSION['id']);


        $user = new Usuario();
            $data['moeda']=  $user->listarMoeda($_SESSION['id']);
            $data['ponto']=  $user->listarPontuacao($_SESSION['id']);


        $resId = $db->listarUmaPostagem($_GET['id']); //Pegando o ALUNO_GRUPO para
        $resId2 = $resId->fetch(PDO::FETCH_ASSOC);    // verificar se o Usuario é o dono da Pergunta
        
        $donoDuvida = $db->verificarDonoDuvida($resId2['cd_aluno_grupo']); //Usando o Aluno Grupo para 
        $idAluno = $donoDuvida->fetch(PDO::FETCH_ASSOC); // descobrir o CD_MATRICULA de quem postou

        if($idAluno['cd_matricula'] == $_SESSION['id']){  //Verificando se o CD_MATRICULA bate com quem ta logado
            $data['dono'] = 1;
        } else {
            $data['dono'] = 0;
        }

        $verEscolha = $db->listarResposta($_GET['id']); //Pega lista de resultados da Resposta
        foreach($verEscolha as $ver){                   //Para verificar se uma delas ja foi
            if($ver['ic_resposta'] == 1){               //Escolhida como resposta definitiva
                $data['escolha'] = 1;                   //Se for o caso, armazena numa variavel
                break;                                  //E envia pra view
            } else {
                $data['escolha'] = 0;
            }
        }
        
        $this->load->helper('url');
        $this->load->view('duvida',$data);

        if(isset($_POST['submitR'])){
            $imagem = '';
            if(isset($_FILES['img_resposta']) && !empty($_FILES['img_resposta']['name'])){
                $imagem = upload($_FILES['img_resposta'],"/duvida/resposta/");
            } 
        	$db->adicionarResposta($_POST['txt_content_post'],$_SESSION['id'],$imagem,$_GET['id']);  
            $db->enviarNotificacao($_SESSION['id'],0,$_GET['id'],4);
            header("Refresh:0");
        }

        if(isset($_POST['id'])){
            $idResp = $_POST['id'];
            $db->escolherResposta($idResp);
        }
    }

    public function outside(){
        $db = new Conn();
        if(isset($_POST['submitR'])){
            $imagem = '';
            if(isset($_FILES['img_resposta']) && !empty($_FILES['img_resposta']['name'])){
                $imagem = upload($_FILES['img_resposta'],"/duvida/resposta/");
            } 
            $db->adicionarResposta($_POST['txt_content_post'],$_SESSION['id'],$imagem,$_POST['id_duvida']);  
            $db->enviarNotificacao($_SESSION['id'],0,$_GET['id'],4);
            header("Refresh:0");
        }
    }
}
