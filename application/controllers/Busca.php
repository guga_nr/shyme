<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once 'Usuario.php';
require_once 'Grupo.php';
require_once 'upload_arquivos.php';
require_once 'Notificacao.php';
class Busca extends CI_Controller {


	public function index(){
        if(isset($_SESSION['id'])){
            $this->load->helper('url');

            $data['nome_da_pagina']=  "Busca - Shyme";
            
            $db = new Conn();
            $data['semanal']= $db->rankingSemanalPessoal($_SESSION['id']);
            $data['mensal']= $db->rankingMensallPessoal($_SESSION['id']);
            $data['total']= $db->rankingTotalPessoal($_SESSION['id']);
                $moeda= $db->listarapenasmoeda($_SESSION['id'])->fetchAll();
                $_SESSION['moeda'] = $moeda[0][0];

            $notificacao = new Notificacao();
            $data['notificacao']=  $notificacao->listarNotificacao($_SESSION['id'],0);
            $data['paginacao']=  $notificacao->listarnotificacaototal($_SESSION['id']);
            $data['novas']=  $notificacao->novasnotificacoes($_SESSION['id']);
            $data['pontuacao']=  $db->listarpontuacoes($_SESSION['id']);

            $data['pessoa'] = 'Nenhum resultado para essa busca.';
            $data['parceiro'] = 'Nenhum resultado para essa busca.';
            $data['grupo'] = 'Nenhum resultado para essa busca.';
            if(isset($_GET['q'])){
                $data['pessoa'] = $db->pesquisaPessoa($_GET['q']);
                $data['grupo'] = $db->pesquisaGrupo($_GET['q']);
                $data['parceiro'] = $db->pesquisaParceiro($_GET['q']);
                $data['nome_da_pagina']=  "Busca - ".$_GET['q'];
            }
            $this->load->view('busca',$data);
        }else{
            header('Location:/Login');
        }
    }
}
