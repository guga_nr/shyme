<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once 'Usuario.php';
require_once 'Conn.php';
require_once 'Notificacao.php';
require_once 'upload_arquivos.php';
class Manual extends CI_Controller {



	public function index(){
        $this->load->helper('url');
            
        $data['nome_da_pagina']=  "Manual - Shyme";
            
        $db = new Conn();
            $data['semanal']= $db->rankingSemanalPessoal($_SESSION['id']);
            $data['mensal']= $db->rankingMensallPessoal($_SESSION['id']);
            $data['total']= $db->rankingTotalPessoal($_SESSION['id']);
                $moeda= $db->listarapenasmoeda($_SESSION['id'])->fetchAll();
                $_SESSION['moeda'] = $moeda[0][0];

        $notificacao = new Notificacao();
            $data['notificacao']=  $notificacao->listarNotificacao($_SESSION['id'],0);
            $data['paginacao']=  $notificacao->listarnotificacaototal($_SESSION['id']);
            $data['novas']=  $notificacao->novasnotificacoes($_SESSION['id']);
            $data['pontuacao']=  $db->listarpontuacoes($_SESSION['id']);


        $user = new Usuario();
            $data['moeda']=  $user->listarMoeda($_SESSION['id']);
            $data['ponto']=  $user->listarPontuacao($_SESSION['id']);
            $this->load->view('manual', $data);

    }
}
