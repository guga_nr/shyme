<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once 'Usuario.php';
require_once 'upload_arquivos.php';
require_once 'Conn.php';
class Login extends CI_Controller {

    public function index(){
        if(isset($_SESSION['id'])){
                header('Location:'.base_url().'index.php/Perfil');
        }
                
        $conn = new Conn();
        
        $data['nome_da_pagina']=  "Login - Shyme";

        $data['tela'] = 'login';
        $data['alunosfake'] = $conn->todosalunoslogin();
            $this->load->helper('url');
              $this->load->view('login',$data);
        if(isset($_POST['submit'])){
            $Usuario = new Usuario();
            if($Usuario->logarConta($_POST['txt_login'])==1){
               // $Usuario->criarPerfil($_SESSION['id']);
                header('Location:'.base_url().'index.php/Perfil');
            }else{
                echo "Usuário ou senha errado(s)";
            }
        }
    }

    public function siga(){
        if(isset($_SESSION['id'])){
                header('Location:'.base_url().'index.php/Perfil');
        }
                
            
            if(isset($_SESSION['id']))
            $data['nome_da_pagina']=  "Login(SIGA) - Shyme". $_SESSION['id'];
            else
            $data['nome_da_pagina']=  "Login(SIGA) - Shyme";

            $data['tela'] = 'siga';
            $this->load->helper('url');
            $this->load->view('login',$data);
            if(isset($_POST['submit'])){

                $data = array("login" => $_POST['txt_login'], "password" => $_POST['txt_senha'] );
                $data_string = json_encode($data);  
                $ch = curl_init('http://razorblade.me:7070/sigatec/Siga/auth');                       
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                    
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                   
                curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                       
                    'Content-Type: application/json',                                                 
                    'Content-Length: ' . strlen($data_string))
                );
                $result = curl_exec($ch);
                $result2 = json_decode($result, TRUE);
                if(isset($result2['aluno']['ra'])){
                    $Usuario = new Usuario();
                    $conn = new Conn();
                        $verifica = $conn->prepare($result2['aluno']['ra']);
                      if($verifica->rowCount() == 0){
                                $curso = 'ADS';
                            $nomeAluno = ucwords(strtolower($result2["aluno"]["nome"]));
                            $raAluno = $result2["aluno"]["ra"];
                            // $curso = ucwords(strtolower(str_replace('Tecnologia em ', '', $result2["aluno"]["curso"])));
                            if ($result2["aluno"]["curso"] == 'Tecnologia em Análise e Desenvolvimento de Sistemas') {
                                $curso = 'ADS';
                            }   
                            $perfil = substr($nomeAluno, 0, 4).substr($raAluno, 0, 4).substr($raAluno, 4, 4) ;
                            $Usuario->criarPerfil($raAluno, $nomeAluno, $curso, $perfil,$result2["aluno"]["rg"]);
                            header('Location:'.base_url().'index.php/Perfil');
                        }else{
                            
                    
                    $Usuario->logarConta($result2['aluno']['ra']);

                    if($Usuario->logarConta($_POST['txt_login'])==1)
                        header('Location:'.base_url().'index.php/Perfil');
                        header('Location:'.base_url().'index.php/Perfil');
                        }
                }else{
                    echo "Usuário ou senha errado(s)";
                }
        }
    }

    public function parceiros(){
        if(isset($_SESSION['id_parceiro'])){
            header('Location:'.base_url().'index.php/Parceiros');
        }else{

            $this->load->helper('url');

            $data['tela'] = 'parceiros';
            $data['nome_da_pagina']=  "Login(Parceiros) - Shyme";
            $this->load->view('login',$data);

            if(isset($_POST['submit'])){
                $conn = new Conn();
                $valorPreparado = $conn->prepareparceiros($_POST['txt_login'],$_POST['txt_senha']);
                if($valorPreparado->rowCount() == 1){
                    $row = $valorPreparado->fetch();
                    $_SESSION["id_parceiro"] = $row['cd_parceiro'];
                    $_SESSION["nome_parceiro"] = $row['nm_parceiro'];
                    $_SESSION["descricao_parceiro"] = $row['ds_parceiro'];
                    $_SESSION["perfil_parceiro"] = $row['pefil_parceiro'];
                    $_SESSION["imagem_parceiro"] = $row['img_parceiro'];
                    header('Location:'.base_url().'index.php/Parceiros');
                }else{
                    echo "Usuário ou senha errados.";
                }
            }
        }
    }
}