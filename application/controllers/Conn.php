<?php

class conn extends ci_controller {
    public $adm = '1';
    public function dbconn() {
        return new pdo("mysql:host=localhost;dbname=db_shyme_2", "root", "root");
    }
    public function listarAdm($grupo) {
                $db = $this->dbconn();
                $st = $db->prepare("select cd_matricula from aluno_grupo  where adm_aluno_grupo = ? and cd_grupo = ?");
                $st->bindparam(1, $this->adm);
                $st->bindparam(2, $grupo);
                $st->execute();
                return $st;
    }

    public function removerPromocao($id) {
        $db = $this->dbconn();
        $st = $db->prepare("delete from promocao where cd_promocao = ?");
        $st->bindparam(1, $id);
        $st->execute();
        return $st;
    }
    public function uploadfotoparceiro($id, $img){
        $db = $this->dbconn();
        $st = $db->prepare("update parceiros set img_parceiro=? where cd_parceiro = ?");
        $st->bindparam(1, $img);
        $st->bindparam(2, $id);
        $st->execute();
        return $st;

    }
    public function addFinancas($ds, $valor, $tipo) {
                $db = $this->dbconn();
                $st = $db->prepare("insert into goku (ds,valor,data,tipo)
                 values(?,?,now(),?);");
                $st->bindparam(1, $ds);
                $st->bindparam(2, $valor);
                $st->bindparam(3, $tipo);
                $st->execute();
                return $st;
    }
    public function listarFinancas() {
                
        $db = $this->dbconn();
        $st = $db->prepare("select * from goku order by data desc");
        $st->execute();
        return $st;
    }
    public function total() {
                
        $db = $this->dbconn();
        $st = $db->prepare("select sum(valor) from goku");
        $st->execute();
        return $st;
    }
    public function totalSalario() {
                
        $db = $this->dbconn();
        $st = $db->prepare("select sum(valor) from goku where tipo = 1");
        $st->execute();
        return $st;
    }
    public function totalCofre() {
                
        $db = $this->dbconn();
        $st = $db->prepare("SELECT sum(valor) FROM `goku` WHERE tipo in (1, 2)");
        $st->execute();
        return $st;
    }
    public function totalColchao() {
                
        $db = $this->dbconn();
        $st = $db->prepare("select sum(valor) from goku where tipo = 2");
        $st->execute();
        return $st;
    }
    public function totalInss() {
                
        $db = $this->dbconn();
        $st = $db->prepare("select sum(valor) from goku where tipo = 3");
        $st->execute();
        return $st;
    }
    public function totalBolso() {
                
        $db = $this->dbconn();
        $st = $db->prepare("select sum(valor) from goku where tipo = 4");
        $st->execute();
        return $st;
    }

    public function todosalunoslogin() {
                
        $db = $this->dbconn();
        $st = $db->prepare("select * from aluno order by cd_matricula");
        $st->execute();
        return $st;
    }
    public function ranking() {
                $db = $this->dbconn();
                $st = $db->prepare(" select p.cd_matricula, sum(p.qt_ponto) as soma, a.nm_aluno, a.img_aluno, a.curso_aluno, a.perfil_aluno from ponto_aluno p, aluno a where p.cd_matricula = a.cd_matricula group by  cd_matricula order by soma desc limit 3");
                $st->execute();
                return $st;
    }
    public function rankingsemanalpessoal($idaluno) {
                $db = $this->dbconn();
                $st = $db->prepare("select sum(p.qt_ponto) as ponto from aluno a, ponto_aluno p where a.cd_matricula = ? and a.cd_matricula = p.cd_matricula and week(p.dt_ponto) = (select week(now())) group by p.cd_matricula;");
                $st->bindparam(1, $idaluno);
                $st->execute();
                return $st;
    }
    public function rankingmensallpessoal($idaluno) {
                $db = $this->dbconn();
                $st = $db->prepare("select sum(p.qt_ponto) as ponto from aluno a, ponto_aluno p where a.cd_matricula = ? and a.cd_matricula = p.cd_matricula and month(p.dt_ponto) = (select month(now())) group by p.cd_matricula");
                $st->bindparam(1, $idaluno);
                $st->execute();
                return $st;
    }
    public function rankingtotalpessoal($idaluno) {
                $db = $this->dbconn();
                $st = $db->prepare("select sum(p.qt_ponto) as ponto from aluno a, ponto_aluno p where a.cd_matricula = ? and a.cd_matricula = p.cd_matricula group by p.cd_matricula;");
                $st->bindparam(1, $idaluno);
                $st->execute();
                return $st;
    }
    public function listarapenasmoeda($idaluno) {
                $db = $this->dbconn();
                $st = $db->prepare("select vl_moeda from aluno where cd_matricula = ?");
                $st->bindparam(1, $idaluno);
                $st->execute();
                return $st;
    }
    public function listarInfoPromocao($id) {
                $db = $this->dbconn();
                $st = $db->prepare("select * from promocao where cd_promocao = ?");
                $st->bindparam(1, $id);
                $st->execute();
                return $st;
    }
    public function listarpontuacoes($id) {
                $db = $this->dbconn();
                $st = $db->prepare("call listarpontuacoes(?)");
                $st->bindparam(1, $id);
                $st->execute();
                return $st;
    }

    public function postarPromocao($prioridade, $idaluno, $texto, $imagem,$dtPublicacao,$dtExpiracao) {
                $db = $this->dbconn();
                $st = $db->prepare("insert into promocao (ic_principal,cd_parceiro,img_promocao,ds_promocao,dt_promocao,dt_expiracao,dt_publicacao)
                 values(?,?,?,?,now(),?,?);");
                $st->bindparam(1, $prioridade);
                $st->bindparam(2, $idaluno);
                $st->bindparam(3, $imagem);
                $st->bindparam(4, $texto);
                $st->bindparam(5, $dtExpiracao);
                $st->bindparam(6, $dtPublicacao);
                $st->execute();
                return $st;
    }
    public function listarPostagemParceiro($perfil) {
                $db = $this->dbconn();
                $st = $db->prepare("select p.*, pp.* from promocao p, parceiros pp where ic_principal = 0
                    and p.cd_parceiro = (select cd_parceiro from parceiros where pefil_parceiro = ?) 
                    and pp.cd_parceiro = p.cd_parceiro 
                    order by dt_publicacao desc");
                $st->bindparam(1, $perfil);
                $st->execute();
                return $st;
    }
    public function listarPromocoes($perfil) {
                $db = $this->dbconn();
                $st = $db->prepare("select p.*, pp.* from promocao p, parceiros pp where ic_principal = 1
                    and p.cd_parceiro = (select cd_parceiro from parceiros where pefil_parceiro = ?) and pp.cd_parceiro = p.cd_parceiro limit 1");
                $st->bindparam(1, $perfil);
                $st->execute();
                return $st;
    }public function listarPostagemParceiroPainel($perfil) {
                $db = $this->dbconn();
                $st = $db->prepare("select p.*, pp.* from promocao p, parceiros pp where ic_principal = 0
                    and p.cd_parceiro = (select cd_parceiro from parceiros where cd_parceiro = ?) and pp.cd_parceiro = p.cd_parceiro");
                $st->bindparam(1, $perfil);
                $st->execute();
                return $st;
    }
    public function listarPromocoesPainel($perfil) {
                $db = $this->dbconn();
                $st = $db->prepare("select p.*, pp.* from promocao p, parceiros pp where ic_principal = 1
                    and p.cd_parceiro = (select cd_parceiro from parceiros where cd_parceiro = ?) and pp.cd_parceiro = p.cd_parceiro");
                $st->bindparam(1, $perfil);
                $st->execute();
                return $st;
    }
    public function prepareparceiros($nmusuario, $nmsenhausuario) {
        $db = $this->dbconn();
        $st = $db->prepare("select * from parceiros where nm_login_parceiro=? and cd_senha_parceiro=?");
        $st->bindparam(1, $nmusuario);
        $st->bindparam(2, $nmsenhausuario);
        $st->execute();
        return $st;
    }

    public function infoparceiros() {
        $db = $this->dbconn();
        $st = $db->prepare("select * from parceiros");
        $st->execute();
        return $st;
    }
     public function listarresposta($idpostagem){
        $db = $this->dbconn();
        $st = $db->prepare("select r.*, a.nm_aluno, a.perfil_aluno, a.img_aluno, p.cd_postagem from 
            resposta r, aluno a, treplica t, postagem p
            where a.cd_matricula = r.cd_resposta_aluno
            and r.cd_resposta = t.cd_resposta
            and p.cd_postagem = t.cd_postagem
            and p.cd_postagem=? order by r.ic_resposta desc, r.dt_resposta asc");
        $st->bindparam(1,$idpostagem);
        $st->execute();
        return $st;
    }
    public function escolherresposta($idresposta){
        $db = $this->dbconn();
        $st = $db->prepare("call escolherresposta(?)");
        $st->bindparam(1,$idresposta);
        return $st->execute();
    }

    public function download($up, $down, $desconto,$ponto,$post){
        $db = $this->dbconn();
        $st = $db->prepare("call download(?,?,?,?,?)");
        $st->bindparam(1,$up);
        $st->bindparam(2,$down);
        $st->bindparam(3,$desconto);
        $st->bindparam(4,$ponto);
        $st->bindparam(5,$post);
        return $st->execute();
    }
    
    public function listarMoeda($up){
        $db = $this->dbconn();
        $st = $db->prepare("select vl_moeda from aluno where cd_matricula = ?");
        $st->bindparam(1,$up);
        $st->execute();
        return $st;
    }

    public function grupoduvida($up){
        $db = $this->dbconn();
        $st = $db->prepare("select g.cd_grupo from aluno_grupo g, postagem p 
                            where p.cd_postagem = ?
                            and g.cd_aluno_grupo = p.cd_aluno_grupo");
        $st->bindparam(1,$up);
        $st->execute();
        return $st;
    }

    public function verificardonoduvida($idusuariopostagem){
        $db = $this->dbconn();
        $st = $db->prepare("select cd_matricula from aluno_grupo
                            where cd_aluno_grupo = ?");
        $st->bindparam(1,$idusuariopostagem);
        $st->execute();
        return $st;
    }

    public function listarresposta1($idpostagem){
        $db = $this->dbconn();
        $st = $db->prepare("select r.cd_resposta, r.ds_resposta, a.nm_aluno, a.perfil_aluno , a.img_aluno, t.cd_postagem from 
            resposta r, aluno a, treplica t, postagem p, aluno_grupo ag
            where a.cd_matricula = r.cd_resposta_aluno
            and r.cd_resposta = t.cd_resposta
            and p.cd_postagem = t.cd_postagem
            and ag.cd_aluno_grupo = p.cd_aluno_grupo
            and ag.cd_grupo=? order by r.ic_resposta desc ");
        $st->bindparam(1,$idpostagem);
        $st->execute();
        return $st;
    }




    public function trocarprioridadegrupo($idaluno,$grupoprimario,$gruposecundario){
        $db = $this->dbconn();
        $st = $db->prepare("call trocargrupoprincipal(?,?,?);");
        $st->bindparam(1, $idaluno);
        $st->bindparam(2, $grupoprimario);
        $st->bindparam(3, $gruposecundario);
        $st->execute();
        return $st;
    }

    
    public function excluiradm($idgrupo,$cdmatricula){
        $db = $this->dbconn();
        $st = $db->prepare("update aluno_grupo set adm_aluno_grupo = NULL where cd_matricula= ? and cd_grupo = ?");
        $st->bindparam(1, $cdmatricula);
        $st->bindparam(2, $idgrupo);
        $st->execute();
        $db = $this->sairgrupo($idgrupo,$cdmatricula);
        return $st;
    }
    public function  sairgrupo($idgrupo,$cdmatricula){
        $db = $this->dbconn();
        $st = $db->prepare("call sairgrupo(?,?);");
        $st->bindparam(1, $idgrupo);
        $st->bindparam(2, $cdmatricula);
        $st->execute();
        return $st;
    }
    public function notificaoconvite($idaluno, $dsnotificacao, $idgrupo){
        $db = $this-dbconn();
        $st = $db->prepare("insert into notificacao(ds_notificacao, cd_aluno, cd_grupo, ic_notificacao) values(?,?,?,1)");
        $st->bindparam(1, $dsnotificacao);
        $st->bindparam(2, $idaluno);
        $st->bindparam(3, $idgrupo);
        $st->execute();
        return $st;
    }

    public function notificacaolida($idnotificacao){
        $db = $this->dbconn();
        $st = $db->prepare("update notificacao set status_notificacao = 1 , resposta_notificacao = 1 where cd_notificacao = ?");
        $st->bindparam(1, $idnotificacao);
        $st->execute();
        return $st;
    }

    public function notificacaosemilida($idnotificacao){
        $db = $this->dbconn();
        $st = $db->prepare("update notificacao set resposta_notificacao = 1 , resposta_notificacao = 1 where cd_matricula_destinatario = ?");
        $st->bindparam(1, $idnotificacao);
        $st->execute();
        return $st;
    }

    public function alterarinfogrupo($idgrupo, $dsgrupo, $nmgrupo, $privado){
        $db = $this->dbconn();
        $st = $db->prepare("update grupo set ds_grupo=?, nm_grupo=?, ic_privado = ? where cd_grupo=?");
        $st->bindparam(1, $dsgrupo);
        $st->bindparam(2, $nmgrupo);
        $st->bindparam(3, $privado);
        $st->bindparam(4, $idgrupo);
        return $st->execute();
    }

    public function atualizamoedaaluno($a, $b){
        $db = $this->dbconn();
        $st = $db->prepare("update aluno set vl_moeda = ? where cd_matricula = ?");
        $st->bindparam(1, $a);
        $st->bindparam(2, $b);
        $st->execute();
        return $st;

    }
    public function uploadfotoperfil($id, $img){
        $db = $this->dbconn();
        $st = $db->prepare("update aluno set img_aluno=? where cd_matricula = ?");
        $st->bindparam(1, $img);
        $st->bindparam(2, $id);
        $st->execute();
        return $st;

    }


    public function uploadfotogrupo($id, $img){
        $db = $this->dbconn();
        $st = $db->prepare("update grupo set img_grupo=? where cd_grupo= ?");
        $st->bindparam(1, $img);
        $st->bindparam(2, $id);
        $st->execute();
        return $st;

    }


    public function prepare($nmusuario) {
        $db = $this->dbconn();
        $st = $db->prepare("select * from aluno where cd_matricula=?");
        $st->bindparam(1, $nmusuario);
        $st->execute();
        return $st;
    }

    public function listarinfoperfil($perfil_aluno) {
        $db = $this->dbconn();
        $st = $db->prepare("select * from aluno where perfil_aluno = ?");
        $st->bindparam(1, $perfil_aluno);
        $st->execute();
        return $st;
    }
    public function listarnomeperfil($perfil_aluno) {
        $db = $this->dbconn();
        $st = $db->prepare("select nm_aluno from aluno where perfil_aluno = ?");
        $st->bindparam(1, $perfil_aluno);
        $st->execute();
        return $st;
    }
    public function pontuacaototal($cdusuario) {
            $db = $this->dbconn();
            $st = $db->prepare("select sum(qt_ponto) from ponto_aluno where cd_matricula=?");
            $st->bindparam(1, $cdusuario);
            $st->execute();
            return $st;
        }

        public function moedatotal($cdusuario) {
            $db = $this->dbconn();
            $st = $db->prepare("select vl_moeda from aluno where cd_matricula=?");
            $st->bindparam(1, $cdusuario);
            $st->execute();
            return $st;
        }

    public function selectstud($nmaluno) {
    $nmaluno = "%".$nmaluno."%";
        $db = $this->dbconn();
        $stmt = $db->prepare("select cd_matricula, nm_aluno,img_aluno from aluno where nm_aluno like ?");
        $stmt->bindparam(1, $nmaluno);
        $stmt->execute();
        return $stmt;
    }

    public function alterarNomeParceiro($nmaluno, $idparceiro) {
        $db = $this->dbconn();
        $stmt = $db->prepare("update parceiros set nm_parceiro = ? where cd_parceiro = ?");
        $stmt->bindparam(1, $nmaluno);
        $stmt->bindparam(2, $idparceiro);
        $stmt->execute();
        return $stmt;
    }
    public function alterarDescricaoParceiro($dsaluno, $idparceiro) {
        $db = $this->dbconn();
        $stmt = $db->prepare("update parceiros set ds_parceiro = ? where cd_parceiro = ?");
        $stmt->bindparam(1, $dsaluno);
        $stmt->bindparam(2, $idparceiro);
        $stmt->execute();
        return $stmt;
    }
    public function adicionarcomunicado($idaluno, $texto, $idgrupo, $imagem, $tppost) {
                $db = $this->dbconn();
                $st = $db->prepare("insert into postagem (cd_aluno_grupo,tipo_postagem_cd_tipo_postagem,ds_postagem,dt_postagem,img_postagem)
                 values((select cd_aluno_grupo from aluno_grupo where cd_matricula=? and cd_grupo=?),?,?,now(),?);");
                $st->bindparam(1, $idaluno);
                $st->bindparam(2, $idgrupo);
                $st->bindparam(3, $tppost);
                $st->bindparam(4, $texto);
                $st->bindparam(5, $imagem);
                return $st->execute();
    }

    public function adicionarpostspotted($idaluno, $texto, $imagem, $nick) {
                $db = $this->dbconn();
                $st = $db->prepare("insert into spotted (cd_matricula, ds_postagem_spotted, dt_postagem_spotted, ic_postagem_spotted, arquivo_postagem_spotted, nick_spotted) values(?,?,now(),1,?,?)");
                $st->bindparam(1, $idaluno);
                $st->bindparam(2, $texto);
                $st->bindparam(3, $imagem);
                $st->bindparam(4, $nick);
                return $st->execute();
    }

    public function adicionarmaterial($imagem,$idaluno, $texto, $idgrupo,$tppost,$arquivo) {
        $db = $this->dbconn();
        $st = $db->prepare("insert into postagem (img_postagem,cd_aluno_grupo,tipo_postagem_cd_tipo_postagem,ds_postagem,dt_postagem,arquivo_postagem)
         values(?,(select cd_aluno_grupo from aluno_grupo where cd_matricula=? and cd_grupo=?),?,?,now(),?);");
        $st->bindparam(1, $imagem);
        $st->bindparam(2, $idaluno);
        $st->bindparam(3, $idgrupo);
        $st->bindparam(4, $tppost);
        $st->bindparam(5, $texto);
        $st->bindparam(6, $arquivo);
        $st->execute();
        return $st;
    }

    public function downloadponto($cd_postagem, $idaluno) {
        $verifica = $this->verificarmoeda($idaluno, $tppost);
        if($verifica->rowcount() !== 0){
                $db = $this->dbconn();
                $st = $db->prepare("insert into download(cd_postagem, cd_moeda_download, cd_aluno_download, dt_download) values(?,1,?,now())");
                $st->bindparam(1, $prioridade);
                $st->bindparam(2, $idaluno);
                return $st->execute();
        }else{
            return 0;
        }
    }

    public function verificarprioridadegrupo($idaluno, $idgrupo){
        $db = $this->dbconn();
        $st = $db->prepare("select prioridade_grupo from aluno_grupo where cd_matricula = ? and cd_grupo = ?");
        $st->bindparam(1, $idaluno);
        $st->bindparam(2, $idgrupo);
        $st->execute();
        return $st;
    }

    public function verificarmoeda($idaluno, $tp){
        $db = $this->dbconn();
        $st = $db->prepare("call verificarmoeda(?,?)");
        $st->bindparam(1, $tp);
        $st->bindparam(2, $idaluno);
        $st->execute();
        return $st;
    }
    public function atualizarmoedaponto($idaluno){
        $db = $this->dbconn();
        $st = $db->prepare("select a.vl_moeda, p.qt_ponto from aluno a, ponto_aluno p where a.cd_matricula = ? and a.cd_matricula = p.cd_matricula");
        $st->bindparam(1, $idaluno);
        $st->execute();
        return $st;
    }


    public function adicionarduvida($idaluno, $texto, $idgrupo, $imagem, $vlduvida,$tppost) {
        $db = $this->dbconn();
        $st = $db->prepare("insert into postagem (cd_aluno_grupo,tipo_postagem_cd_tipo_postagem,ds_postagem,dt_postagem,img_postagem,cd_vl_duvida)
         values((select cd_aluno_grupo from aluno_grupo where cd_matricula=? and cd_grupo=?),?,?,now(),?,?);");
        $st->bindparam(1, $idaluno);
        $st->bindparam(2, $idgrupo);
        $st->bindparam(3, $tppost);
        $st->bindparam(4, $texto);
        $st->bindparam(5, $imagem);
        $st->bindparam(6, $vlduvida);
        return $st->execute();
    }   

    public function adicionarresposta($texto,$idaluno,$imagem,$idpostagem) {
        $db = $this->dbconn();
        $st = $db->prepare("insert into resposta (ds_resposta,cd_resposta_aluno,img_resposta,dt_resposta) 
            values (?,?,?,now());");
        $st->bindparam(1,$texto);
        $st->bindparam(2,$idaluno);
        $st->bindparam(3,$imagem);
        $st->execute();
        $db = $this->adicionartreplica($idpostagem);
        return $st;
    }
    public function adicionarcomentario($texto,$idaluno,$idpostagem) {
        $db = $this->dbconn();
        $st = $db->prepare("insert into comentario_spotted (ds_comentario_spotted,cd_matricula) 
            values (?,?);");
        $st->bindparam(1,$texto);
        $st->bindparam(2,$idaluno);
        $st->execute();
        $db = $this->adicionartreplicaspotted($idpostagem);
        return $st;
    }


    public function criargrupo($ds,$idaluno,$nmgrupo,$privacidade,$token,$imagem) {
        $db = $this->dbconn();
        $st = $db->prepare("insert into grupo (nm_grupo,ds_grupo,ic_privado,ic_grupo,tipo_grupo_cd_tipo,token,criador,img_grupo) 
            values (?,?,?,1,2,?,?,?)");
        $st->bindparam(1,$nmgrupo);
        $st->bindparam(2,$ds);
        $st->bindparam(3,$privacidade);
        $st->bindparam(4,$token);
        $st->bindparam(5,$idaluno);
        $st->bindparam(6,$imagem);
        $st->execute();
        return $st;
    }
    public function adicionaradm($idaluno,$token) {
        $db = $this->dbconn();
        $st = $db->prepare("call adicionaradm(?,?);");
        $st->bindparam(1,$idaluno);
        $st->bindparam(2,$token);
        $st->execute();
        return $st;
    }
    public function nomearadm($idaluno,$nmgrupo) {
        $db = $this->dbconn();
        $st = $db->prepare("update aluno_grupo set adm_aluno_grupo = ? where cd_grupo = ? and cd_matricula=?");
        $st->bindparam(1,$this->adm);
        $st->bindparam(2,$nmgrupo);
        $st->bindparam(3,$idaluno);
        $st->execute();
        return $st;
    }

    public function adicionartreplica($idpostagem){
        $db = $this->dbconn();
        $st = $db->prepare("insert into treplica values ((select max(cd_resposta) from resposta),?)");
        $st->bindparam(1,$idpostagem);
        $st->execute();
    } 

    public function adicionartreplicaspotted($idpostagem){
        $db = $this->dbconn();
        $st = $db->prepare("insert into treplica_spotted values ((select max(cd_comentario_Spotted) from comentario_spotted),?)");
        $st->bindparam(1,$idpostagem);
        $st->execute();
    } 

//falta completar totalmente, falta coisaaaas
    public function listarpostagem($idgrupo, $pag) {
        $db = $this->dbconn();
        $stmt = $db->prepare("select ag.cd_aluno_grupo,a.cd_matricula, a.vl_moeda, a.nm_aluno,a.perfil_aluno, a.img_aluno, p.ds_postagem, p.dt_postagem, p.arquivo_postagem,p.img_postagem, p.tipo_postagem_cd_tipo_postagem, p.cd_postagem, p.cd_vl_duvida  from aluno_grupo ag, aluno a, postagem p where  ag.cd_grupo = ? and p.cd_aluno_grupo = ag.cd_aluno_grupo and ag.cd_matricula = a.cd_matricula and p.ic_postagem = 1 order by p.dt_postagem desc limit ". $pag ." , 10 ");
        $stmt->bindparam(1, $idgrupo);
        $stmt->execute();
        return $stmt;
    }
   
    public function listarpostagemtotal($idgrupo) {
        $db = $this->dbconn();
        $stmt = $db->prepare("select count(*)  from aluno_grupo ag, aluno a, postagem p where  ag.cd_grupo = ? and p.cd_aluno_grupo = ag.cd_aluno_grupo and ag.cd_matricula = a.cd_matricula and p.ic_postagem = 1 order by p.dt_postagem desc");
        $stmt->bindparam(1, $idgrupo);
        $stmt->execute();
        return $stmt;
    }
   
  

    //gustavo vai fazer o select, essa função é no caso da pagina da duvida
    public function listarumapostagem($idpostagem){
        $db = $this->dbconn();
        $st = $db->prepare("select a.nm_aluno,a.perfil_aluno,a.img_aluno, p.* from aluno a, postagem p, aluno_grupo g where a.cd_matricula = g.cd_matricula and g.cd_aluno_grupo = p.cd_aluno_grupo and p.cd_postagem=?");
        $st->bindparam(1,$idpostagem);
        $st->execute();
        return $st;
    }

    public function listarinfogrupo($idgrupo){
        $db = $this->dbconn();
        $stmt = $db->prepare("select * from grupo where cd_grupo = ?");
         $stmt->bindparam(1, $idgrupo);
        $stmt->execute();
        return $stmt;
    }
    
    public function listarpostagemperfil($idaluno) {
        $db = $this->dbconn();
        $stmt = $db->prepare("select  p.cd_aluno_grupo, p.*, a.cd_matricula, a.nm_aluno, g.cd_grupo, g.nm_grupo, p.tipo_postagem_cd_tipo_postagem, a.perfil_aluno , a.img_aluno 
                            from postagem p, aluno_grupo ag, grupo g, aluno a
                            where ag.cd_aluno_grupo = p.cd_aluno_grupo
                            and ag.cd_grupo = g.cd_grupo
                            and ag.cd_matricula = a.cd_matricula
                            and p.ic_postagem = 1
                            and ag.cd_grupo in (select cd_grupo from aluno_grupo where cd_matricula = ?) order by p.dt_postagem desc limit 5");
         $stmt->bindparam(1, $idaluno);
        $stmt->execute();
        return $stmt;
    }
    
    public function listarpostagemperfilaluno($idaluno) {
        $db = $this->dbconn();
        $stmt = $db->prepare("select distinct a.nm_aluno, a.img_aluno, a.perfil_aluno, 
            p.*,
            g.*
            from aluno a, postagem p, grupo g, aluno_grupo ag
            where a.perfil_aluno = ?
            and a.cd_matricula = ag.cd_matricula
            and ag.cd_aluno_grupo = p.cd_aluno_grupo
            and ag.cd_grupo = g.cd_grupo
            and ag.ic_aluno_grupo = 1
            and g.ic_grupo = 1
            order by p.dt_postagem desc 
            limit 0,5;");
         $stmt->bindparam(1, $idaluno);
        $stmt->execute();
        return $stmt;
    }
    //--------------------------------------------------------------------------------------------------------------

    //excluir postagem precisa ser arrumado!!!
    public function excluirpostagem($idpost) {
        $db = $this->dbconn();
        $st = $db->prepare("update postagem set ic_postagem=0 where cd_postagem=? limit 1");
        $st->bindparam(1, $idpost);
        $st->execute();
        return $st;
    }

    public function pesquisaGrupo($texto) {
        $texto = "%" . $texto . "%";
        $db = $this->dbconn();
        $st = $db->prepare('select * from grupo where nm_grupo like ? and ic_privado = 0 and ic_grupo = 1;');
        $st->bindparam(1, $texto);
        $st->execute();
        return $st;
    }

    public function pesquisaParceiro($texto) {
        $texto = "%" . $texto . "%";
        $db = $this->dbconn();
        $st = $db->prepare('select * from parceiros');
        $st->execute();
        return $st;
    }

    public function pesquisaPessoa($texto) {
        $texto = "%" . $texto . "%";
        $db = $this->dbconn();
        $st = $db->prepare('select * from aluno where nm_aluno like ?');
        $st->bindparam(1, $texto);
        $st->execute();
        return $st;
    }
  public function pesquisaralunogrupo($texto) {
        //$texto = "%" . $texto . "%";
        $db = $this->dbconn();
        $st = $db->prepare('insert into parceiros(nm_login_parceiro) values(?)');
        $st->bindparam(1, $texto);
        $st->execute();
        return $st;
    }

    public function selectmembers($cdgrupo) {
        $db = $this->dbconn();
        $stmt = $db->prepare("select a.cd_matricula, a.nm_aluno, a.img_aluno, a.curso_aluno, ag.cd_aluno_grupo, ag.adm_aluno_grupo, a.perfil_aluno from aluno_grupo ag, aluno a where ag.cd_matricula = a.cd_matricula and ag.cd_grupo = ? and ag.ic_aluno_grupo = 1 order by ag.adm_aluno_grupo desc");
        $stmt->bindparam(1, $cdgrupo);
        $stmt->execute();
        return $stmt;
    }
    public function verificaradministradorgrupo($cdgrupo, $cdusuario) {
        $db = $this->dbconn();
        $stmt = $db->prepare("select adm_aluno_grupo from aluno_grupo where cd_grupo = ? and cd_matricula = ? ");
        $stmt->bindparam(1, $cdgrupo);
        $stmt->bindparam(2, $cdusuario);
        $stmt->execute();
        return $stmt;
    }

    public function adicionarmembro($cdusuario, $cdnotificacao, $cdgrupo ) {
        $db = $this->dbconn();
        $stmt = $db->prepare("call adicionarmembrogrupo(?,?,?)");
        $stmt->bindparam(1, $cdusuario);
        $stmt->bindparam(2, $cdnotificacao);
        $stmt->bindparam(3, $cdgrupo);
        $stmt->execute();
        return $stmt;
    }
    public function respondernotificacao($idnot) {
        $db = $this->dbconn();
        $stmt = $db->prepare("update notificacao set resposta_notificacao = 1 where cd_notificacao = ?");
        $stmt->bindparam(1, $idnot);
        $stmt->execute();
        return $stmt;
    }

    public function enviarnotificacao($cdusuario, $cdgrupo,$cdpost, $tp) {
        $db = $this->dbconn();
        $stmt = $db->prepare("call enviarnotificacao(?,?,?,?)");
        $stmt->bindparam(2, $cdgrupo);
        $stmt->bindparam(1, $cdusuario);
        $stmt->bindparam(3, $cdpost);
        $stmt->bindparam(4, $tp);
        $stmt->execute();
        return $stmt;
    }


    public function excluirgrupo($cdgrupo) {
        $db = $this->dbconn();
        $stmt = $db->prepare("call excluirgrupo(?)");
        $stmt->bindparam(1, $cdgrupo);
        $stmt->execute();
        return $stmt;
    }

    public function deletemembers($cdgrupo, $cdusuario) {
        $db = $this->dbconn();
        $stmt = $db->prepare("delete from aluno_grupo where cd_grupo=? and cd_matricula=?)");
        $stmt->bindparam(1, $cdgrupo);
        $stmt->bindparam(2, $cdusuario);
        $stmt->execute();
        return $stmt;
    }
    public function qtdmembros($idgrupo){
        $db = $this->dbconn();
        $st = $db->prepare('select count(*) from aluno a, aluno_grupo b where b.cd_matricula = a.cd_matricula and b.cd_grupo =?');
        $st->bindparam(1,$idgrupo);
        $st->execute();
        return $st;
    }


    /*
     * modelo
     * método seleciona o código do curso de acordo com o código do aluno informado.
     * @param matricula do aluno
     * @return o valor do retorno em caso de sucesso, depende do fetch a ser utilizado. em
     * todo caso retorna false em caso de erro.
     */

    public function selectcourse($cdmatriculaaluno) {
        $db = $this->dbconn();
        $stmt = $db->prepare("select curso_aluno from aluno where cd_aluno = ?");
        $stmt->bindparam(1, $cdmatriculaaluno);
        $stmt->execute();
        return $stmt;
    }

    /*
     * modelo
     * método delete o grupo de acordo com o código do grupo e código do administrador
     * @param código do grupo
     * @param códido do administrador do grupo
     * @return o valor do retorno em caso de sucesso, depende do fetch a ser utilizado. em
     * todo caso retorna false em caso de erro.
     */

    public function deletargrupo($cdgrupo) {
        $db = $this->dbconn();
        //incompleto
        $stmt = $db->prepare("update grupo set ic_grupo = 0 where cd_grupo=?");
        $stmt->bindparam(1, $cdgrupo);
        $stmt->execute();
        return $stmt;
    }

    /*
     * modelo
     * método que retorna o nome do grupo de acordo com o código do administrador
     * @param código do administrador
     * @param2 código do grupo
     * @return nome do grupo se o usuário informado for administrador.
     */

    public function selectverifyadm($cdadmgrupo, $cdgrupo) {
        $db = $this->dbconn();
        $stmt = $db->prepare("select cd_grupo from aluno_grupo where adm_aluno_grupo = ? and cd_matricula= ? and cd_grupo = ?");
        $stmt->bindparam(1, $this->adm);
        $stmt->bindparam(2, $cdadmgrupo);
        $stmt->bindparam(3, $cdgrupo);
        $stmt->execute();
        return $stmt;
    }

    public function selectgroupsadm($cdadmgrupo) {
        $db = $this->dbconn();
        $stmt = $db->prepare("select cd_grupo from aluno_grupo  where adm_aluno_grupo = ? and cd_matricula=?");
        $stmt->bindparam(1, $this->adm);
        $stmt->bindparam(2, $cdadmgrupo);
        $stmt->execute();
        return $stmt;
    }

    public function verificarspotteddia($idaluno) {
        $db = $this->dbconn();
        $stmt = $db->prepare("select cd_postagem_spotted, date_format(dt_postagem_spotted, '%d/%m/%y') from spotted where cd_matricula = ? and date_format(dt_postagem_spotted, '%d/%m/%y') = date_format(now(), '%d/%m/%y')");
        $stmt->bindparam(1, $idaluno);
        $stmt->execute();
        return $stmt;
    }

    public function verificarduvidadia($idaluno,$idgrupo) {
        $db = $this->dbconn();
        $stmt = $db->prepare("select cd_postagem, date_format(dt_postagem, '%d/%m/%y') from postagem where cd_aluno_grupo = (select cd_aluno_grupo from aluno_grupo where cd_matricula = ? and cd_grupo=?) and (tipo_postagem_cd_tipo_postagem = 26 or tipo_postagem_cd_tipo_postagem = 27)  and date_format(dt_postagem, '%d/%m/%y') = date_format(now(), '%d/%m/%y')");
        $stmt->bindparam(1, $idaluno);
        $stmt->bindparam(2, $idgrupo);
        $stmt->execute();
        return $stmt;
    }

    public function listarspottedpostagem() {
        $db = $this->dbconn();
        $stmt = $db->prepare("select cd_postagem_spotted, cd_matricula, ds_postagem_spotted, dt_postagem_spotted, ic_postagem_spotted, arquivo_postagem_spotted, img_spotted, nick_spotted  from spotted order by dt_postagem_spotted desc");
        $stmt->execute();
        return $stmt;
    }

    public function listarspottedcomentario($idpostagem){
        $db = $this->dbconn();
        $st = $db->prepare("SELECT s.cd_postagem_spotted, s.ds_postagem_spotted, s.dt_postagem_spotted, s.img_spotted, s.nick_spotted, c.cd_matricula, c.ds_comentario_spotted, c.dt_comentario_spotted ,c.arquivo_comentario_spotted, c.cd_comentario_spotted, a.nm_aluno, a.img_aluno, a.perfil_aluno FROM spotted s, treplica_spotted t, comentario_spotted c, aluno a WHERE t.cd_postagem_spotted = s.cd_postagem_spotted AND t.cd_comentario_spotted = c.cd_comentario_spotted AND c.cd_matricula = a.cd_matricula AND s.cd_postagem_spotted = 36 ORDER BY c.dt_comentario_spotted desc");
        $st->bindparam(1,$idpostagem);
        $st->execute();
        return $st;
    }

    public function listarspottedcomentariocurto($idpostagem){
        $db = $this->dbconn();
        $st = $db->prepare("select * from spotted where cd_postagem_spotted = ?");
        $st->bindparam(1,$idpostagem);
        $st->execute();
        return $st;
    }


    /*
     * modelo
     * método atualiza o nome do grupo de acordo com o código do grupo e do
     * administrador.
     * @param código do administrador;
     * @param2 código do grupo;
     * @param3 nome do grupo.
     * @return o valor do retorno em caso de sucesso, varia de acordo com o fetch
     * a ser utilizado. em todo o caso retorna false se houver erro.
     */

    public function updategroupname($cdadmgrupo, $cdgrupo, $nmgrupo) {
        $db = $this->dbconn();
        $stmt = $db->prepare("update grupo set nm_grupo = ? where cd_grupo = ?");
        $stmt->bindparam(1, $nmgrupo);
        $stmt->bindparam(2, $cdadmgrupo);
        $stmt->bindparam(3, $cdgrupo);
        $stmt->execute();
        return $stmt;
    }

    /*
     * modelo
     * método atualiza a descrição do grupo de acordo com os valores informados.
     * @param código do administrador.
     * @param2 código do grupo.
     * @param3 descrição.
     * @return o valor do retorno em caso de sucesso, varia de acordo com o fetch
     * a ser utilizado. em todo o caso retorna false se houver erro.
     */

    public function updategroupdescription($cdadmgrupo, $cdgrupo, $dsgrupo) {
        $db = $this->dbconn();
        $stmt = $db->prepare("update grupo set ds_grupo = ? where cd_grupo = ?");
        $stmt->bindparam(1, $dsgrupo);
        $stmt->bindparam(2, $cdadmgrupo);
        $stmt->bindparam(3, $cdgrupo);
        $stmt->execute();
        return $stmt;
    }

    /*
     * modelo
     * método que exclui um membro de acordo com o código informado.
     * @param código do grupo;
     * @param2 código do usuário;
     * @return o valor do retorno em caso de sucesso, varia de acordo com o fetch
     * a ser utilizado. em todo o caso retorna false se houver erro.
     */


    public function excluirmembro( $cdusuario, $cdgrupo) {
        $db = $this->dbconn();
        $stmt = $db->prepare("call excluirmembro(?,?)");
        $stmt->bindparam(1, $cdusuario);
        $stmt->bindparam(2, $cdgrupo);
        $stmt->execute();
        return $stmt;
    }

    public function gruponotificacao($cdnotificacao) {
        $db = $this->dbconn();
        $stmt = $db->prepare("select cd_grupo_notificacao from notificacao where cd_notificacao = ?");
        $stmt->bindparam(1, $cdnotificacao);
        $stmt->execute();
        return $stmt;
    }
    public function alterarnotificacao($cdnotificacao) {
        $db = $this->dbconn();
        $stmt = $db->prepare("update notificacao set respondido = 1 where cd_notificacao = ?");
        $stmt->bindparam(1, $cdnotificacao);
        $stmt->execute();
        return $stmt;
    }

    public function getidgrupo($cdnotificacao) {
        $db = $this->dbconn();
        $stmt = $db->prepare("select cd_grupo_notificacao from notificacao  where cd_notificacao = ?");
        $stmt->bindparam(1, $cdnotificacao);
        $stmt->execute();
        return $stmt;
    }


    public function verificaratividadegrupo($cdgrupo) {
        $db = $this->dbconn();
        $stmt = $db->prepare("select ic_grupo from grupo where cd_grupo = ?");
        $stmt->bindparam(1, $cdgrupo);
        $stmt->execute();
        return $stmt;
    }
    public function listarnotificacao($cdaluno, $pag) {
        $db = $this->dbconn();
        $stmt = $db->prepare("select n.*, a.img_aluno, tp.img_tipo_notificacao, tp.ds_tipo_notificacao from notificacao n, aluno a, tipo_notificacao tp where n.cd_matricula_destinatario = a.cd_matricula and n.cd_matricula_destinatario = ? and  n.respondido is null and tp.cd_tipo_notificacao = (select cd_tipo_notificacao from tipo_notificacao where cd_tipo_notificacao = n.tp_notificacao) order by n.dt_notificacao desc  limit ". $pag ." , 10 ");
        $stmt->bindparam(1, $cdaluno);
        $stmt->execute();
        return $stmt;
    }
    public function semilidas($id) {
        $db = $this->dbconn();
        $stmt = $db->prepare("update notificacao set resposta_notificacao = 1 where cd_notificacao = ?");
        $stmt->bindparam(1, $id);
        $stmt->execute();
        return $stmt;
    }
    public function listarnotificacaototal($cdaluno) {
        $db = $this->dbconn();
        $stmt = $db->prepare("select count(*) from notificacao n, aluno a, tipo_notificacao tp where n.cd_matricula_destinatario = a.cd_matricula and n.cd_matricula_destinatario = ? and  n.respondido is null and tp.cd_tipo_notificacao = (select cd_tipo_notificacao from tipo_notificacao where cd_tipo_notificacao = n.tp_notificacao) ");
        $stmt->bindparam(1, $cdaluno);
        $stmt->execute();
        return $stmt;
    }
    public function novasnotificacoes($cdaluno) {
        $db = $this->dbconn();
        $stmt = $db->prepare("select count(*) from notificacao n, aluno a, tipo_notificacao tp where n.cd_matricula_destinatario = a.cd_matricula and n.cd_matricula_destinatario = ? and  tp.cd_tipo_notificacao = (select cd_tipo_notificacao from tipo_notificacao where cd_tipo_notificacao = n.tp_notificacao) and resposta_notificacao = 0");
        $stmt->bindparam(1, $cdaluno);
        $stmt->execute();
        return $stmt;
    }
    public function listarumanotificacao($cdaluno, $id) {
        $db = $this->dbconn();
        $stmt = $db->prepare("select n.*, a.img_aluno, tp.img_tipo_notificacao, tp.ds_tipo_notificacao from notificacao n, aluno a, tipo_notificacao tp where n.cd_matricula_destinatario = a.cd_matricula and n.cd_matricula_destinatario = ? and   n.respondido is null and tp.cd_tipo_notificacao = (select cd_tipo_notificacao from tipo_notificacao where cd_tipo_notificacao = n.tp_notificacao) and n.cd_notificacao = ?");
        $stmt->bindparam(1, $cdaluno);
        $stmt->bindparam(2, $id);
        $stmt->execute();
        return $stmt;
    }
    public function verificaratividadegrupoaluno($cdgrupo, $cdusuario) {
        $db = $this->dbconn();
        $stmt = $db->prepare("select ic_aluno_grupo from aluno_grupo where cd_grupo = ? and cd_matricula = ?");
        $stmt->bindparam(1, $cdgrupo);
        $stmt->bindparam(2, $cdusuario);
        $stmt->execute();
        return $stmt;
    }
    public function verificarPrivacidadeGrupo($cdgrupo) {
        $db = $this->dbconn();
        $stmt = $db->prepare("select ic_privado from grupo where cd_grupo = ?");
        $stmt->bindparam(1, $cdgrupo);
        $stmt->execute();
        return $stmt;
    }

    
    /*
     * modelo
     * método que atualiza o nome da imagem do grupo
     * @param código do administrador;
     * @param2 código do grupo;
     * @param3 nome(caminho) da imagem.
     * @return o valor do retorno em caso de sucesso, varia de acordo com o fetch
     * a ser utilizado. em todo o caso retorna false se houver erro.
     */

    public function updateimage( $cdgrupo, $imggrupo) {
        $db = $this->dbconn();
        $stmt = $db->prepare("update grupo set img_grupo = ? where  cd_grupo = ?");
        $stmt->bindparam(1, $imggrupo);
        $stmt->bindparam(2, $cdgrupo);
        $stmt->execute();
        return $stmt;
    }

    public function updateimageperfil($cdaluno, $imgperil) {
        $db = $this->dbconn();
        $stmt = $db->prepare("update aluno set img_aluno = ? where  cd_matricula = ?");
        $stmt->bindparam(1, $imgperfil);
        $stmt->bindparam(2, $cdaluno);
        $stmt->execute();
        return $stmt;
    }

    /*
     * modelo
     * método seleciona o tipo de acesso ao grupo de acordo com o código do grupo
     * ou seja, se o grupo é privado ou não.
     * @param código do administrador;
     * @param2 código do grupo.
     * @return o valor do retorno em caso de sucesso, varia de acordo com o fetch
     * a ser utilizado. em todo o caso retorna false se houver erro.
     */

    public function selectaccess($cdadmgrupo, $cdgrupo) {
        $db = $this->dbconn();
        $stmt = $db->prepare("select ic_privado from grupo where  cd_grupo = ?");
        $stmt->bindparam(1, $cdadmgrupo);
        $stmt->bindparam(2, $cdgrupo);
        $stmt->execute();
        return $stmt;
    }

    /*
     * modelo
     * este método atualiza o tipo de acesso ao grupo.
     * recebe do método selectaccess() e altera o valor recebido.
     * @param código do administrador;
     * @param2 código do grupo;
     * @return o valor do retorno em caso de sucesso, varia de acordo com o fetch
     * a ser utilizado. em todo o caso retorna false se houver erro.
     */

    public function updateaccess($cdadmgrupo, $cdgrupo) {
        $result = $this->selectaccess($cdadmgrupo, $cdgrupo);
        $row = $result->fetchcolumn();
        if ($row == "s") {
            $row = "n";
        } else {
            $row = "s";
        }

        $db = $this->dbconn();
        $stmt = $db->prepare("update grupo set ic_privado = ? where cd_adm_grupo  = ? and cd_grupo = ?");
        $stmt->bindparam(1, $row);
        $stmt->bindparam(2, $cdadmgrupo);
        $stmt->bindparam(3, $cdgrupo);
        $stmt->execute();
        return $stmt;
    }

    /*
     * modelo 
     * método seleciona todos os nomes dos grupos de acordo com o código informado.
     * @param código do usuário.
     * @return o valor do retorno em caso de sucesso, varia de acordo com o fetch
     * a ser utilizado. em todo o caso retorna false se houver erro.
     */

    public function selectallgroups($cdusuario) {
        $db = $this->dbconn();
        $stmt = $db->prepare("select g.cd_grupo, g.nm_grupo from grupo g, aluno_grupo ug where ug.cd_grupo = g.cd_grupo and  ug.cd_matricula= ? and ug.ic_aluno_grupo=1");
        $stmt->bindparam(1, $cdusuario);
        $stmt->execute();
        return $stmt;
    }

    public function listargruposprimarios($cdusuario) {
        $db = $this->dbconn();
        $stmt = $db->prepare("select g.cd_grupo, g.nm_grupo, g.img_grupo  from grupo g, aluno_grupo ug where ug.cd_grupo = g.cd_grupo and  ug.cd_matricula= ? and ug.prioridade_grupo=1 and ug.ic_aluno_grupo=1");
        $stmt->bindparam(1, $cdusuario);
        $stmt->execute();
        return $stmt;
    }

    public function listargrupossecundarios($cdusuario) {
        $db = $this->dbconn();
        $stmt = $db->prepare("select g.cd_grupo, g.nm_grupo, g.img_grupo from grupo g, aluno_grupo ug where ug.cd_grupo = g.cd_grupo and  ug.cd_matricula= ? and ug.prioridade_grupo=0 and ug.ic_aluno_grupo=1");
        $stmt->bindparam(1, $cdusuario);
        $stmt->execute();
        return $stmt;
    }

    public function listargruposprimariosperfil($id_aluno, $id_perfil) {
        $db = $this->dbconn();
        $stmt = $db->prepare("select  ag.cd_grupo, g.nm_grupo, g.img_grupo from aluno_grupo ag, grupo g where ag.cd_matricula = ? 
            and ag.ic_aluno_grupo = 1
            and ag.prioridade_grupo = 1
            and g.ic_privado = 1
            and ag.cd_grupo in(select al.cd_grupo from aluno_grupo al, grupo gr 
                                where al.cd_matricula = ? 
                                and al.prioridade_grupo = 1 
                                and al.ic_aluno_grupo = 1
                                and al.cd_grupo = gr.cd_grupo 
                                and gr.ic_privado = 1)
            and ag.ic_aluno_grupo = 1
            and ag.cd_grupo = g.cd_grupo
            union
            select distinct ag.cd_grupo, g.nm_grupo, g.img_grupo from aluno_grupo ag, grupo g where ag.cd_matricula = ? 
            and ag.ic_aluno_grupo = 1
            and ag.prioridade_grupo = 1
            and g.ic_privado = 0
            and ag.cd_grupo = g.cd_grupo");
        $stmt->bindparam(1, $id_perfil);
        $stmt->bindparam(2, $id_aluno);
        $stmt->bindparam(3, $id_perfil);
        $stmt->execute();
        return $stmt;
    }

    public function pegarIdPerfil($id_perfil) {
        $db = $this->dbconn();
        $stmt = $db->prepare("select cd_matricula from aluno where perfil_aluno like ?");
        $stmt->bindparam(1, $id_perfil);
        $stmt->execute();
        return $stmt;
    }

    public function pegarIdGrupo($grupo) {
        $db = $this->dbconn();
        $stmt = $db->prepare("select cd_matricula from aluno_grupo where cd_grupo = ? and ic_aluno_grupo = 1");
        $stmt->bindparam(1, $grupo);
        $stmt->execute();
        return $stmt;
    }

    public function listargrupossecundariosperfil($id_aluno, $id_perfil) {
        $db = $this->dbconn();
        $stmt = $db->prepare("select  ag.cd_grupo, g.nm_grupo, g.img_grupo from aluno_grupo ag, grupo g where ag.cd_matricula = ? 
            and ag.ic_aluno_grupo = 1
            and ag.prioridade_grupo = 0
            and g.ic_privado = 1
            and ag.cd_grupo in(select al.cd_grupo from aluno_grupo al, grupo gr 
                                where al.cd_matricula = ? 
                                and al.prioridade_grupo = 0 
                                and al.ic_aluno_grupo = 1
                                and al.cd_grupo = gr.cd_grupo 
                                and gr.ic_privado = 1)
            and ag.ic_aluno_grupo = 1
            and ag.cd_grupo = g.cd_grupo
            union
            select distinct ag.cd_grupo, g.nm_grupo, g.img_grupo from aluno_grupo ag, grupo g where ag.cd_matricula = ? 
            and ag.ic_aluno_grupo = 1
            and ag.prioridade_grupo = 0
            and g.ic_privado = 0
            and ag.cd_grupo = g.cd_grupo");
        $stmt->bindparam(1, $id_perfil);
        $stmt->bindparam(2, $id_aluno);
        $stmt->bindparam(3, $id_perfil);
        $stmt->execute();
        return $stmt;
    }
    public function listarPrivacidadeGrupoVisitante($perfil_aluno) {
        $db = $this->dbconn();
        $stmt = $db->prepare("select g.cd_grupo, g.ic_privado from grupo g,aluno_grupo ag where ag.cd_grupo = g.cd_grupo and ag.cd_matricula= ? and ag.ic_aluno_grupo = 1 and g.ic_privado = 1");
        $stmt->bindparam(1, $perfil_aluno);
        $stmt->execute();
        return $stmt;
    }

    //essa função pode ser mais tarde utilizada na selectallgroups - a mesma pode 
    //pegar também o código do usuário.
    public function selectcdgroup($cdusuario) {
        $db = $this->dbconn();
        $stmt = $db->prepare("select cd_grupo from aluno_grupo where cd_matricula = ? and ug.ic_aluno_grupo=1");
        $stmt->bindparam(1, $cdusuario);
        $stmt->execute();
        return $stmt;
    }

    public function validarperfil($idaluno, $nmaluno, $perfil, $moeda, $curso, $rg){
        $db = $this->dbconn();
        $stm = $db->prepare("insert into aluno(cd_matricula, nm_aluno, perfil_aluno, vl_moeda, curso_aluno, cd_rg_aluno) values(?,?,?,?,?,?)");
        $stm->bindparam(1, $idaluno);
        $stm->bindparam(2, $nmaluno);
        $stm->bindparam(3, $perfil);
        $stm->bindparam(4, $moeda);
        $stm->bindparam(5, $curso);
        $stm->bindparam(6, $rg);
        $stm->execute();
        return $stm;
    }
    
    public function grupoautomatico($idaluno, $cdgrupo){
        $db = $this->dbconn();
        $stm = $db->prepare("insert into aluno_grupo(cd_matricula, cd_grupo, ic_aluno_grupo, prioridade_grupo) values(?,?,1,1)");
        $stm->bindparam(1, $idaluno);
        $stm->bindparam(2, $cdgrupo);
        $stm->execute();
        return $stm;
    }
    public function grupoafatec($idaluno, $cdgrupo){
        $db = $this->dbconn();
        $stm = $db->prepare("insert into aluno_grupo(cd_matricula, cd_grupo, ic_aluno_grupo, prioridade_grupo) values(?,?,1,1)");
        $stm->bindparam(1, $idaluno);
        $stm->bindparam(2, $cdgrupo);
        $stm->execute();
        return $stm;
    }
    
    public function listarmembros($idgrupo){
        $db = $this->dbconn();
        $st = $db->prepare('select a.cd_matricula, a.nm_aluno, b.adm_aluno_grupo, a.perfil_aluno from aluno a, aluno_grupo b where b.cd_matricula = a.cd_matricula and b.cd_grupo =?');
        $st->bindparam(1,$idgrupo);
        $st->execute();
    }
    
    public function verificarmembrosgrupo($cdgrupo) {
        $db = $this->dbconn();
        $stmt = $db->prepare("select count(*) from aluno_grupo where cd_grupo = ? and ic_aluno_grupo = 1");
        $stmt->bindparam(1, $cdgrupo);
        $stmt->execute();
        return $stmt;
    }
   

}

