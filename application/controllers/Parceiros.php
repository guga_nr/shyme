<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once 'Usuario.php';
require_once 'Grupo.php';
require_once 'upload_arquivos.php';
require_once 'Notificacao.php';
class Parceiros extends CI_Controller {

    public function publicacao($id_promo = ''){
        if(isset($_SESSION['id_parceiro'])){
            $conn = new Conn;
            $this->load->helper('url');
            
            $data['botaopost'] = 'postar-promocao';
            $data['nome_da_pagina'] = 'Painel de Publicações';
            
            // $notificacao = new Notificacao();

            //     $data['notificacao']=  $notificacao->listarNotificacao($_SESSION['id'],0);
            //     $data['paginacao']=  $notificacao->listarnotificacaototal($_SESSION['id']);
            //     $data['novas']=  $notificacao->novasnotificacoes($_SESSION['id']);
                
            //     foreach ($data['notificacao'] as $key) {
            //         $notificacao->semilidas($key['cd_notificacao']);
            //     }
                
            if($id_promo != ''){
                $data['infopromo'] = $conn->listarInfoPromocao($id_promo);
                if($data['infopromo']->rowCount() > 0)
                    $data['botaopost'] = 'alterar-promocao'; 
                else
                    header('Location:'.base_url().'index.php/Login/parceiros');
            }

            if(isset($_POST['postar-promocao'])){
                $idAluno = $_SESSION['id_parceiro']; 
                $texto = $_POST['txt_content_post'];
                $tipo = $_POST['tipo'];
                $dtPublicacao = null;
                $dtExpiracao = null;
                
                if (isset($_POST['atv-agendar']) && $_POST['atv-agendar'] == 1) {
                    $dtPublicacao = date("Y"). '-' . $_POST['mes-agendamento']. '-' . $_POST['dia-agendamento'];
                }

                if (isset($_POST['atv-exp']) && $_POST['atv-exp'] == 1) {
                    $dtExpiracao = date("Y"). '-' . $_POST['mes-expiracao']. '-' . $_POST['dia-expiracao'];
                }

                $imagem ='';
                if(isset($_FILES['promocao_imagem'])  && !empty($_FILES['promocao_imagem']['name'])){ 
                    $imagem = upload($_FILES['promocao_imagem'],"/parceiros/postagens/");
                }

                if(isset($_POST['data-promocao'])){
                    $data = explode("+", $_POST['data-promocao']);
                    $dtPublicacao = $data[0];
                    $dtExpiracao = $data[1];
                    // alert($data[0]);
                }
                $pg = $conn->postarPromocao($tipo,$_SESSION['id_parceiro'],$texto,$imagem,$dtPublicacao,$dtExpiracao);
                alert("Promoção publicada");
                // header("Refresh:0");
            }

            $this->load->view('parceiros-publicacao', $data);

        }else{
            header('Location:'.base_url().'index.php/Login/parceiros');
        }
    } 

	public function index(){
        if(isset($_SESSION['id_parceiro'])){
            $conn = new Conn;
            $this->load->helper('url');

            $data['promocao'] = $conn->listarPromocoes($_SESSION['id_parceiro']);
            $data['ranking'] =$conn->ranking();

            $data['nome_da_pagina'] = $_SESSION['nome_parceiro'] . ' - Shyme';

            
            if (isset($_GET['newname'])) {
                $conn->alterarNomeParceiro($_GET['newname'], $_SESSION['id_parceiro']);
                $_SESSION['nome_parceiro'] = $_GET['newname'];
            }
            
            if (isset($_GET['newdesc'])) {
                $conn->alterarDescricaoParceiro($_GET['newdesc'], $_SESSION['id_parceiro']);
                $_SESSION['descricao_parceiro'] = $_GET['newdesc'];
            }
            if (isset($_POST['removerPost'])) {
                $conn->removerPromocao($_POST['removerPost']);
            }

            $data['promocao'] = $conn->listarPromocoesPainel($_SESSION['id_parceiro']);
            $data['postagem'] = $conn->listarPostagemParceiroPainel($_SESSION['id_parceiro']);  

            if(isset($_FILES['fileToUpload'])){
                upload($_FILES['fileToUpload'],"/parceiros/");
            }

            $this->load->view('parceiros',$data);
        }else{
            header('Location:'.base_url().'index.php/Login/parceiros');
        }

    }

    public function perfil($perfil){
        $conn = new Conn;
        $data['promocao'] = $conn->listarPromocoes($perfil);
        $data['postagem'] = $conn->listarPostagemParceiro($perfil);        
        $data['parceiro'] = $conn->infoParceiros();
        if(isset($_SESSION['id'])){
            $db = new Conn();
            $data['semanal']= $db->rankingSemanalPessoal($_SESSION['id']);
            $data['mensal']= $db->rankingMensallPessoal($_SESSION['id']);
            $data['total']= $db->rankingTotalPessoal($_SESSION['id']);
                $moeda= $db->listarapenasmoeda($_SESSION['id'])->fetchAll();
                $_SESSION['moeda'] = $moeda[0][0];

            $notificacao = new Notificacao();
            $data['notificacao']=  $notificacao->listarNotificacao($_SESSION['id'],0);
            $data['paginacao']=  $notificacao->listarnotificacaototal($_SESSION['id']);
            $data['novas']=  $notificacao->novasnotificacoes($_SESSION['id']);
            $data['pontuacao']=  $db->listarpontuacoes($_SESSION['id']);
        }
                $data['nome_da_pagina']=  "Perfil - Shyme";
        $this->load->view('parceiros-perfil',$data);
        
    }
}