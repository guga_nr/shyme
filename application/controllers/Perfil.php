<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once 'Usuario.php';
require_once 'Grupo.php';
require_once 'upload_arquivos.php';
require_once 'Notificacao.php';
require_once 'functions.php';
class Perfil extends CI_Controller {



    public function aluno($perfil_aluno){
        if(isset($_SESSION['id']) || isset($_SESSION['id_parceiro'])){

            $this->load->helper('url');

            $db = new Conn();
                $data['semanal']= $db->rankingSemanalPessoal($_SESSION['id']);
                $data['mensal']= $db->rankingMensallPessoal($_SESSION['id']);
                $data['total']= $db->rankingTotalPessoal($_SESSION['id']);
                $moeda= $db->listarapenasmoeda($_SESSION['id'])->fetchAll();
                $_SESSION['moeda'] = $moeda[0][0];


            $notificacao = new Notificacao();
                $data['notificacao']=  $notificacao->listarNotificacao($_SESSION['id'],0);
                $data['paginacao']=  $notificacao->listarnotificacaototal($_SESSION['id']);
                $data['novas']=  $notificacao->novasnotificacoes($_SESSION['id']);
                $data['pontuacao']=  $db->listarpontuacoes($_SESSION['id']);


            $user = new Usuario();
                $data['moeda']=  $user->listarMoeda($_SESSION['id']);
                $data['ponto']=  $user->listarPontuacao($_SESSION['id']);
            
            $stmt = $db->pegarIdPerfil($perfil_aluno);
            $stmt = $stmt->fetchAll(PDO::FETCH_ASSOC);
            $id_perfil = $stmt[0]['cd_matricula'];
            
            $data['perfil']= $db->listarinfoperfil($perfil_aluno);
            $nomeAluno = $db->listarnomeperfil($perfil_aluno)->fetchAll();
            $data['grupoPrimario']= $db->listargruposprimariosperfil($_SESSION['id'], $id_perfil);
            $data['grupoSecundario']= $db->listargrupossecundariosperfil($_SESSION['id'], $id_perfil);
            $data['postPerfil']= $db->listarpostagemperfilaluno($perfil_aluno);


            $data['nome_da_pagina']=  "Perfil - ". $nomeAluno[0][0] . " - Shyme";
            $data['kamehameha'] = false;
            $this->load->view('perfil-aluno',$data);

        }else{
            header('Location:'.base_url().'index.php/Login');
        }
    } 



    public function notas(){
        
        if(isset($_SESSION['id'])){
            $this->load->helper('url');


                $data = array("login" => '50905710xsp', "password" => '76qq309G' );
                $data_string = json_encode($data);  
                $ch = curl_init('http://razorblade.me:7070/sigatec/Siga/auth');                       
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                    
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                   
                curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                       
                    'Content-Type: application/json',                                                 
                    'Content-Length: ' . strlen($data_string))
                );
                $result = curl_exec($ch);
                $result2 = json_decode($result, TRUE);
                
                $data['notas']= $result2;
            
            $data['nome_da_pagina']=  "Notas e Faltas - Shyme";
                    
            $db = new Conn();
                $data['semanal']= $db->rankingSemanalPessoal($_SESSION['id']);
                $data['mensal']= $db->rankingMensallPessoal($_SESSION['id']);
                $data['total']= $db->rankingTotalPessoal($_SESSION['id']);
                $moeda= $db->listarapenasmoeda($_SESSION['id'])->fetchAll();
                $_SESSION['moeda'] = $moeda[0][0];


            $notificacao = new Notificacao();
                $data['notificacao']=  $notificacao->listarNotificacao($_SESSION['id'],0);
                $data['paginacao']=  $notificacao->listarnotificacaototal($_SESSION['id']);
                $data['novas']=  $notificacao->novasnotificacoes($_SESSION['id']);
                $data['pontuacao']=  $db->listarpontuacoes($_SESSION['id']);


            $user = new Usuario();
                $data['moeda']=  $user->listarMoeda($_SESSION['id']);
                $data['ponto']=  $user->listarPontuacao($_SESSION['id']);
            $this->load->view('notas',$data);

            }else{
            header('Location:'.base_url().'index.php/Login');
        }
                
    } 

	public function index()
	{
            if(isset($_SESSION['id'])){
                $this->load->helper('url');
                $db = new Conn();
                $grupo = new Grupo();
                
                $perfil = new Perfil();
                $data['grupoPrimario']= $grupo->listarGruposPrimarios($_SESSION['id']);
                $data['grupoSecundario']= $grupo->listarGruposSecundarios($_SESSION['id']);
                $data['postPerfil']= $db->listarpostagemperfil($_SESSION['id']);
                $ids_post= $db->listarpostagemperfil($_SESSION['id'])->fetchAll();
                $moeda= $db->listarapenasmoeda($_SESSION['id'])->fetchAll();
                $_SESSION['moeda'] = $moeda[0][0];


                $data['semanal']= $db->rankingSemanalPessoal($_SESSION['id']);
                $data['mensal']= $db->rankingMensallPessoal($_SESSION['id']);
                $data['total']= $db->rankingTotalPessoal($_SESSION['id']);

                $notificacao = new Notificacao();
                $data['notificacao']=  $notificacao->listarNotificacao($_SESSION['id'],0);
                $data['paginacao']=  $notificacao->listarnotificacaototal($_SESSION['id']);
                $data['novas']=  $notificacao->novasnotificacoes($_SESSION['id']);
                $data['pontuacao']=  $db->listarpontuacoes($_SESSION['id']);

    
                $respostas[0] = 0; 

                for ($i=0; $i <  sizeof($ids_post); $i++) { 
                    if ($ids_post[$i]['tipo_postagem_cd_tipo_postagem'] == 27 ||$ids_post[$i]['tipo_postagem_cd_tipo_postagem'] == 26) {
                        $resposta = $db->listarresposta($ids_post[$i]['cd_postagem']);
                        $resposta = $resposta->fetchAll();
                        array_push($respostas,$resposta);
                    }
                }
                $data['respostas'] = $respostas;   


                $user = new Usuario();
                $data['moeda']=  $user->listarMoeda($_SESSION['id']);

                $data['nome_da_pagina']=  "Perfil - Shyme";
                

                if(isset($_POST['criarGrupo'])){
                    $imagem = '';
                    if(isset($_FILES['foto_grupo'])){
                        $imagem = upload($_FILES['foto_grupo'],"/grupo/foto/");
                    } 
                    $token = substr(md5($_POST['nmgrupo']), 4,4).substr(md5($_POST['dsgrupo']), 4,4).substr(md5($_SESSION['id']), 4,4).substr(md5( date("d-m-Y-H-i-s")), 4,4);
                    $db->criarGrupo($_POST['dsgrupo'],$_SESSION['id'],$_POST['nmgrupo'],$_POST['privacidade'], $token,$imagem);
                    $db->adicionarAdm($_SESSION['id'],$token);
                    $_SESSION['grupoAdm'] = 1;
                    alert("Grupo criado com sucesso");
                    header("Refresh:0");
                  
                }
                if(isset($_GET['gp']) && isset($_GET['gs'])){
                    $db->trocarPrioridadeGrupo($_SESSION['id'],$_GET['gp'],$_GET['gs']);
                }

                if(isset($_FILES['fileToUpload'])){
                    upload($_FILES['fileToUpload'],"/perfil/");
                    header("Refresh:0");
                } 

                if(isset($_POST['download_material'])){     
                    $this->load->helper('download');


                    $membros = 0;
                    $v = 0;
                    $ponto = 0;
                    $desconto = 0;
                    $alunoUp = $_POST['aluno_upload'];
                    $arquivo = str_replace(' ', '', $_POST['arquivo']);
                    $alunodown = $_SESSION['id'];
                    $cdPostagem = $_POST['idPost'];
                    $vpg = $db->verificarprioridadegrupo($alunodown, $_GET['id']);
                    foreach ($vpg as $vp) {
                        $v = $vp['prioridade_grupo'];
                    }
                    $qtdMembros = $db->verificarmembrosgrupo($_GET['id']);
                    foreach ($qtdMembros as $qtd) {
                        if ($qtd['count(*)'] <= 20 && $v = 1) 
                            $ponto = 3;
                        if ($qtd['count(*)'] <= 20  && $v = 0) 
                            $ponto = 2;
                        if ($qtd['count(*)'] <= 40 && $qtd['count(*)'] > 20 && $v = 1) 
                            $ponto = 3;
                        if ($qtd['count(*)'] <= 40 && $qtd['count(*)'] > 20 && $v = 0)
                            $ponto = 2; 
                        if ($qtd['count(*)'] > 40 && $v = 1) 
                            $ponto = 4;
                        if ($qtd['count(*)'] > 40 && $v = 0) 
                            $ponto = 3;
                    }
                    $stmt = $db->download($alunoUp, $alunodown, 1, $ponto, $cdPostagem);
                    if($stmt == 1){
                        $data = file_get_contents(asset_url() . $arquivo); 
                        $name = str_replace('img/material/upload', '',$arquivo) ;
                        $result = $db->listarMoeda($alunodown);
                        $newmoeda = $result->fetchAll(PDO::FETCH_COLUMN, 0);
                        $_SESSION['moeda'] = $newmoeda[0];
                        force_download($name, $data);
                    }else
                        alert("Você não tem moedas o suficiente para este download");
                }    


                $data['kamehameha'] = true;
                $this->load->view('perfil',$data);
	} else{
            header('Location:'.base_url().'index.php/Login');
        
    }
    
}
}
