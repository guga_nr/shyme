<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
require_once 'Usuario.php';
require_once 'Grupo.php';
class Notificacao extends CI_Controller {

    public $n;
    
    public function listarNotificacao($cdAluno, $pagina){
        $db = new Conn();
        $n = $db->listarNotificacao($cdAluno, $pagina);
        $result = $n->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }
    
    public function listarnotificacaototal($cdAluno){
        $db = new Conn();
        $n = $db->listarnotificacaototal($cdAluno);
        $result = $n->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }
    public function novasnotificacoes($cdAluno){
        $db = new Conn();
        $n = $db->novasnotificacoes($cdAluno);
        $result = $n->fetchAll();
        return $result;
    }
    public function semilidas($cdAluno){
        $db = new Conn();
        $n = $db->semilidas($cdAluno);
        $result = $n->fetchAll();
        return $result;
    }

    public function index(){
        if(isset($_SESSION['id'])){
            
            if (!isset($_GET['p'])){
                header('Location:'.base_url().'index.php/Notificacao?p=1');
            }else{
                $this->load->helper('url');
                $pagina = 0;
                if($_GET['p'] > 1)
                    $pagina = ($_GET['p'] - 1) * 10;

                $db = new Conn();
                
                $data['nome_da_pagina']=  "Notificações - Shyme";
                
                $db = new Conn();
                $data['semanal']= $db->rankingSemanalPessoal($_SESSION['id']);
                $data['mensal']= $db->rankingMensallPessoal($_SESSION['id']);
                $data['total']= $db->rankingTotalPessoal($_SESSION['id']);
                $moeda= $db->listarapenasmoeda($_SESSION['id'])->fetchAll();
                $_SESSION['moeda'] = $moeda[0][0];

                $notificacao = new Notificacao();
                $data['notificacao']=  $notificacao->listarNotificacao($_SESSION['id'],$pagina);
                $data['paginacao']=  $notificacao->listarnotificacaototal($_SESSION['id']);
                $data['novas']=  $notificacao->novasnotificacoes($_SESSION['id']);
                $data['pontuacao']=  $db->listarpontuacoes($_SESSION['id']);


                $user = new Usuario();
                    $data['moeda']=  $user->listarMoeda($_SESSION['id']);
                    $data['ponto']=  $user->listarPontuacao($_SESSION['id']);

                // if (isset($_GET['id'])) {
                //     $data['umaNotificacao']=  $db->listarUmaNotificacao($_SESSION['id'],$_GET['id']);
                // }

                if (isset($_POST['solicitacaoGrupo'])) {
                    $bacon = explode('-', $_POST['solicitacaoGrupo']);
                    $id = $db->getidgrupo($bacon[1]);
                    $idgrupo = $id->fetchAll();
                     $db->alterarnotificacao($bacon[2]);
                    if($bacon[0] == 1 ){
                        $db->adicionarmembro($bacon[2], 1, $idgrupo[0][0] );
                        $db->enviarnotificacao($bacon[2], $idgrupo[0][0],0,1);
                    }else
                        $db->enviarnotificacao($bacon[2], $idgrupo[0][0],0,15);

                    $db->alterarnotificacao($bacon[1]);
                }

                if (isset($_POST['notificacaolida'])) {
                    $db->notificacaolida($_POST['notificacaolida']);
                }

                if (isset($_POST['notificacaosemilida'])) {
                    $db->notificacaosemilida($_SESSION['id']);
                }

                if(isset($_POST['addmembro'])){
                    $id_add = explode(',', $_POST['addmembro']);
                    $db = new Conn(); 
                    for ($i=1; $i < sizeof($id_add); $i++){
                        $db->adicionarmembro($id_add[$i], 1, $_GET['id'] );
                    $result = $db->enviarnotificacao($id_add[$i], $_GET['id'],0,1);
                    }
                    
                }
                
                $this->load->view('notificacao',$data);
            }                
        }else{
            header('Location:Login');
        }
    }

}

