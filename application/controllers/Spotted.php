<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once 'Conn.php';
require_once 'Usuario.php';
require_once 'upload_arquivos.php';

require_once 'Notificacao.php';
class Spotted extends CI_Controller{
    

public function comentario(){   
    if(isset($_SESSION['id'])){
        $db = new Conn();

        $data['curto'] = $db->listarspottedcomentariocurto($_GET['id']);

        $data['comentario'] = $db->listarspottedcomentario($_GET['id']);

                
        $notificacao = new Notificacao();
        $data['notificacao']=  $notificacao->listarNotificacao($_SESSION['id'],0);
        $data['paginacao']=  $notificacao->listarnotificacaototal($_SESSION['id']);
        $data['novas']=  $notificacao->novasnotificacoes($_SESSION['id']);
        
        foreach ($data['notificacao'] as $key) {
            $notificacao->semilidas($key['cd_notificacao']);
        }
                
        $this->load->helper('url');
        $this->load->view('comentario-spotted',$data);

        if(isset($_POST['submitR'])){
            $db->adicionarComentario($_POST['txt_content_post'],$_SESSION['id'],$_GET['id']);
            header('Refresh:0');
        }

    }else{
        header("Location:Login");
    }

}

public function index(){
         if(isset($_SESSION['id'])){
            $db = new Conn();
            $data['resultadoP'] = $db->listarSpottedPostagem();
            $rP = $db->listarSpottedPostagem();
            $i = 0;

            $user = new Usuario();
                $data['moeda']=  $user->listarMoeda($_SESSION['id']);
                $data['ponto']=  $user->listarPontuacao($_SESSION['id']);



            $notificacao = new Notificacao();

                $data['notificacao']=  $notificacao->listarNotificacao($_SESSION['id'],0);
                $data['paginacao']=  $notificacao->listarnotificacaototal($_SESSION['id']);
                $data['novas']=  $notificacao->novasnotificacoes($_SESSION['id']);
                
                foreach ($data['notificacao'] as $key) {
                    $notificacao->semilidas($key['cd_notificacao']);
                }
                
            foreach($rP as $resP){
                $numeroD[$i] = $resP['cd_postagem_spotted'];
                $i++;                
            }
                $i = 0;
                if(isset($numeroD)){
                    foreach($numeroD as $nD){
                        $data['resultadoR'][$i] = $db->listarSpottedComentarioCurto($nD);
                        $i++;

                }
            } 




            $this->load->helper('url');
            $this->load->view('spotted',$data);

            if(isset($_POST['spotted-post'])){
                $imagem = "";

                if(isset($_FILES['spotted_imagem'])){
                    $imagem = upload($_FILES['spotted_imagem'],"/spotted/");
                }

                $texto = $_POST['txt_content_spotted_post'];
                $idAluno = $_SESSION['id']; 
                $nick = $_POST['nick-spotted'];

                $ver = $db->verificarSpottedDia($idAluno);
                if($ver->rowCount()==0){   
                    $comu = $db->adicionarPostSpotted($idAluno, $texto, $imagem, $nick);                
                    if($comu !== 0){
                       header("Refresh:0");
                    }else{
                        echo "<script>alert('Vish...Ocorreu um erro, tente novamente mais tarde!');</script>";
                    }
                }else{
                    header("Refresh:0");
                    echo "<script>alert('Você já postou no spotted hoje!".$nick."');</script>";
                    }
            }

        }else{
            header('Location:Login');
        }
    }


}
