<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once 'Usuario.php';
require_once 'Notificacao.php';
require_once 'upload_arquivos.php';
require_once 'functions.php';
class Grupo extends CI_Controller{
    
    private $nmGrupo;
    private $cdGrupo;
    private $cdMembros;
    private $dsGrupo;
    private $cdAdmGrupo;
    private $cdUsuario;

    public function listarMembros($cdGrupo)
    {
        $conn = new Conn;
        $stmt = $conn->selectmembers($cdGrupo);
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }
     public function listarGrupos($cdUsuario)
    {
        $conn = new Conn;
        $stmt = $conn->selectallgroups($cdUsuario);
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

     public function listarinfogrupos($cdGrupo)
    {
        $conn = new Conn;
        $stmt = $conn->listarinfogrupo($cdGrupo);
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }
    
    public function listarGruposPrimarios($cdUsuario)
    {
        $conn = new Conn;
        $stmt = $conn->listargruposprimarios($cdUsuario);
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }
     public function listarGruposSecundarios($cdUsuario)
    {
        $conn = new Conn;
        $stmt = $conn->listargrupossecundarios($cdUsuario);
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    public function adicionarMembro($cdGrupo, $cdUsuario){
        $conn = new Conn;
        $result = $conn->enviarnotificacao($cdUsuario,$cdGrupo, null , 1);
        if ($result->rowCount() !== 0) {
            echo "Adicionado com sucesso!";
        } else {
            echo "Erro no banco de dados.";
        }
    }

public function index(){
   if(isset($_SESSION['id'])){
        
        $data['nome_da_pagina']=  "Grupo - Shyme";
        $data['pagina']=  "post";
                $db = new Conn();
                $moeda= $db->listarapenasmoeda($_SESSION['id'])->fetchAll();
                $_SESSION['moeda'] = $moeda[0][0];


                $data['semanal']= $db->rankingSemanalPessoal($_SESSION['id']);
                $data['mensal']= $db->rankingMensallPessoal($_SESSION['id']);
                $data['total']= $db->rankingTotalPessoal($_SESSION['id']);

                $notificacao = new Notificacao();
                $data['notificacao']=  $notificacao->listarNotificacao($_SESSION['id'],0);
                $data['paginacao']=  $notificacao->listarnotificacaototal($_SESSION['id']);
                $data['novas']=  $notificacao->novasnotificacoes($_SESSION['id']);
                $data['pontuacao']=  $db->listarpontuacoes($_SESSION['id']);

        
        $conn = new Conn();
        $grupoAdmVer = 0;
        $data['solicitacao'] = true;
        $data['infoN']= $conn->listarinfogrupo($_GET['id']);
        $vag = $conn->verificaratividadegrupo($_GET['id'])->fetchAll();
        $vaga = $conn->verificaratividadegrupoaluno($_GET['id'], $_SESSION['id']);
        $privacidadeGrupo = $conn->verificarPrivacidadeGrupo($_GET['id']);
        $administradores = $conn->listarAdm($_GET['id']);

        $data['administrador'] = false;
        if (isset($administradores)):
            foreach ($administradores as $ad) {
                if($ad['cd_matricula'] == $_SESSION['id'])
                    $data['administrador'] = true;
            }   
        endif;
      

            $user = new Usuario();
            $data['moeda']=  $user->listarmoeda($_SESSION['id']);
            $data['ponto']=  $user->listarpontuacao($_SESSION['id']);
                $moeda= $conn->listarapenasmoeda($_SESSION['id'])->fetchAll();
                $_SESSION['moeda'] = $moeda[0][0];

            $notificacao = new Notificacao();
            $db = new Conn();
            $data['notificacao']=  $notificacao->listarNotificacao($_SESSION['id'],0);
            $data['paginacao']=  $notificacao->listarnotificacaototal($_SESSION['id']);
            $data['novas']=  $notificacao->novasnotificacoes($_SESSION['id']);
            $data['pontuacao']=  $db->listarpontuacoes($_SESSION['id']);

        if ($vag[0][0] ==0) {
            header("Location:".base_url()."index.php/Perfil");
        }

        if ($vaga->rowCount()==0) {
            foreach($privacidadeGrupo as $prig){               
                if ($prig['ic_privado'] == 0) {
                   // header("Location:'.base_url().'index.php/Grupo?id=".$_GET['id']."&pg=2");
                    $data['solicitacao'] = false;
                }else{
                    header('Location:'.base_url().'index.php/Perfil');
                }
            }
        }else{
            foreach ($vaga as $vaga1) {
                if ($vaga1['ic_aluno_grupo'] == 0) {
                    foreach($privacidadeGrupo as $prig){               
                        if ($prig['ic_privado'] == 0) {
                            $data['solicitacao'] = false;
                            if(!isset($_GET['pg']))
                                header("Location:".base_url()."index.php/Grupo?id=".$_GET['id']."&pg=2");
                        }else{
                            header('Location:'.base_url().'index.php/Perfil');
                        }
                    }
                }
            }
        }

        if($_SESSION['grupoAdm'] == 1){
            $verifyAdm = $conn->selectverifyadm($_SESSION['id'],$_GET['id']);
            if($verifyAdm->rowCount()>0){
                $grupoAdmVer = 1;
            }else{
                $grupoAdmVer = 0;
            }
        }

        if(isset($_POST['sairGrupo'])){                   
            $db1 = new Conn();
            // $caralho = $db1->excluiradm($_GET['id'],$_SESSION['id']);
            $caralho = $db1->sairgrupo($_GET['id'],$_SESSION['id']);
        
        }

        if(isset($_POST['addadm'])){
            $db = new Conn();
            $result = $db->nomearadm($_POST['addadm'],$_GET['id']);
            $result = $db->enviarnotificacao($_POST['addadm'], $_GET['id'],0,9);
        }

        if(isset($_POST['addmembro'])){
            $id_add = explode(',', $_POST['addmembro']);
            $db = new Conn(); 
            for ($i=1; $i < sizeof($id_add); $i++){
                $db->adicionarmembro($id_add[$i], 1, $_GET['id'] );
            $result = $db->enviarnotificacao($id_add[$i], $_GET['id'],0,1);
            }
            
        }
        if(isset($_FILES['fileToUpload'])){
           upload($_FILES['fileToUpload'],"/grupo/");
        }



        if(isset($_POST['excluirGrupo']) && $grupoAdmVer == 1){
            $db1 = new Conn();
            $a =  $db1->excluirgrupo($_GET['id']);
            $db1->enviarnotificacao(0,$_GET['id'],0,2);
        }


        if(isset($_GET['remover'])){
            $idpost = $_GET['remover'];
            $idgru = $_GET['id'];
            $db1 = new Conn();
            $db1->excluirpostagem($idpost);
            $db1->enviarnotificacao(0,$_GET['id'],$idpost,3);
        }
        if(isset($_POST['removerMembro'])){
            echo $_POST['removerMembro'];
            $db1 = new Conn();
            $db1->excluirmembro($_POST['removerMembro'],$_GET['id']);
            $db1->enviarnotificacao($_POST['removerMembro'],$_GET['id'],0,13);
        }

        if(isset($_POST['dsgru']) || isset($_POST['nomegru'])){
            $db = new Conn();
            $db->alterarinfogrupo($_GET['id'],$_POST['dsgru'],$_POST['nomegru'], $_POST['privacidade']);
            
            $prepare = $db->pegarIdGrupo($_GET['id']);
            $ids = $prepare->fetchAll();
            
            for($i = 0; $i < sizeof($ids); $i++)
                $db->enviarnotificacao($ids[$i][0],$_GET['id'],0,12);
        }
       
        if(isset($_POST['solicitacaoGrupo'])){
            $db = new Conn();
            $prepare = $db->listarAdm($_GET['id']);
            $ids = $prepare->fetchAll();
            for($i = 0; $i < sizeof($ids); $i++)
                $db->enviarnotificacao($ids[$i][0],$_GET['id'],$_SESSION['id'],10);
        }
        if(isset($_GET['pg'])){
           if($_GET['pg']==2){
                $grupo = new Grupo();
                $data['membros']= $grupo->listarMembros($_GET['id']);
                $data['info']= $grupo->listarinfogrupos($_GET['id']);
                $data['pagina']=  "membros";

                $this->load->helper('url');     
                $this->load->view('grupo-membro',$data);                    
            }
        } else{
          
            $pagina = 0;
            if(isset($_GET['p']) && $_GET['p'] > 1)
                $pagina = ($_GET['p'] - 1) * 10;

            $db = new Conn();
            $grupo = new Grupo();
            $data['resultado']= $grupo->listarGrupos($_SESSION['id']);
            $data['resultadoP'] = $db->listarpostagem($_GET['id'], $pagina);
            $ids_post = $db->listarpostagem($_GET['id'], $pagina)->fetchAll();            

            $respostas[0] = 0; 

            for ($i=0; $i <  sizeof($ids_post); $i++) { 
                if ($ids_post[$i]['tipo_postagem_cd_tipo_postagem'] == 27 ||$ids_post[$i]['tipo_postagem_cd_tipo_postagem'] == 26) {
                    $resposta = $db->listarresposta($ids_post[$i]['cd_postagem']);
                    $resposta = $resposta->fetchAll();
                    array_push($respostas,$resposta);
                }
            }
            $data['respostas'] = $respostas;  

            $data['paginacao']=  $db->listarpostagemtotal($_GET['id']);

            $this->load->helper('url');
            $this->load->view('grupo',$data);
            



            
    
            
        if(isset($_POST['comunicado'])){
            $tpPost = "";
            $imagem = "";
            if(isset($_FILES['imagem_post'])  && !empty($_FILES['imagem_post']['name'])){ 
                $imagem = upload($_FILES['imagem_post'],"/comunicado/");
            }
            $texto = $_POST['txt_content_post'];
            $idGrupo = $_GET['id'];
            $idAluno = $_SESSION['id']; 
            $pg = $db->verificarprioridadegrupo($_SESSION['id'],$_GET['id']);

            foreach ($pg as $p){
                if($p['prioridade_grupo'] == 1){
                    $tpPost = 32;
                }else{
                    $tpPost = 33;
                }
            }              

            $moeda = $db->verificarmoeda($_SESSION['id'],$tpPost);
             //print_r($moeda);
            foreach ($moeda as $m) {
                if ($m['teste'] == 1) {
                    
                        $comu = $db->adicionarcomunicado($idAluno, $texto, $idGrupo, $imagem,$tpPost);
                        if($comu !== 0){
                           header("Refresh:0");
                        }else{
                            echo "<script>alert('Não foi possível postar!');</script>"; 
                        }
                }else{ 
                    echo "<script>alert('Você não tem moedas o suficiente :(');</script>";
                    header("Refresh:0");
                }
            }
        }



        if(isset($_POST['duvida'])){
            $imagem = "";
            $texto = $_POST['txt_content_post'];
            $arquivo = "";
            $idGrupo = $_GET['id'];
            $idAluno = $_SESSION['id']; 
            $cdMoeda = $_POST['moeda_duvida'];
            $vl_moeda = 0;
            if($cdMoeda == 29)
                $vl_moeda =  -5;
            if($cdMoeda == 30)
                $vl_moeda =  -10;
            if($cdMoeda == 31)
                $vl_moeda =  -15;
            
            if(isset($_FILES['imagem_post'])  && !empty($_FILES['imagem_post']['name'])){
                $imagem = upload($_FILES['imagem_post'],"/duvida/");
            } 

            $pg = $db->verificarprioridadegrupo($_SESSION['id'],$_GET['id']);
            foreach ($pg as $p){
                if($p['prioridade_grupo'] == 1){
                    $tpPost = 26;
                }else{
                    $tpPost = 27;
                }
            } 

            $verificar = $db->verificarduvidadia($idAluno,$idGrupo);

            if(true){  
            // if($verificar->rowCount()==0){  
                if ($_SESSION['moeda'] > $vl_moeda) {
                    $comu = $db->adicionarduvida($idAluno, $texto, $idGrupo, $imagem,$cdMoeda,$tpPost);
                    if($comu !== 0){       
                        atualizaMoedaAluno($vl_moeda, $_SESSION['id']);         
                        header("Refresh:0");
                    }else{
                        echo "<script>alert('Não foi possível postar!');</script>"; 
                    }
                }else{ 
                    echo "<script>alert('Você não tem moedas o suficiente :(');</script>";               
                    header("Refresh:0");
                }
           }else{
                 echo "<script>alert('Você já postou uma dúvida hoje!');</script>";                
                header("Refresh:0");
            }    
        }

        if(isset($_POST['upload'])){
            $tpPost = 0;
            $imagem = "";
            $texto = $_POST['txt_content_post'];
            $arquivo = "";
            $idGrupo = $_GET['id'];
            $idAluno = $_SESSION['id']; 
            if(isset($_FILES['imagem_post']) && !empty($_FILES['imagem_post']['name'])){
                $imagem = upload($_FILES['imagem_post'],"/material/");
            }

            if(isset($_FILES['material_post'])  && !empty($_FILES['material_post']['name'])){
                $arquivo = false;
                $arquivo = upload($_FILES['material_post'],"/material/upload/");
            }
            $vpg = $db->verificarprioridadegrupo($_SESSION['id'],$_GET['id']);
            foreach ($vpg as $vp) {
                $v = $vp['prioridade_grupo'];
            }
            $qtdMembros = $db->verificarmembrosgrupo($_GET['id']);
            foreach ($qtdMembros as $qtd) {
                if ($qtd['count(*)'] <= 20 && $v = 1) 
                    $tpPost = 10;
                if ($qtd['count(*)'] <= 20 && $v = 0) 
                    $tpPost = 13;
                if ($qtd['count(*)'] <= 40 && $qtd['count(*)'] > 20 && $v = 1) 
                    $tpPost = 11;
                if ($qtd['count(*)'] <= 40 && $qtd['count(*)'] > 20 && $v = 0) 
                    $tpPost = 14;
                if ($qtd['count(*)'] > 40 && $v = 1) 
                    $tpPost = 12;
                if ($qtd['count(*)'] > 40 && $v = 0) 
                    $tpPost = 15;
            }

            $moeda = $db->verificarmoeda($_SESSION['id'],$tpPost);
            foreach ($moeda as $m) {
                if ($m['teste'] == 1) {
                    if($arquivo){
                        $material = $db->adicionarmaterial($imagem,$idAluno, $texto, $idGrupo,$tpPost,$arquivo);        
                        header("Refresh:0");
                    }
                }else{
                    alert("Os arquivos tem que ter no máximo 20MB");       
                    header("Refresh:0");
                }
            }   
              
        }





        if(isset($_POST['download_material'])){     
               $this->load->helper('download');


            $membros = 0;
            $v = 0;
            $ponto = 0;
            $desconto = 0;
            $alunoUp = $_POST['aluno_upload'];
            $arquivo = str_replace(' ', '', $_POST['arquivo']);
            $alunodown = $_SESSION['id'];
            $cdPostagem = $_POST['idPost'];
            $vpg = $db->verificarprioridadegrupo($alunodown, $_GET['id']);
            foreach ($vpg as $vp) {
                $v = $vp['prioridade_grupo'];
            }
            $qtdMembros = $db->verificarmembrosgrupo($_GET['id']);
            foreach ($qtdMembros as $qtd) {
                if ($qtd['count(*)'] <= 20 && $v = 1) 
                    $ponto = 3;
                if ($qtd['count(*)'] <= 20  && $v = 0) 
                    $ponto = 2;
                if ($qtd['count(*)'] <= 40 && $qtd['count(*)'] > 20 && $v = 1) 
                    $ponto = 3;
                if ($qtd['count(*)'] <= 40 && $qtd['count(*)'] > 20 && $v = 0)
                    $ponto = 2; 
                if ($qtd['count(*)'] > 40 && $v = 1) 
                    $ponto = 4;
                if ($qtd['count(*)'] > 40 && $v = 0) 
                    $ponto = 3;
            }
            $stmt = $db->download($alunoUp, $alunodown, 1, $ponto, $cdPostagem);
            if($stmt == 1){
                $data = file_get_contents(asset_url() . $arquivo); 
                $name = str_replace('img/material/upload', '',$arquivo) ;
                $result = $db->listarMoeda($alunodown);
                $newmoeda = $result->fetchAll(PDO::FETCH_COLUMN, 0);
                $_SESSION['moeda'] = $newmoeda[0];
                force_download($name, $data);
            }else
                alert("Você não tem moedas o suficiente para este download");
        }    



    


                 
    }
    }else{
        header('Location:'.base_url().'index.php/Login');
    }
}


    public function index2($cdUsuario, $cdGrupo){ 
        adicionarMembro($cdGrupo, $cdUsuario);
    }
    
    public function index3($cdUsuario, $cdGrupo){
        tornarAdministrador($cdUsuario, $cdGrupo);
    }
}