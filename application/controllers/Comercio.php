<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once 'Usuario.php';
require_once 'Grupo.php';
require_once 'Notificacao.php';
class c extends CI_Controller {

	public function index()
	{
            if(isset($_SESSION['id'])){
                $this->load->helper('url');
           //     if($_SESSION['primeira']==1){
        //       echo "bem-vindo";
          //      }
                $db = new Conn();
                $grupo = new Grupo();
                
                $perfil = new Perfil();
                $data['grupoPrimario']= $grupo->listarGruposPrimarios($_SESSION['id']);
                $data['grupoSecundario']= $grupo->listarGruposSecundarios($_SESSION['id']);
                $data['postPerfil']= $db->listarPostagemPerfil($_SESSION['id']);

                $notificacao = new Notificacao();
                $data['notificacao']=  $notificacao->listarNotificacao($_SESSION['id']);



                $user = new Usuario();
                $data['moeda']=  $user->listarMoeda($_SESSION['id']);
                $data['ponto']=  $user->listarPontuacao($_SESSION['id']);


                if(isset($_GET['nmgrupo']) && isset($_GET['dsgrupo']) && isset($_GET['privacidade'])){
                    $db->criarGrupo($_GET['dsgrupo'],$_SESSION['id'],$_GET['nmgrupo'],$_GET['privacidade']);
                }
                if(isset($_GET['gp']) && isset($_GET['gs'])){
                    $db->trocarPrioridadeGrupo($_SESSION['id'],$_GET['gp'],$_GET['gs']);
                }

                if(isset($_POST['enviarArq'])){
                  if(isset($_FILES['img-perfil'])){
                    $this->anexarArquivo($_FILES['img-perfil'], $_SESSION['id']);
                    $db->updateImagePerfil($_FILES['img-perfil']['temp'], $_SESSION['id']);
                }

                
            }
                $this->load->view('perfil',$data);
	} else{
            header('Location:Login');
        }
}
}
