<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once 'Usuario.php';
require_once 'Conn.php';
require_once 'Notificacao.php';
class ComentarioSpotted extends CI_Controller {
    
    public function index()
    {   if(isset($_SESSION['id'])){
        $db = new Conn();

        $data['umcomentario'] = $db->listarUmaPostagem($_GET['id']);
        $data['comentario'] = $db->listarResposta($_GET['id']);

                
                $notificacao = new Notificacao();
                $data['notificacao']=  $notificacao->listarNotificacao($_SESSION['id']);
                
        $resId = $db->listarUmaPostagem($_GET['id']); //Pegando o ALUNO_GRUPO para
        $resId2 = $resId->fetch(PDO::FETCH_ASSOC);    // verificar se o Usuario é o dono da Pergunta
        
        $donoDuvida = $db->verificarDonoDuvida($resId2['cd_aluno_grupo']); //Usando o Aluno Grupo para 
        $idAluno = $donoDuvida->fetch(PDO::FETCH_ASSOC); // descobrir o CD_MATRICULA de quem postou

        
        $this->load->helper('url');
        $this->load->view('comentario-potted',$data);

        if(isset($_POST['submitR'])){
        	$db->adicionarComentario($_POST['txt_content_post'],$_SESSION['id'],$_GET['id']);
        	header('Location:Duvida?id='.$_GET['id']);
        }

    else{
        header("Location:Login");
    }
    }

}
