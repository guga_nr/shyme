<?php $this->load->view("elements/head");  ?>

        <div class="container-fluid corpo" >
            
            <div class="row">
               <div class="col-md-12">
               <h1 style="text-align:center">Manual</h1>
                <div class="col-md-offset-2 col-md-4" id="manual">
                    <div class="col-md-12 titulo-manual" id="Login">Login
                        <div class="col-md-12 ds-manual" id="Loginds">
                       1.1 O login dos alunos é apenas realizado através da conta do SIGA fornecida pela faculdade. Não há necessidade de cadastro.
                        <br>1.2 O login de parceiros precisa de um cadastro, feito pelo próprio usuário no site.
                        <br>1.3 Para fazer o logout, basta clicar na seta ao lado do nome do usuário no cabeçalho do site e escolher a opção "Sair".
                        </div>
                    </div>


                      <div class="col-md-12 titulo-manual" id="Cadastro">Cadastro
                        <div class="col-md-12 ds-manual" id="Cadastrods">
                        1.1 O login dos alunos é apenas realizado através da conta do SIGA fornecida pela faculdade. Não há necessidade de cadastro.
                        <br>1.2 O login de parceiros precisa de um cadastro, feito pelo próprio usuário no site.
                        <br>1.3 Para fazer o logout, basta clicar na seta ao lado do nome do usuário no cabeçalho do site e escolher a opção "Sair".
                        </div>
                    </div>

                      <div class="col-md-12 titulo-manual" id="Personalizacao">Personalizacao
                        <div class="col-md-12 ds-manual" id="Personalizacaods">
                        3.1 Para alterar a foto de perfil, é necessário clicar nela, escolher uma foto de seu computador ou celular e clicar em "Salvar".
                    </div>
                    </div>

                      <div class="col-md-12 titulo-manual" id="Grupos">Grupos
                        <div class="col-md-12 ds-manual" id="Gruposds">
                        4.1 Para criar um grupo, é necessário clicar no botão "+" acima da lista de grupos. Abrirá uma janela com os dados necessários para criação do grupo: Nome; Descrição; Privacidade; Imagem; Prioridade.
                        <br>4.2 Existem 2 tipos de grupos, principais e secundários.
                        <br>4.2.1 Nos grupos principais, o usuário gasta mais moedas para postar e tem um retorno maior.
                        <br>4.2.1.1 Descrição dos pontos***
                       <br> 4.2.2 Nos grupos secundários, o usuário gasta menos moedas para postar e tem um retorno menor.
                        <br>4.2.2.1 Descrição dos pontos***
                        <br>4.2.3 Para trocar a prioridade dos grupos, é necessário clicar na engrenagem acima da lista de grupos, selecionar os dois grupos de troca e clicar em "trocar".
                       <br>4.2.4 Outra maneira de trocar a prioridade dos grupos, é só arrastar os grupos de acordo com a posição que deseja deixar.
                        <br>4.2.5 O aluno só pode trocar um grupo repetido uma vez por semana.
                        <br>4.2.6 Para visualizar postagens de um grupo, é necessário clicar em um grupo de sua lista.
                        <br>4.2.7 Para visualizar os membros e as informações do grupo, é necessário clicar na aba "Grupo" abaixo do nome do grupo.
                        <br>4.2.8 Para sair do grupo, basta clicar no botão "Sair do Grupo" e confirmar a decisão.
                        <br>4.2.9 Para acessar o perfil de um aluno, é necessário clicar em seu nome ou foto tanto na lista de membros, quanto na lista de postagens.
                        <br>4.2.11 Administradores tem funções exclusivas no grupo.
                        <br>4.2.11.1 Para excluir qualquer postagem, é necessário o administrador clicar no "x" que aparece no canto superior direito dá postagem após passar o mouse em cima.
                        <br>4.2.11.2 Para mudar os dados do grupo, é necessário clicar no botão "Alterar Informações" e preencher os campos necessários: Nome; Privacidade; Descrição;
                        <br>4.2.11.3 Para adicionar um novo membro, é necessário clicar no botão "Adicionar Membro". Abrirá uma nova janela com um campo de busca. Após escrever o nome do aluno e seleciona-lo, clique em "Adicionar".
                        <br>4.2.11.4 Para remover um membro, é necessário o administrador clicar no "x" que aparece no canto superior direito da imagem do aluno após passar o mouse em cima.
                        <br>4.2.11.5 Para excluir o grupo, basta clicar no botão "Excluir Grupo" e confirmar a decisão.
                        <br>4.2.11.6 Para alterar a foto do Grupo, é necessário clicar nela, escolher uma foto de seu computador ou celular e clicar em "Salvar".
                        </div>
                </div>

                      <div class="col-md-12 titulo-manual" id="Postagens">Postagens
                        <div class="col-md-12 ds-manual" id="Postagensds">
                       5.1 Existem 3 tipos de postagem: Comunicado; Dúvida; Material. 
                        <br>5.1.1 Os comunicados não retornam valor algum para o aluno. Serve apenas para divulgar uma mensagem.
                        <br>5.1.1.1 Para postar um comunicado, é necessário clicar em "Postar Comunicado"  e preencher os campos necessários: Descrição; Imagem (não é necessário).
                        <br>5.1.2 As dúvidas, como o nome já diz, serve para perguntar algo e esperar que um usuário responda sua a mesma, sendo necessário oferecer uma quantia de moedas, com os valores padrões de: 5; 10; 15.
                        <br>5.1.2.1 Para postar uma Dúvida, é necessário clicar em "Postar Dúvida"  e preencher os campos necessários: Descrição; Imagem (não é necessário); Quantia de moeda que será direcionada para quem responder corretamente;
                       <br> 5.1.2.2 Para responder uma dúvida, é necessário acessar  a página de postagens do grupo e clicar no botão "Responder" de alguma dúvida. O aluno será redirecionado para uma página apenas com respostas, e terá um campo para digitar sua resposta.
                        <br>5.1.2.3 Para selecionar uma resposta como correta, é necessário ser o dono da dúvida e clicar na estrela dá resposta que deseja escolher como correta.
                        <br>5.1.3 A postagem de material serve para compartilhar algum arquivo, tendo um retorno de moeda para cada download feito do arquivo.
                        <br>5.1.3.1 Para postar um Material, é necessário clicar em "Postar Material"  e preencher os campos necessários: Descrição; Imagem (não é necessário); Arquivo que será disponibilizado para download;
                        <br>5.1.3.2 Para baixar um material, é necessário acessar a página de postagens do grupo, localizar uma postagem do tipo "Material" e clicar no botão "Baixar".
                        <br>5.1.4 Para excluir uma postagem, é necessário ser dono dá mesma e clicar no "X" do canto superior direito do campo da postagem.
                        <br>5.1.5 Existe também, a postagem de promoções, exclusiva dos parceiros, onde eles postam semanalmente suas promoções.
                        <br>5.1.5.1 Para postar uma promoção, é necessário clicar no campo de postagem no perfil e digitar a mensagem.
                    </div>
                    </div>

                    <div class="col-md-12 titulo-manual" id="Pontuacao">Pontuacao
                        <div class="col-md-12 ds-manual" id="Pontuacaods">
                      6.1 Para fazer postagens e compartilhar suas ideias, e necessário ter quantidades equivalentes de moedas para cada ação.
                        <br>6.2 É possível ganhar moedas de acordo com suas ações no site.
                        <br>6.2.1 Usuários baixando seu material.
                        <br>6.2.2 Respondendo dúvidas.
                        <br>6.2.3 Tirando notas acima de 8.
                        <br>6.3 Não há possibilidade de perder pontos. Toda moeda que você ganha com ações, se torna ponto.
                        <br>6.4 Toda semana, os 3 primeiros colocados recebem um código promocional para usar com os comércios participantes dá região.
                        <br>6.5 Todo mês, os 3 primeiros colocados recebem benefícios dá faculdade.
                        <br>6.6 Para validar esses códigos, basta levar um print com o código e o RA ao comércio ou a secretária dá faculdade.
                    </div>
                    </div>

                    <div class="col-md-12 titulo-manual" id="Pesquisa">Pesquisa
                        <div class="col-md-12 ds-manual" id="Pesquisads">
                      7.1 Para pesquisar por algo, basta clicar no campo de pesquisa no cabeçalho e digitar o que busca e o usuário será redirecionado para uma página.
<br>7.2 Nessa página, é listado o resultado da busca em 3 tipos: Usuários; Grupos; Parceiros.
                    </div>
                    </div>


                    <div class="col-md-12 titulo-manual" id="NotasEFaltas">Notas & Faltas
                        <div class="col-md-12 ds-manual" id="NotasEFaltasds">
                     7.1 Para pesquisar por algo, basta clicar no campo de pesquisa no cabeçalho e digitar o que busca e o usuário será redirecionado para uma página.
<br>7.2 Nessa página, é listado o resultado da busca em 3 tipos: Usuários; Grupos; Parceiros.
                    </div>
                    </div>


            </div> 

             <div class="col-md-offset-1 col-md-4 div-noticias " style="text-align:center;padding:15px;margin-top: 100px !important;">
             <h3>Dúvidas ainda???</h3>
             <p>Baixe nosso manual completo! Não se preocupe, você não vai gastar nada ;)</p>
             <form method="POST">
             <input type="submit" class="btn btn-shyme-default" name="download_manual" id="download_manual" value="Baixar Manual"></form>
             </div>     
                 
            </div></div>
        </div> <!-- container- fluid -->
<?php $this->load->view("elements/footer");  ?>

</body>


  <script type="text/javascript">
        
    $(document).ready(function(){

        $( "#Loginds" ).slideUp();
        $( "#Login" ).bind( "click", function() {
          $( "#Loginds" ).slideDown();
        });
        $( "#Login" ).bind( "dblclick", function() {
          $( "#Loginds" ).slideUp();
        });



    
        $( "#Cadastrods" ).slideUp();
        $( "#Cadastro" ).bind( "click", function() {
          $( "#Cadastrods" ).slideDown();
        });
        $( "#Cadastro" ).bind( "dblclick", function() {
          $( "#Cadastrods" ).slideUp();
        });
    
    
        $( "#Personalizacaods" ).slideUp();
        $( "#Personalizacao" ).bind( "click", function() {
          $( "#Personalizacaods" ).slideDown();
        });
        $( "#Personalizacao" ).bind( "dblclick", function() {
          $( "#Personalizacaods" ).slideUp();
        });
    
    
    
        $( "#Gruposds" ).slideUp();
        $( "#Grupos" ).bind( "click", function() {
          $( "#Gruposds" ).slideDown();
        });
        $( "#Grupos" ).bind( "dblclick", function() {
          $( "#Gruposds" ).slideUp();
        });
    
    
        $( "#Postagensds" ).slideUp();
        $( "#Postagens" ).bind( "click", function() {
          $( "#Postagensds" ).slideDown();
        });
        $( "#Postagens" ).bind( "dblclick", function() {
          $( "#Postagensds" ).slideUp();
        });
    

        $( "#Pontuacaods" ).slideUp();
        $( "#Pontuacao" ).bind( "click", function() {
          $( "#Pontuacaods" ).slideDown();
        });
        $( "#Pontuacao" ).bind( "dblclick", function() {
          $( "#Pontuacaods" ).slideUp();
        });
    

    
        $( "#Pesquisads" ).slideUp();
        $( "#Pesquisa" ).bind( "click", function() {
          $( "#Pesquisads" ).slideDown();
        });
        $( "#Pesquisa" ).bind( "dblclick", function() {
          $( "#Pesquisads" ).slideUp();
        });
    
    
    
        $( "#NotasEFaltasds" ).slideUp();
        $( "#NotasEFaltas" ).bind( "click", function() {
          $( "#NotasEFaltasds" ).slideDown();
        });
        $( "#NotasEFaltas" ).bind( "dblclick", function() {
          $( "#NotasEFaltasds" ).slideUp();
        });
    





    });



                
    </script>
</html>