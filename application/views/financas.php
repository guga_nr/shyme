<!DOCTYPE html>
<html>
<head>
	<title>Din Din</title>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	<style type="text/css">
		.botoes button{
			background-color: #3750A3;
			padding: 10px;
			font-size: 13px;
			font-weight: 900;
			min-width: 100%;
			color: #FFF;
			margin-bottom: 20px;
		}.enviar button{
			background-color: #3750A3;
			padding: 10px;
			font-size: 16px;
			font-weight: 900;
			min-width: 100%;
			color: #FFF;
			margin-top: 20px
		}.botoes-money button{
			background-color: #3750A3;
			padding: 10px 0;
			font-size: 15px;
			font-weight: 900;
			width: 18%;
			color: #FFF;
			margin-left: 5%;
		}
		.total{
			background-color: #3750A3;
			padding: 10px;
			height: auto;
			width: 49%;
			color: #FFF;
			text-align: center;
			margin-bottom: 40px;
			display: inline-flex;
		}
		.formulario{
			background-color: #7C8FCE;
			height: auto;
			padding: 20px;
			margin-top: 30px;
			display: none;
		}
		.formulario input{
			width: 100%;
			margin-bottom: 10px;
			height: 40px;
		}
		.gasto textarea{
			width: 100%;
			margin-bottom: 10px;
			background-color: #FFF;
			height: 70px;
			text-align: center;
		    font-size: 25px;
		    padding: 16px 0px;
		}
		.centavo-mais{
			position: relative;
		    left: -20px;
		    top: 3px;
		    font-size: 1em;
		    background-color: #0bea0b;
		    padding: 5px;
		    border-radius: 100px;
		    color: #FFF;
		    text-align: center;
		    cursor: pointer;
		}
		.centavo-menos{
			position: relative;
		    left: -20px;
		    top: 22px;
		    font-size: 1em;
		    background-color: #ea0b0b;
		    padding: 5px;
		    border-radius: 100px;
		    color: #FFF;
		    text-align: center;
		    cursor: pointer;
		}
		.menos{
		    font-size: 2.2em;
		    background-color: #ea0b0b;
		    padding: 5px;
		    border-radius: 100px;
		    color: #FFF;
		    text-align: center;
		    cursor: pointer;
		}
		.mais{
		    font-size: 2.2em;
		    background-color: #0bea0b;
		    padding: 5px;
		    border-radius: 100px;
		    color: #FFF;
		    text-align: center;
		    margin-left: 20px;
		    cursor: pointer;
		}
		.titulo{
			font-size: 20px;	
		    text-align: center;
		    color: #FFF;
		    font-weight: bolder;
		}
		.gastos table{
			width: 100%;
			margin-top: 20px;
		}
		.gastos th{
			background-color: #3750A3;
			font-size: 20px;	
		    font-weight: bolder;
		    color: #FFF;
		}
		.gastos tr{
			background-color: #7C8FCE;
			font-size: 16px;	
		    color: #FFF;
		}
	</style>
</head>
<body>	
	<?php 

	if (isset($total)) {
		foreach ($total as $t) {
			$valorTotal = $t['sum(valor)'];
		}
	}
	if (isset($cofre)) {
		foreach ($cofre as $t) {
			$valorCofre = $t['sum(valor)'];
		}
	}
	if (isset($Bolso)) {
		foreach ($Bolso as $t) {
			$valorbolso = $t['sum(valor)'];
		}
	}

	if (isset($Salario)) {
		foreach ($Salario as $t) {
			$valorsalario = $t['sum(valor)'];
		}
	}
	if (isset($Inss)) {
		foreach ($Inss as $t) {
			$valorinss = $t['sum(valor)'];
		}
	}
	if (isset($Colchao)) {
		foreach ($Colchao as $t) {
			$valorcolchao = $t['sum(valor)'];
		}
	}

	?>

	<div class="container-fluid">
		<div class="row">

			<div class="total"><h4><center>Seu Cofre é de: <br>R$ <?php echo $valorCofre; ?></center></h4></div>
			<div class="total"><h4><center>Seu total é de: <br>R$ <?php echo $valorTotal; ?></center></h4></div>

			<div class=" col-xs-offset-1 col-xs-10 botoes">
				<div class="col-xs-6">
					<button id="conta-salario" value="">Conta Salário</button>
				</div>
				<div class="col-xs-6">
					<button id="colchao" value="">Colchão</button>
				</div>
				<div class="col-xs-6">
					<button id="inss" value="">INSS</button>
				</div>
				<div class="col-xs-6">
					<button id="bolso" value="">Bolso</button>
				</div>
			</div>
			</div>

		<div class="row">
			<div class=" col-xs-offset-1 col-xs-10 formulario">
				<form>
					<input type="hidden" id="tipo" value="">
					<div class="col-xs-12 titulo"><p id="titulo">Titulo: R$ 500,00</p></div>
						<input type="text" name="valor" id="ds">
					<div class="col-xs-offset-3 col-xs-6">
						<div class="gasto"><textarea>0.00</textarea></div>
					</div>
					<div class="col-xs-3" style="height:80px">
						<span class="glyphicon glyphicon-plus centavo-mais"></span>
						<span class="glyphicon glyphicon-minus centavo-menos"></span>
					</div>
					<div class="col-xs-offset-3 col-xs-9" style="margin-bottom:20px">
						<span class="glyphicon glyphicon-minus menos"></span>
						<span class="glyphicon glyphicon-plus mais"></span>
					</div>

					<div class="col-xs-12 botoes-money ">
							<button id="um">1</button>
							<button id="cinco">5</button>
							<button id="dez">10</button>
							<button id="cinquenta">50</button>
					</div>

					<div class="col-xs-12 enviar">
						<button id="enviar">Enviar</button>
					</div>
				</form>
			</div>
			</div>

		<div class="row">
			<div class="col-xs-12 gastos">
				<table border="1">
					<tr>
						<th>Descrição</th>
						<th>Valor</th>
						<th>Data</th>
						<th>Tipo</th>
					</tr>
				<?php if(isset($goku)):  ?>
				<?php $i = 0; ?>
				<?php foreach($goku as $g): ?>
					<tr>
						<td><?php echo $g['ds']; ?></td>
						<td><?php echo $g['valor']; ?></td>
						<td><?php echo date("d/m/Y H:i", strtotime($g['data'])); ?></td>
						<td><?php echo $g['tipo']; ?></td>
					</tr>

				<?php endforeach; ?>
				<?php endif; ?>
				</table>
			</div>
		</div>
	</div>



	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>

	<script type="text/javascript">
		$(document).ready(function(){
			var total = parseInt($(".gasto textarea").text(), 5);	
			total = Number.parseFloat(total);		
			var valor = 0;

			$('#um').click(function(event){
				event.preventDefault();
				valor = 1.00;
			});
			$('#cinco').click(function(event){
				event.preventDefault();
				valor = 5.00;
			});
			$('#dez').click(function(event){
				event.preventDefault();
				valor = 10.00;
			});
			$('#cinquenta').click(function(event){
				event.preventDefault();
				valor = 50.00;
			});
			
			$('.mais').click(function(){
				total = total + valor;
				$(".gasto textarea").text(total.toFixed(2));
			});
			$('.menos').click(function(){
				total = total - valor;
				$(".gasto textarea").text(total.toFixed(2));
			});
			
			$('.centavo-mais').click(function(){
				total = total + 0.25;
				$(".gasto textarea").text(total.toFixed(2));
			});
			$('.centavo-menos').click(function(){
				total = total - 0.25;
				$(".gasto textarea").text(total.toFixed(2));
			});



			$('#conta-salario').click(function(){
				$(".formulario").css("display", "block");
				$(".titulo p").text("Conta Salário: R$<?php echo $valorsalario; ?>");
				$("#tipo").val("1");
			});
			$('#colchao').click(function(){
				$(".formulario").css("display", "block");
				$(".titulo p").text("Colchão: R$<?php echo $valorcolchao; ?>");
				$("#tipo").val("2");
			});
			$('#inss').click(function(){
				$(".formulario").css("display", "block");
				$(".titulo p").text("INSS: R$<?php echo $valorinss; ?>");
				$("#tipo").val("3");
			});
			$('#bolso').click(function(){
				$(".formulario").css("display", "block");
				$(".titulo p").text("Bolso: R$<?php echo $valorbolso; ?>");
				$("#tipo").val("4");
			});


			$('#enviar').click(function(event){
				event.preventDefault();
				var tipo = $("#tipo").val();
				var ds = $("#ds").val();
				var data = "enviar=1&ds="+ds+"&valor="+total+"&tipo="+tipo;
				$.ajax({
		            type: 'POST',
		            url: '',
		            data: data,
		            dataType: 'json',
		            success: function (data) {
		                
		            },         
		            error: function(){
			          // $( ".container-fluid" ).load(".container-fluid");
			          window.location.reload();
			          // $(".formulario").fadeOut(300);
			        }          
	            });
			});



		});
	</script>
</body>
</html>