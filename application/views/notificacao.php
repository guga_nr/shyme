<?php $this->load->view("elements/head");  ?>

<style> 
.img-notification{
    background-color:black;
    height: 80px;
    width: 80px;
    margin-right: 10px;
}
        </style>

        <div class="container-fluid ">
        <div class="row">

                    <?php 
                    if (isset($notificacao)) { ?>
                    <div class="col-lg-offset-1 col-lg-10 col-md-offset-1 col-md-10  col-sm-12 col-xs-12 shyme-box">
                <center><h2>Notificações</h2></center>
                        <?php foreach($notificacao as $n){
                            $goku = false;
                            $duvida = false;
                            $solicitacao = false;
                            if($n['tp_notificacao'] == 4 || $n['tp_notificacao'] == 5 || $n['tp_notificacao'] == 6){
                                $goku = true;
                                $duvida = true;
                                echo  "<span data-id='".$n['cd_notificacao']."'>";
                            }
                            if($n['tp_notificacao'] == 10){
                                $goku = true;
                                $solicitacao = true;
                                echo  "<span data-id='".$n['cd_notificacao']."'>";
                            }
                            else if($n['tp_notificacao'] == 11){
                                $goku = true;
                                echo "<a target='blank' data-id='".$n['cd_notificacao']."' href='" . base_url() . "index.php/spotted?id=" . $n['cd_postagem_notificacao']. "'>";
                            }
                            else if($n['tp_notificacao'] == 12 || $n['tp_notificacao'] == 1 || $n['tp_notificacao'] == 9){
                                $goku = true;
                                echo "<a target='blank' data-id='".$n['cd_notificacao']."' href='" . base_url() . "index.php/grupo?id=" . $n['cd_grupo_notificacao']. "&pg=2'>";
                            }else if($n['status_notificacao'] == 0)
                                    echo "<span data-id='".$n['cd_notificacao']."'>";
                            

                    ?>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  shyme-box postagem-perfil" <?php if ($n['status_notificacao'] == 0) echo 'id="nao-lida"'; ?>>
                            <div>
                                <div class="box-foto" id="last-post">
                                    <img src="<?php echo asset_url()  .  $n['img_tipo_notificacao']; ?>" alt="">
                                </div>
                                <h4><?php echo $n['ds_tipo_notificacao']; ?></h4>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                                <p><?php echo $n['ds_notificacao']; ?></p>
                                <p>

                                </p>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 final-post">
                                <p>
                                <?php echo  Date('d/m/y', StrToTime($n['dt_notificacao'])); ?>
                                <?php if($duvida): ?> 
                                - <a href="<?php echo base_url() . 'index.php/duvida?id=' . $n['cd_postagem_notificacao']; ?>"><span class="shyme-box">Ver Respostas</span></a>
                                <?php endif; ?> 
                                <?php if($solicitacao): ?> 
                                    <div class="respostaSolicitacao">
                                        - <span data-resposta='1' data-idalunonoti="<?php echo $n['cd_matricula_remetente']; ?>" data-noti="<?php echo $n['cd_notificacao']; ?>" class="shyme-box">Sim</span>
                                          <span data-resposta='0' data-idalunonoti="<?php echo $n['cd_matricula_remetente']; ?>" data-noti="<?php echo $n['cd_notificacao']; ?>" class="shyme-box">Não</span>
                                </div>
                                <?php endif; ?> 
                                </p>
                            </div>
                        </div>

                    <?php
                            if($goku)
                                echo "</a>";
                            else
                                echo "</span>"; 
                        } ?>


                        <?php 
                            $pagina = $paginacao[0]['count(*)'];
                            if(!isset($_GET['p']))
                                $_GET['p'] = 1;
                            $goku = $pagina / 10;
                            if(str_replace('.', ' ', $goku) != $goku)
                                $pagina= (int) $goku + 1;
                            else
                                $pagina= (int) $goku;

                            if($_GET['p'] == $pagina)
                                $proxima = (int) $_GET['p'];
                            else
                                $proxima = (int) $_GET['p'] + 1;
                            if($_GET['p'] == 1)
                                $anterior = (int) $_GET['p'];
                            else
                                $anterior = (int) $_GET['p'] - 1;
                        ?>
                        <div class="col-md-12">
                            <div class="paginacao-shyme" style="margin: 20px 0;">
                                <?php if($pagina > 0): ?>
                                <a href="<?php echo base_url() . '/index.php/Notificacao?p='. $anterior; ?>">
                                    <span class="glyphicon glyphicon-triangle-left"></span>
                                </a>
                                <?php endif; ?>
                                <span class="pagina-atual"><?php if(isset($_GET['p']) && $_GET['p'] > 1) echo $_GET['p']; else echo 1;?></span> de <?php if($pagina == 0) echo 1; else echo $pagina; ?>    
                                <?php if($pagina > 0): ?>
                                <a href="<?php echo base_url() . '/index.php/Notificacao?p='. $proxima; ?>">
                                    <span class="glyphicon glyphicon-triangle-right"></span>
                                </a>
                                <?php endif; ?>
                            </div>
                        </div>
                   </div>
                   <?php } ?>
            </div>
            </div>
           <?php $this->load->view("elements/footer");  ?>
    </body>

</html>