<!DOCTYPE html>
<html lang="pt-BR">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <?php foreach($infoN as $i){ $nomeGrupo = $i['nm_grupo']; ?>
        <title>Grupo - <?php echo $i['nm_grupo']; ?>|ADM</title>
    <?php } ?>
    <script src="<?php echo asset_url(); ?>js/jquery.min.js"></script>
    <link rel="stylesheet" href="<?php echo asset_url(); ?>css/bootstrap.min.css" />
    <link rel="stylesheet" href="<?php echo asset_url(); ?>css/grupo.css" />

</head>

<body>
<div class="navbar navbar-fixed-top">
            <?php include("header.php"); 
            ?>
        </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-offset-1 col-md-10 col-xs-12"><h3><?php echo $nomeGrupo; ?></h3></div>
            <div class="navbar-grupo">
            <nav class="navbar navbar-inverse col-md-offset-1 col-md-10 col-xs-12">
                    <ul class="nav navbar-nav col-md-12  col-xs-12">
                        <li class="nav-item active col-md-6 col-xs-6" style="padding: 0px;width: 50%;float: left;">
                          <a class="nav-link" href="Grupo?id=<?php echo $_GET['id']; ?>">Postagens <span class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item col-md-6 col-xs-6" style="padding: 0px;width: 50%;float: left;">
                          <a class="nav-link " href="Grupo?id=<?php echo $_GET['id']; ?>&pg=2">Grupo</a>
                        </li>
                    </ul>
            </nav>
            <div>
            <section id="main-content">
                <center><div>
                <button onclick="comun()" id="bt_criar_post" class="btn btn-shyme-default btn-post">Publicar Comunicado</button>
                <button onclick="duvid()" id="bt_criar_post" class="btn btn-shyme-default btn-post">Publicar Duvida</button>
                <button onclick="mater()" id="bt_criar_post" class="btn btn-shyme-default btn-post">Publicar Material</button>
                </div></center>
                <div id="com" class="row titulo">
                    <div class="col-md-offset-1 col-md-10">
                        <h3>Postar Comunicado</h3>

                            <div class=" col-md-12 col-xs-12 col-sm-12  objeto-postar">
                                <div>
                                    <!-- <alteracao> -->
                                    <form method="POST" enctype="multipart/form-data" >
                                        <div class="form-group">
                                            <textarea class="form-control" placeholder="Digite alguma coisa para postar." name="txt_content_post" id="txt_content_post" rows="3"></textarea>
                                        </div>
                                        <div class="form-inline">
                                            <div class="form-group">
                                                <!-- USAR O NAME -->

                                                <input type="file" name="comunicado_imagem" class="btn btn-shyme-default btn-anexar"><span name="comunicado-file" id="anexar_arquivo" class="glyphicon glyphicon-paperclip"></span>
                                                

                                            </div>
                                            

                                            <input   type="submit" class="btn btn-shyme-default btn-post" name="comunicado" id="comunicado" value="Postar Comunicado">                                        
                                        </div>
                                            

                                        </div>
                                    </form>
                                </div>
                                <!-- </alteracao> -->
                            </div>
                        </div>
                    </div>
                </div>

                <div id="duv" class="row titulo">
                    <div class="col-md-offset-1 col-md-10">
                        <h3>Postar Dúvida</h3>

                            <div class=" col-md-12 col-xs-12 col-sm-12  objeto-postar">
                                <div>
                                    <!-- <alteracao> -->
                                    <form method="POST" enctype="multipart/form-data">
                                        <div class="form-group">
                                            <textarea class="form-control" placeholder="O dia está bonito para tirar uma dúvida!" id="txt_content_post_duvida" name="txt_content_post_duvida" rows="3"></textarea>
                                        </div>
                                        <div class="form-inline">
                                            <div class="form-group">
                                                <!-- USAR O NAME -->

                                                 <input type="file" name="duvida_imagem" class="btn btn-shyme-default btn-anexar"><span name="duvida-file" id="anexar_arquivo" class="glyphicon glyphicon-paperclip"></span>
                                                
                                            </div>
                                            <input type="radio" name="moeda_duvida" id="moeda_duvida" value=29>5 Pontos
                                            <input type="radio" name="moeda_duvida" id="moeda_duvida" value=30>10 Pontos
                                            <input type="radio" name="moeda_duvida" id="moeda_duvida" value=31>15 Pontos
                                            <input   type="submit" class="btn btn-shyme-default btn-post" name="duvida" id="duvida" value="Postar Dúvida">
                                           
                                </div>
                                    </form>
                                </div>
                                <!-- </alteracao> -->
                            </div>
                        </div>
                    </div>
                    
                </div>

                

                <div id="mat" class="row titulo">
                    <div class="col-md-offset-1 col-md-10">
                        <h3>Postar Material</h3>

                            <div class=" col-md-12 col-xs-12 col-sm-12  objeto-postar">
                                <div>
                                    <form method="POST" enctype="multipart/form-data" >
                                        <div class="form-material">
                                            <input type="file" name="material_arquivo" id="material_arquivo" class="btn btn-shyme-default btn-anexar">
                                        <div class="form-group">
                                            <textarea class="form-control" placeholder="Digite alguma coisa para postar." name="txt_content_material"  id="txt_content_material" rows="3"></textarea>
                                        </div>
                                        <div class="form-inline">
                                            <div class="form-group">
                                                <input type="file" class="btn btn-shyme-default btn-post"  name="material_imagem">  
                                            </div>
                                            <input type="submit" class="btn btn-shyme-default btn-post" name="upload" id="upload" value="Criar postagem"> 
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>
                    </div>
           
                <div class="row">
                    <div class="col-md-offset-1 col-md-10 conteudo">
            <?php $g=0; $i=0; if (isset($resultadoP)){
                foreach($resultadoP as $resP) { if($resP['tipo_postagem_cd_tipo_postagem'] == 33 || $resP['tipo_postagem_cd_tipo_postagem'] == 32) { ?>

                        <div class="post-objeto">
                        <?php if($resP['cd_matricula'] == $_SESSION['id'] ){ ?>
                             <a class="close remover-post" href="Grupo?id=<?php echo $_GET['id']; ?>&remover=<?php echo $resP['cd_postagem']; ?>">X</a>
                             <?php } ?>
                            <div class="media-left">
                                <a href="PerfilAluno?ldp=<?php echo $resP['perfil_aluno'] ;?>">
                                <img class="media-object foto-usuario-post" src="<?php echo str_replace("C:\\xampp\\htdocs", "", $resP['img_aluno']); ?>" alt="Icone usuario">
                                </a>
                            </div>

                            <div class="media-body">
                                    <h4 class="media-heading">
                                        <a href="PerfilAluno?ldp=<?php echo $resP['perfil_aluno'] ;?>"><?php echo $resP['nm_aluno'] ;?></a>
                                    </h4>
                                <p class="conteudo"><?php echo $resP['ds_postagem']; ?>
                                </p>

                                <p><img src="<?php echo $resP['img_postagem']; ?>" class="img-comunicado">
                                </p>

                                <!-- <button class="btn-shyme-avaliar">-</button> -->
                                <span class="span-tipo-post">Comunicado</span>
                            </div>
                        </div>
                <?php } else if($resP['tipo_postagem_cd_tipo_postagem'] == 26 || $resP['tipo_postagem_cd_tipo_postagem'] == 27) { ?>
                        <div class="post-objeto">
                            <?php if($resP['cd_matricula'] == $_SESSION['id'] ){ ?>
                             <a class="close remover-post" href="Grupo?id=<?php echo $_GET['id']; ?>&remover=<?php echo $resP['cd_postagem']; ?>">X</a>
                             <?php } ?>
                            <div class="media-left">
                                
                                <a href="PerfilAluno?ldp=<?php echo $resP['perfil_aluno'] ;?>">
                                <img class="media-object foto-usuario-post" src="<?php echo str_replace("C:\\xampp\\htdocs", "", $resP['img_aluno']); ?>" alt="Icone usuario">
                                </a>
                            </div>
                            <div class="media-body">
                                    <h4 class="media-heading">
                                        <a href="PerfilAluno?ldp=<?php echo $resP['perfil_aluno'] ;?>"><?php echo $resP['nm_aluno'] ;?></a>
                                    </h4>
                                
                                <p><?php echo $resP['ds_postagem']; ?><?php echo str_replace("C:\\xampp\\htdocs", "", $resP['img_aluno']); ?>
                                </p>
                                            <p><img src="<?php echo $resP['img_postagem']; ?>" class="img-comunicado">
                                            </p>
                                <!-- <button class="btn-shyme-avaliar">-</button> -->

                                <?php if ($resP['CD_VL_DUVIDA'] == 29) { ?>
                                    <b><span class="span-tipo-post">Duvida valendo 5 PONTOS</span></b>
                               <?php }  else if ($resP['CD_VL_DUVIDA'] == 30) { ?>
                                   <b><span class="span-tipo-post">Duvida valendo 10 PONTOS</span></b>
                               <?php }  elseif ($resP['CD_VL_DUVIDA'] == 31) { ?>
                                    <b><span class="span-tipo-post">Duvida valendo 15 PONTOS</span></b>
                               <?php } else{ ?>
                                    <b><span class="span-tipo-post">Duvida</span></b>
                                <?php } ?>
                                <div class="post-resposta col-md-offset-1 col-md-11">
                                    <!--AQUI COMEÇA A PARTE DA RESPOSTA DA DUVIDA!!!!!!-->
                                    <?php if (isset($resultadoR)){ foreach($resultadoR as $resR){ if($resR['cd_postagem'] == $resP['cd_postagem']){ ?>
                                     <div class="col-md-12">
                                     <div class="media-left img-resposta">
                                        <img class="media-object" src="<?php echo $resR['img_aluno'] ;?>" alt="Icone usuario">
                                    </div>
                                    <div class="media-body">
                                        
                                            <h4 class="media-heading"><a href="PerfilAluno?ldp=<?php echo $resR['perfil_aluno'] ;?>"><?php echo $resR['nm_aluno']; ?></a></h4>
                                        
                                        <p class="conteudo"><?php echo $resR['DS_RESPOSTA']; ?>
                                        </p>

                                        <!-- <button class="btn-shyme-avaliar">-</button> -->
                                        <span class="span-tipo-post">Resposta</span>
                                    </div>
                                    </div>
                                    <?php }} }?>
                                    </div>
                                    <!--AQUI TERMINA A PARTE DA RESPOSTA DA DUVIDA!!!!!!-->
                                     <!--AQUI COMEÇA A PARTE DE CAMPO DE RESPOSTA!!!!!!-->
                                     <div class="post-responder col-md-offset-0 col-md-12">
                                        <a class="btn btn-shyme-default" role="button" href="Duvida?id=<?php echo $resP['cd_postagem'];?>">Responder</a>
                                    </div>
                                </div>
                            </div>
                                     <!--AQUI TERMINA A PARTE DE CAMPO DE RESPOSTA!!!!!!-->
                            <?php } else if($resP['tipo_postagem_cd_tipo_postagem'] >= 10 && $resP['tipo_postagem_cd_tipo_postagem'] <= 15) { ?>
                                    <div class="post-objeto">
                                    <?php if($resP['cd_matricula'] == $_SESSION['id'] ){ ?>
                                         <a class="close remover-post" href="Grupo?id=<?php echo $_GET['id']; ?>&remover=<?php echo $resP['cd_postagem']; ?>">X</a>
                                         <?php } ?>
                                        <div class="media-left">
                                         
                                            <a href="PerfilAluno?ldp=<?php echo $resP['perfil_aluno'] ;?>">
                                            <img class="media-object foto-usuario-post" src="<?php echo str_replace("C:\\xampp\\htdocs", "", $resP['img_aluno']); ?>" alt="Icone usuario">
                                            </a>
                                         </div>
                                        <div class="media-body">
                                            <a href="PerfilAluno?ldp=<?php echo $resP['perfil_aluno'] ;?>">
                                                <h4 class="media-heading"><?php echo $resP['nm_aluno'] ;?></h4>
                                            </a>
                                            <p><?php echo $resP['ds_postagem']; ?>
                                            </p>
                                            <p><img src="<?php echo $resP['img_postagem']; ?>" class="img-comunicado">
                                            </p>
                                            <b><span class="span-tipo-post">Material</span></b>
                                            <?php if($resP['cd_matricula'] != $_SESSION['id'] ){ ?>
                                             <div class="post-responder col-md-offset-0 col-md-12">
                                                <form method="POST">
                                                    <input type="hidden" name="aluno_upload" id="aluno_upload" value="<?php echo $resP['cd_matricula']; ?>">
                                                    <input type="hidden" name="idPost" id="idPost"  value="<?php echo $resP['cd_postagem']; ?>">
                                                    <input type="hidden" name="caminho_arquivo" id="caminho_arquivo"  value="<?php echo $resP['ARQUIVO_POSTAGEM']; ?>">
                                                    <input type="submit" class="btn btn-shyme-default" name="download_material" id="download_material" value="Baixar <?php echo str_replace("\\shyme\\assets\\img\\material\\upload\\","",$resP['ARQUIVO_POSTAGEM']); ?>">
                                                </form>
                                            </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                            <?php $i++;} ?>
                            
                        <?php $g++; }}if($g == 0){?>
                            <div class="col-md-offset-1 col-md-10 conteudo">
                            <div class="post-objeto  none-post">

                                <div class="media-body">
                                        <h4 class="media-heading"></h4>
                                    <p><img style="max-width:95%; margin-left: 5%;" src="<?php echo base_url(); ?>assets/img/aviso-postagem.png"> </p>

                                    <!-- <button class="btn-shyme-avaliar">-</button> -->
                                    <span class="span-tipo-post">:(</span>
                                </div>
                            </div>

                        <?php }?>
                </div>
            </section>
        </div>
    </div>

    <div class="container-fluid footer">
        <div class="row">
            <div class="col-md-12 ">
                <div class="row">
                    <?php include("footer.php"); ?>
                </div>
            </div>
        </div>
    </div>
    <script src="<?php echo asset_url(); ?>js/bootstrap.min.js"></script>
    <script src="<?php echo asset_url(); ?>js/grupo.js"></script>
</body>

<script type="text/javascript">
        
   
    $(document).ready(function(){
       $("#upload").click(function(event){
            var file = $('#material_arquivo').val();
            var texto = $('textarea#txt_content_material').val();
            if(file === ''){
                alert('Você precisa subir algum arquivo para postar um material!');
                event.preventDefault();
            }else if(texto === ''){        
                alert('Não deixe a postagem vazia =/');
                event.preventDefault();
            }
        });

       $("#duvida").click(function(event){
            var file = jQuery("input[name=moeda_duvida]:checked").val();
            var texto = $('textarea#txt_content_post_duvida').val();
            if(file === undefined){
                alert('Você precisa escolher uma quantidade de pontos para postar uma dúvida!');
                event.preventDefault();
            }else if(texto === ''){        
                alert('Não deixe a postagem vazia =/');
                event.preventDefault();
            }
        });

       $("#comunicado").click(function(event){
        var texto = $('textarea#txt_content_post').val();
         if(texto === ''){        
            alert('Não deixe a postagem vazia =/');
            event.preventDefault();
        }
        }); 
    });
            
                
    </script>

</html>