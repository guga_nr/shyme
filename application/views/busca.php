<?php $this->load->view("elements/head");  ?>

        <div class="container-fluid corpo">
            <div class="row">
                <div class="col-lg-offset-1 col-lg-10 col-md-offset-1 col-md-10  col-sm-offset-1 col-sm-10 col-xs-12">
                    <div class="navegacao-parceiro">
                        <a href="#">
                            <div  id="pessoas">Alunos</div>
                        </a>
                        <a  href="#">
                            <div id="grupos">Grupos</div>
                        </a>
                        <a  href="#">
                            <div id="parceiros">Parceiros</div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-offset-1 col-lg-10 col-md-offset-1 col-md-10  col-sm-12 col-xs-12 shyme-box" style="min-height: 700px;">
                    <?php 
                    if (isset($pessoa)) {?>
                        <div id="resultado-pessoas">
                    <?php foreach($pessoa as $n){?>
                    <div  class="col-md-6 col-sm-12 membro-objeto">
                        <div class="shyme-box membro-box" >
                            <div class="box-foto" id="last-post">
                                <img src="<?php echo asset_url()  . $n['img_aluno']; ?>" alt="">
                            </div>
                            <p><a href="<?php echo base_url() . 'index.php/perfil/aluno/'. $n['perfil_aluno']; ?>"><?php echo $n['nm_aluno']; ?></a></p>
                            <p><?php echo $n['curso_aluno']; ?></p>
                        </div>
                    </div>
                    <?php }?>
                        </div>
                    <?php }else{ 
                        echo"Não há resultados para sua busca"; 
                        }?>

                    <?php 
                    if (isset($parceiro)) {?>
                        <div id="resultado-parceiros">
                    <?php foreach($parceiro as $p){?>
                    <div  class="col-md-6 col-sm-12 membro-objeto">
                        <div class="shyme-box membro-box" >
                            <div class="box-foto" id="last-post">
                                <img src="<?php echo asset_url()  . $p['img_parceiro']; ?>" alt="">
                            </div>
                            <p><a href="<?php echo base_url() . 'index.php/parceiros/perfil/'. $p['pefil_parceiro']; ?>"><?php echo $p['nm_parceiro']; ?></a></p>
                        </div>
                    </div>
                    <?php }?>
                        </div>
                    <?php }else{ 
                        echo"Não há resultados para sua busca"; 
                        }?>

                    <?php 
                    if (isset($grupo)) {?>
                        <div id="resultado-grupos">
                    <?php 
                    foreach($grupo as $n){?>
                    <div  class="col-md-6 col-sm-12 membro-objeto">
                        <div class="shyme-box membro-box" >
                            <div class="box-foto" id="last-post">
                                <img src="<?php echo asset_url()  . $n['img_grupo']; ?>" alt="">
                            </div>
                            <p><a href="<?php echo base_url() . 'index.php/grupo?id='. $n['cd_grupo']; ?>"><?php echo $n['nm_grupo']; ?></a></p>
                            <p><?php echo $n['ds_grupo']; ?></p>
                        </div>
                    </div>
                    
                    
                    <?php }?>
                        </div>
                    <?php }else{ 
                        echo"Não há resultados para sua busca"; 
                        }?>

                    
                </div>
            </div>

        </div>
<?php $this->load->view("elements/footer");  ?>
        
    </body>

<script type="text/javascript">
    
            $('#pessoas').css('background', '#147f85');
            $('#pessoas').css('color', '#FFF');
            $('#resultado-grupos').css('display', 'none');
            $('#resultado-pessoas').css('display', 'block');
            $('#resultado-parceiros').css('display', 'none');
        $('#grupos').click(function(){
            $('#resultado-pessoas').css('display', 'none');
            $('#resultado-grupos').fadeIn('400');
            $('#grupos').css('background', '#147f85');
            $('#grupos').css('color', '#FFF');
            $('#pessoas').css('background', '#FFF');
            $('#pessoas').css('color', '#147f85');
            $('#resultado-parceiros').css('display', 'none');

            $('#parceiros').css('background', '#FFF');
            $('#parceiros').css('color', '#147f85');
        });
        $('#pessoas').click(function(){
            $('#resultado-grupos').css('display', 'none');
            $('#resultado-pessoas').fadeIn('400');
            $('#resultado-parceiros').css('display', 'none');
            $('#pessoas').css('background', '#147f85');
            $('#pessoas').css('color', '#FFF');
            $('#grupos').css('background', '#FFF');
            $('#grupos').css('color', '#147f85');
            $('#parceiros').css('background', '#FFF');
            $('#parceiros').css('color', '#147f85');
        });
        $('#parceiros').click(function(){
            $('#resultado-grupos').css('display', 'none');
            $('#resultado-pessoas').css('display', 'none');
            $('#resultado-parceiros').fadeIn('400');
            $('#parceiros').css('background', '#147f85');
            $('#parceiros').css('color', '#FFF');
            $('#grupos').css('background', '#FFF');
            $('#grupos').css('color', '#147f85');
            $('#pessoas').css('background', '#FFF');
            $('#pessoas').css('color', '#147f85');
        });
            
</script>
</html>