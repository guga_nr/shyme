<!DOCTYPE html>
<html lang="pt-BR">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>Shyme - Spotted</title>
    <script src="<?php echo asset_url(); ?>js/jquery.min.js"></script>
    <link rel="stylesheet" href="<?php echo asset_url(); ?>css/bootstrap.min.css" />
    <link rel="stylesheet" href="<?php echo asset_url(); ?>css/grupo.css" />

</head>

<body>
<div class="navbar navbar-fixed-top">
            <?php include("header.php"); 
            ?>
        </div>
    <div class="container-fluid">
        <div class="row">
            <div class="navbar-grupo">
            <div>
            <section id="main-content">
                <div class="row titulo">
                    <div class="col-md-offset-1 col-md-10">
                        <h3>Spotted!</h3>

                        <div class="row">
                            <div class="col-md-12 objeto-postar">
                                <div>
                                    <!-- <alteracao> -->
                                    <form method="POST" enctype="multipart/form-data" >
                                    <input type="text" name="nick-spotted" value="Fatecano Apaixonado" style="width: 100%;padding: 5px 10px;">
                                        <div class="form-group">

                                            <textarea class="form-control" placeholder="Digite alguma coisa para postar." name="txt_content_spotted_post" rows="3"></textarea>
                                        </div>
                                        <div class="form-inline">
                                            <div class="form-group">
                                                <!-- USAR O NAME -->

                                                <input type="file" name="spotted_imagem" class="btn btn-shyme-default btn-anexar"><span name="spotted-file" id="anexar_arquivo" class="glyphicon glyphicon-paperclip"></span>
                                                

                                            </div>
                                            

                                            <input type="submit" value="Postar" name="spotted-post" class="btn btn-shyme-default btn-post" >
                                            </div>
                                            </form>
                                        </div>
                                    
                                </div>
                                <!-- </alteracao> -->
                            </div>
                        </div>
                    </div>
                </div>
                    </div>
                </div>
           

                <div class="row">
                    <div class="col-md-offset-1 col-md-10 conteudo">
                     <?php  $i=0; foreach($resultadoP as $resP) { ?>
                        <div class="post-objeto">
                            <div class="media-left">
                                <img class="media-object foto-usuario-post" src="<?php echo $resP['img_spotted']; ?>" alt="Icone usuario">
                            </div>
                            <div class="media-body">
                                <a href="#">
                                    <h4 class="media-heading"><?php echo $resP['nick_spotted'] ;?></h4>
                                </a>
                                <p><?php echo $resP['ds_postagem_spotted']; ?>
                                </p>
                                <img src="<?php echo asset_url(). $resP['arquivo_postagem_spotted']; ?>" class="img-comunicado">
                                         

                                <!-- <alteracao> -->
                                <span id="fav_post" class="glyphicon glyphicon-star-empty"></span>
                                <!-- </alteracao> -->

                    
                                    <!--AQUI TERMINA A PARTE DA RESPOSTA DA DUVIDA!!!!!!-->
                                     <!--AQUI COMEÇA A PARTE DE CAMPO DE RESPOSTA!!!!!!-->
                                     <div class="post-responder col-md-offset-0 col-md-12">
                                        <a class="btn btn-shyme-default" role="button" href="Spotted/Comentario?id=<?php echo $resP['cd_postagem_spotted'];?>">Comentar</a>
                                    </div>
                                     <!--AQUI TERMINA A PARTE DE CAMPO DE RESPOSTA!!!!!!-->
                            </div>
                        </div>

                <?php $i++; }?>
                        <div class="col-md-offset-2 col-md-8">
                            <center>
                            <a href="#">
                            <h2><span id="mais_posts" class="glyphicon glyphicon-option-horizontal"></h2></span></a>
                        
                    </div>
                </div>
            </section>
        </div>
    </div>

    <div class="container-fluid footer">
        <div class="row">
            <div class="col-md-12 ">
                <div class="row">
                    <?php include("footer.php"); ?>
                </div>
            </div>
        </div>
    </div>
    <script src="<?php echo asset_url(); ?>js/bootstrap.min.js"></script>
    <script src="<?php echo asset_url(); ?>js/grupo.js"></script>
</body>

</html>