<div class="col-md-offset-1 col-md-10 col-sm-offset-1 col-sm-10 col-xs-offset-1 col-xs-10">

<?php $g=0; $i=0; if (isset($resultadoP)){
foreach($resultadoP as $resP) { if($resP['tipo_postagem_cd_tipo_postagem'] == 33 || $resP['tipo_postagem_cd_tipo_postagem'] == 32) { ?>

        <div class="post-objeto">
        <?php if($resP['cd_matricula'] == $_SESSION['id'] || $administrador){ ?>
             <a class="close remover-post" href="Grupo?id=<?php echo $_GET['id']; ?>&remover=<?php echo $resP['cd_postagem']; ?>"><img class="close-img" src="\shyme\assets\img\close.png"></a>
             <?php } ?>
            <div class="media-left">
                <a href="Perfil/Aluno/<?php echo $resP['perfil_aluno'] ;?>">
                <img class="media-object foto-usuario-post" src="<?php echo asset_url() . $resP['img_aluno']; ?>" alt="Icone usuario">
                </a>
            </div>

            <div class="media-body">
                    <h4 class="media-heading">
                        <a href="Perfil/Aluno/<?php echo $resP['perfil_aluno'] ;?>"><?php echo $resP['nm_aluno'] . ' - ' . $resP['vl_moeda'] ;?></a>
                    </h4>
                <p class="conteudo"><?php echo $resP['ds_postagem']; ?>
                </p>

                <?php if(!empty($resP['img_postagem'])): ?>
                    <p><img src="<?php echo asset_url() .$resP['img_postagem']; ?>" class="img-comunicado">
                </p>
                <?php endif; ?>

                <!-- <button class="btn-shyme-avaliar">-</button> -->
                <span class="span-tipo-post">Comunicado</span>
            </div>
        </div>
<?php } else if($resP['tipo_postagem_cd_tipo_postagem'] == 26 || $resP['tipo_postagem_cd_tipo_postagem'] == 27) { ?>
        <div class="post-objeto">
            <?php if($resP['cd_matricula'] == $_SESSION['id'] || $administrador){ ?>
             <a class="close remover-post" href="Grupo?id=<?php echo $_GET['id']; ?>&remover=<?php echo $resP['cd_postagem']; ?>"><img class="close-img" src="\shyme\assets\img\close.png"></a>
             <?php } ?>
             
            <div class="media-left">
                
                <a href="Perfil/Aluno/<?php echo $resP['perfil_aluno'] ;?>">
                <img class="media-object foto-usuario-post" src="<?php echo asset_url() . $resP['img_aluno']; ?>" alt="Icone usuario">
                </a>
            </div>
            <div class="media-body">
                    <h4 class="media-heading">
                        <a href="Perfil/Aluno/<?php echo $resP['perfil_aluno'] ;?>"><?php echo $resP['nm_aluno'] . ' - ' . $resP['vl_moeda'] ;?></a>
                    </h4>
                <p><?php echo $resP['ds_postagem']; ?>
                </p>
                <?php if(!empty($resP['img_postagem'])): ?>
                
                    <p><img src="<?php echo asset_url() .$resP['img_postagem']; ?>" class="img-comunicado"></p>
                
                <?php endif; ?>

                <?php if ($resP['cd_vl_duvida'] == 29) { ?>
                    <b><span class="span-tipo-post">Duvida valendo 5 PONTOS</span></b>
               <?php }  else if ($resP['cd_vl_duvida'] == 30) { ?>
                   <b><span class="span-tipo-post">Duvida valendo 10 PONTOS</span></b>
               <?php }  elseif ($resP['cd_vl_duvida'] == 31) { ?>
                    <b><span class="span-tipo-post">Duvida valendo 15 PONTOS</span></b>
               <?php } else{ ?>
                    <b><span class="span-tipo-post">Duvida</span></b>
                <?php } ?>
                    <!--AQUI TERMINA A PARTE DA RESPOSTA DA DUVIDA!!!!!!-->
                     <!--AQUI COMEÇA A PARTE DE CAMPO DE RESPOSTA!!!!!!-->
                     <div class="post-responder col-md-offset-0 col-md-12">
                     <?php if($_SESSION['id'] == $resP['cd_matricula']): ?>
                        <a class="btn btn-shyme-default" role="button" href="Duvida?id=<?php echo $resP['cd_postagem'];?>">Ver Respostas</a>
                    <?php else: ?>
                        <a class="btn btn-shyme-default" role="button" href="Duvida?id=<?php echo $resP['cd_postagem'];?>">Responder</a>
                    <?php endif; ?>
                    </div>
                </div>
            </div>
                     <!--AQUI TERMINA A PARTE DE CAMPO DE RESPOSTA!!!!!!-->
            <?php } else if($resP['tipo_postagem_cd_tipo_postagem'] >= 10 && $resP['tipo_postagem_cd_tipo_postagem'] <= 15) { ?>
                    <div class="post-objeto">
                    <?php if($resP['cd_matricula'] == $_SESSION['id'] || $administrador){ ?>
                         <a class="close remover-post" href="Grupo?id=<?php echo $_GET['id']; ?>&remover=<?php echo $resP['cd_postagem']; ?>"><img class="close-img" src="\shyme\assets\img\close.png"></a>
                         <?php } ?>
                        <div class="media-left">
                         
                            <a href="Perfil/Aluno/<?php echo $resP['perfil_aluno'] ;?>">
                            <img src="<?php echo asset_url() . $resP['img_aluno']; ?>" class="media-object foto-usuario-post"  alt="Icone usuario">
                            </a>
                         </div>
                        <div class="media-body">
                            <a href="Perfil/Aluno/<?php echo $resP['perfil_aluno'] ;?>">
                                <h4 class="media-heading"><?php echo $resP['nm_aluno'] . ' - ' . $resP['vl_moeda'] ;?></h4>
                            </a>
                            <p><?php echo $resP['ds_postagem']; ?>
                            </p>

                            <?php if(!empty($resP['img_postagem'])): ?>
                                <p><img src="<?php echo asset_url() .$resP['img_postagem']; ?>" class="img-comunicado">
                            </p>

                            <?php endif; ?>
                            <b><span class="span-tipo-post">Material</span></b>
                            <?php if($resP['cd_matricula'] != $_SESSION['id'] ){ ?>
                             <div class="post-responder col-md-offset-0 col-md-12">
                                <form  method="post" accept-charset="utf-8">   
                                    <input type="hidden" name="arquivo" value="<?php echo  $resP['arquivo_postagem']; ?>  ">
                                    <input type="hidden" name="aluno_upload" value="<?php echo  $resP['cd_matricula']; ?>  ">
                                    <input type="hidden" name="idPost" value="<?php echo  $resP['cd_postagem']; ?>  ">
                                    <input type="submit" class="btn btn-shyme-default" name="download_material" value="Baixar <?php echo  str_replace("img/material/upload/","",$resP['arquivo_postagem']); ?>  ">
                                </form>
                            </div>
                            <?php } ?>
                        </div>
                    </div>
            <?php $i++;} ?>
            
        <?php $g++; }}if($g == 0){?>
            <div class="col-md-offset-1 col-md-10 col-sm-offset-1 col-sm-10 col-xs-offset-1 col-xs-10">
            <div class="post-objeto  none-post">

                <div class="media-body">
                        <h4 class="media-heading"></h4>
                    <p><img style="max-width:95%; margin-left: 5%;" src="<?php echo base_url(); ?>assets/img/aviso-postagem.png"></p>

                    <!-- <button class="btn-shyme-avaliar">-</button> -->
                    <span class="span-tipo-post">:(</span>
                </div>
            </div>

        <?php } ?>