<footer class="footer-shyme">
	<div class="col-lg-offset-3 col-md-offset-3 col-sm-offset-1 col-xs-offset-1 col-lg-3 col-md-3 col-sm-4 col-xs-4 ">
		<a alt="logo" href="<?php echo base_url() ."index.php/perfil"; ?>"><img src="<?php echo asset_url(); ?>/img/logo-shyme-branco.png" alt=""></a>
	</div>
	<div class="col-lg-4 col-md-4 col-sm-7 col-xs-7 ">
		<h4>Links Importantes</h4>
		<ul>
			<li><a href="https://www.sigacentropaulasouza.com.br/aluno/login.aspx" target="blank">SIGA</a></li>
			<li><a href="http://www.fatecpg.com.br/default.aspx" target="blank">Intranet</a></li>
			<li><a href="<?php echo base_url() ."index.php/manual"; ?>">Manual</a></li>
			<li><a href="<?php echo base_url() ."index.php/perfil/notas"; ?>">Notas & Faltas</a></li>
		</ul>
	</div>
</footer>


<div class="modal" id="modalimagem" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" style="width: 50% !important;" role="document">
        <div class="modal-content">
            <div style="padding: 0px !important" class="modal-body">
            	<img style="width: 100%" src="">
            </div>
              
        </div> <!-- Modal content -->
    </div><!-- Modal dialog -->
</div>

	<script src="<?php echo asset_url(); ?>js/jquery.min.js"></script>
    <script src="<?php echo asset_url(); ?>js/bootstrap.min.js"></script>
    <script src="<?php echo asset_url(); ?>js/main.js"></script>
	<script src="<?php echo asset_url(); ?>js/grupo.js"></script>
	<script src="<?php echo asset_url(); ?>js/notificacao.js"></script>
	<script src="<?php echo asset_url(); ?>js/parceiros.js"></script>
</body>
</html>