<!DOCTYPE html>
<html>
<head>
	<title><?php echo $nome_da_pagina; ?></title>
	
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">

    <link rel="stylesheet" href="<?php echo asset_url(); ?>css/bootstrap.min.css" />
    <link rel="stylesheet" href="<?php echo asset_url(); ?>css/main.css" />
    <link rel="stylesheet" href="<?php echo asset_url(); ?>css/modal.css" />
    <link rel="stylesheet" href="<?php echo asset_url(); ?>css/perfil.css" />
    <link rel="stylesheet" href="<?php echo asset_url(); ?>css/grupo.css" />
    <link rel="stylesheet" href="<?php echo asset_url(); ?>css/parceiros.css" />
    <link rel="stylesheet" href="<?php echo asset_url(); ?>css/notas.css" />
    <link rel="stylesheet" href="<?php echo asset_url(); ?>css/login.css" />
</head>
<body>
<?php $this->load->view("elements/header"); ?>