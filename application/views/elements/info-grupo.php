
<?php 
foreach($infoN as $i){ 
    $privacidade =  $i['ic_privado'];
    $nomeGrupo = $i['nm_grupo'];
    $img = $i['img_grupo'];
    $ds = $i['ds_grupo'];
} 
?>
    <div class="row" id="body-mobile">
        <div class="col-lg-offset-1 col-lg-10 col-md-offset-1 col-md-10  col-sm-offset-1 col-sm-10 col-xs-12 shyme-box capa" id="perfil-box">
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4">
                <div class="box-foto" id="perfil-foto">
                    <img src="<?php echo asset_url() . $img; ?>" style="border:0px;" alt="logo"> 
                    <?php if($administrador){ ?>
                    <div class="alterar-imagem" data-alterarimagem="hide">
                        <a href="#" data-toggle="modal" data-target="#myModal">Alterar Imagem</a>
                    </div>
                    <?php } ?>
                </div>
            </div>

            <!-- Modal -->
            <div class="modal" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" name="img-perfil" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Upload de foto - <?php echo $nomeGrupo; ?></h4>
                        </div>
                        <div class="modal-body">
                              <?php include("modals/upload-foto-perfil.php"); ?>
                        </div>
                          
                        <div class="modal-footer">
                            <input class="btn-modal" type="submit" value="Salvar"/>
                          </form>  
                        </div>
                    </div> <!-- Modal content -->
                </div><!-- Modal dialog -->
            </div>
            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-8 no-padd shyme-info">
                <div id="perfil-info">
                    <p class="titulo"><?php echo $nomeGrupo; ?></p>
                    <p class="descricao"><?php echo $ds; ?></p>
                    <p class="acoes-grupo">
                    <?php if($administrador){ ?>
                        <span id="excluirGrupo">Excluir Grupo</span>
                    <?php } ?>
                    <?php if($solicitacao){ ?>
                        <span id="sairGrupo">Sair do Grupo</span>
                    <?php } else{ ?>
                        <span id="solicitacaoGrupo">Enviar Solicitação</span>
                    <?php } ?>
                    </p>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-offset-3 col-lg-6 col-md-offset-3 col-md-6  col-sm-offset-3 col-sm-6 col-xs-12">
            <div class="navegacao-grupo">
                <a href="<?php echo base_url() . 'index.php/Grupo?id=' . $_GET['id']; ?>">
                    <div <?php if($pagina == 'post') echo 'id="selected"'; ?> style="margin-right: 8%">Postagens do Grupo</div>
                </a>
                <a  href="<?php echo base_url() . 'index.php/Grupo?id=' . $_GET['id'] . '&pg=2'; ?>">
                    <div <?php if($pagina == 'membros') echo 'id="selected"'; ?>>Membros</div>
                </a>
            </div>
        </div>
        <div class="col-lg-3 col-md-3"></div>
</div>