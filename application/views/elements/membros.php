<style type="text/css">
    .matched{
        width:60px;
        display: inline-block;
        height: 80px;
        position: relative;
    }
    .matched img{
        width: 80% !important;
        height: auto !important;
        margin-left: 10%; 
    }
    
    .matched p{
        font-size: 10px;
        opacity: 0;
        margin: 0 auto;
        text-align: center;
        transition: 1s;
    }
    
    .matched span{
        position: absolute;
        top: 0px;
        right: 0px;
        opacity: 0;
        font-size: 10px;
        color: #FFF;
        background-color: red;
        border-radius: 10px;
        padding: 3px 7px;
        transition: 1s;

    }
    .matched:hover span{
        opacity: 1;
        cursor: pointer;
    }
    .matched:hover p{
        opacity: 0.7;
    }
    #limpar-busca{
        opacity: 0;
        cursor: pointer;
    }
    #limpar-busca:hover{
        opacity: 0.8;
    }
    .area-busca:hover #limpar-busca{
            opacity: 0.8;
    }
</style>

<section>
    <div class="row titulo">
        <div class="col-md-offset-1 col-md-10">
            <h3>Membros</h3>
            <!-- <alteracao> -->

            <div id="loading" style="width: 70px; margin: 0 auto; height: 70px;display: none;">
                <img style="width: 100%;" src="<?php echo asset_url(); ?>img/loading.gif" alt="Carregando..."> 
            </div>
            <div class="col-md-6 col-sm-6">
            <?php if($administrador): ?>
            <button id="bt_add_membro" class="btn btn-shyme-default" data-toggle="modal" data-target="#myModal3">
                <span class="glyphicons glyphicons-user-add"></span>Adicionar Membro
            </button>
            <?php endif; ?>
            <!-- Modal -->
            <div class="modal" id="myModal3" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Adicionar ao grupo</h4>
                        </div>
                        <div class="modal-body">
                            <?php include("modals/add_membro.php"); ?>
                        </div>
                    </div>
                    <!-- Modal content -->
                </div>
                <!-- Modal dialog -->
            </div>
            <!-- modal -->

            <!-- </alteracao> -->
            <?php if($administrador): ?>
            <button id="bt_tornar_adm" class="btn btn-shyme-default" data-toggle="modal" data-target="#myModal4">
                Nomear Administrador
            </button>
        <?php endif; ?>
            <div class="modal" id="myModal4" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Nomear Administrador</h4>
                        </div>
                        <div class="modal-body">
                            <?php include("modals/tornar-adm.php"); ?>
                        </div>
                    </div>
                    <!-- Modal content -->
                </div>
                <!-- Modal dialog -->
            </div>
            <!-- modal -->
            </div>
            <div class="col-md-6 col-sm-6 area-busca">     
                <form method="POST">
                <span id="limpar-busca" class="glyphicon glyphicon-remove"></span>
                  <input type="text" name="" id="campo-busca-membro"" class="campo-busca-membro">
                  <input type="submit" name="" id='busca-membro' value="Buscar">
                </form>
            </div>
        </div>
    </div>

    <div class="row">
        <div id="lista-membros-grupo" class="col-md-offset-1 col-md-10 conteudo">
       <div id="load-membros">
       <?php  foreach($membros as $membro){ ?>.name_usr
            

            <div id="aluno-<?php echo $membro['cd_matricula']; ?>" class="post-objeto membro-objeto col-md-4">

            <p style="display: none;" class="busca-nome-membro"> 
                <?php echo strtolower($membro['nm_aluno']); ?>        
            </p>
             <?php if($administrador &&  $membro['cd_matricula'] != $_SESSION['id']): ?>         
                <div class="close remover-membro"><span onclick="removerMembro(<?php echo $membro['cd_matricula']; ?>)" class="glyphicon glyphicon-remove"></div>
            <?php endif; ?>
            <?php if($membro['adm_aluno_grupo'] !== null ){ ?>
                <img class="crown-adm" src="<?php echo asset_url(); ?>img/crown.png" style="height:40px;width: 40px;position:absolute;top:4px;left:-6px;"/>
                <?php } ?>
                <div class="media-left">
                    <img class="media-object foto-membro" src="<?php echo asset_url() . $membro['img_aluno']; ?>" alt="Icone usuario">
                </div>
                <div class="media-body">
                    <a href="PerfilAluno?ldp=<?php echo $membro['perfil_aluno'] ;?>">
                        <h4 class="media-heading name_usr"><?php echo $membro['nm_aluno']; ?></h4>
                    </a>
                    <!-- banco -->
                </div>
                <div class="media-right rank">
                    <h4 class="media-heading"></h4>
                    <!-- Banco -->
                </div>
            </div>
            
            <?php } ?>
            <!-- Fim do loop -->
            </div>
            </div>
        </div>
    </div>
</section>