<div class="col-lg-offset-1 col-lg-3 col-md-offset-1 col-md-3  col-sm-offset-1 col-sm-10 col-xs-12 lista-grupos">

	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padd">

        <?php if($kamehameha): ?>
			<a href="#" data-toggle="modal" data-target="#myModaladd">
				<span class="glyphicon glyphicon-plus grupo-acao"></span>
			</a> 
			<!-- <a href="#" data-toggle="modal" data-target="#myModal1">
				<span class="glyphicon glyphicon-cog grupo-acao"></span>
			</a> -->
        <?php endif; ?>

			<h4>Grupos </h4>	
		</div>
		<?php foreach($grupoPrimario as $res): ?>
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  shyme-box " id="grupo">
			<p><a href="<?php echo base_url() ."index.php/Grupo?id=".$res["cd_grupo"]?>"><?php echo $res['nm_grupo']; ?></a></p>	
		</div>
		<?php endforeach; ?>
			<?php foreach($grupoSecundario as $res3):?>
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  shyme-box " id="grupo">
			<p><a href="<?php echo base_url() ."index.php/Grupo?id=".$res3["cd_grupo"]?>"><?php echo $res3['nm_grupo']; ?></a></p>	
		</div>
		<?php endforeach; ?>
    </div>
</div>


<!-- Modal -->
<div class="modal" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Gerenciar Grupos</h4>
            </div>
            <div class="modal-body">
                  <?php include("modals/gerenciar-grupos.php"); ?>
            </div>
              
            <div class="modal-footer">
                <button type="button" class="btn-modal" data-dismiss="modal" id="btn-change-group">Trocar</button>
            </div>
        </div> <!-- Modal content -->
    </div><!-- Modal dialog -->
</div> <!-- modal --> 

<div class="modal" id="myModaladd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Criar Grupos</h4>
            </div>
            <div class="modal-body">
                  <?php include("modals/criar-grupo.php"); ?>
            </div>
              
            <div class="modal-footer">
                <input type="submit" class="btn-modal" name="criarGrupo" id="criar-grupo" value="Criar Grupo">
            </div>
        </div> <!-- Modal content -->
    </div><!-- Modal dialog -->
</div> <!-- modal -->    
