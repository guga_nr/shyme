<div class="row gerenciar-grupos">
  <form>
    <div class="grupo">
      <h3>Grupos Principais</h3>

        <?php 
          $i=1;
          foreach($grupoPrimario as $res2){
          if($res2['cd_grupo'] > 4 && $res2['cd_grupo'] != 11){ ?>
            <div class=" col-md-offset-1  col-md-5 col-sm-offset-1  col-sm-5 col-xs-offset-1  col-xs-5">

              <label>
                <div class="box-foto-modal">
                  <input type="radio" name="grupo" id="gp" value="<?php echo $res2['cd_grupo']; ?>" />
                  <?php if(!empty($res2['img_grupo'])): ?>
                  <img alt="logo" src="<?php echo asset_url() . $res2['img_grupo']; ?>" >
                  <?php else: ?>
                  <img alt="logo" src="<?php echo asset_url() . 'img/logo-shyme-branco.png'; ?>" >
                  <?php endif; ?>
                </div>
                <p> <?php echo $res2['nm_grupo']; ?></p>
              </label>
            </div>
        <?php $i++; ?>
        <?php } } ?>
    </div>
  <?php if($i == 2): ?>
    <div class="grupo col-md-offset-1  col-md-5 col-sm-offset-1  col-sm-5 col-xs-offset-1  col-xs-5">
    <label>
  
    <div class="box-foto-modal">
    <input type="radio" name="grupo" id="gp" value="0" />
    <img alt="logo" src="https://eueapsicologia.com.br/wp-content/uploads/2016/07/vazio.jpg">
    </div>
    <p> Campo Vazio</p>
    </label>
    </div>
    <?php endif; ?>
    </form>
    <div class="grupo">
    <h3>Grupos Secundarios</h3>
    <div class="scroll"> 
    <form>
  
    <?php 
    $i=1;
    foreach($grupoSecundario as $res){
    ?>
    <div class="  col-md-offset-1  col-md-5 col-sm-offset-1  col-sm-5 col-xs-offset-1  col-xs-5">
    <label>
    <div class="box-foto-modal">
    <input type="radio" name="grupo" id="gs" value="<?php echo $res['cd_grupo']; ?>" />
    <?php if(!empty($res['img_grupo'])): ?>
    <img alt="logo" src="<?php echo asset_url() . $res['img_grupo']; ?>">
    <?php else: ?>
    <img alt="logo" src="<?php echo asset_url() . 'img/logo-shyme-branco.png'; ?>">
    <?php endif; ?>
    </div>
    <p> <?php echo $res['nm_grupo']; ?></p>
    </label>
    </div>
    <?php $i++; ?>
    <?php } ?>
    </div>
    </div>
  </form>
</div>
