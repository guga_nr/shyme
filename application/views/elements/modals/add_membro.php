<style>
#pessoas{
    margin-top: 5px;
    border-radius: 5px;
    background-color: rgba(125, 197, 204, 0.2);
}
</style>
<div class="container-fluid">
        <div class="input-group">
            <label for="name_user">Procure por um usuário</label>
            <input class="form-control" type="text" id="name_user" name="txt_name_user"/>
            <br><button class="btn btn-shyme-default" id="btn-add-membro" name="adicionarMembro" type="submit"  data-dismiss="modal" aria-label="Close"> Adicionar </button>
        </div>
        <div class="pessoas_selecionadas">
            <p id="pessoas">
            <ul style="list-style: none;" id="pessoas-ul">
            </ul>
            <div id="matched"></div>
            </p>
        </div>
</div>