<nav class="navbar navbar-default header">
  <div class="container-fluid">
  <div class=" col-md-12  col-xs-12 col-lg-offset-1 col-lg-10 ">
  	 
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <?php if(!isset($_SESSION['id_parceiro']) && isset($_SESSION['id'])): ?>
      <a class="navbar-brand" href="<?php echo base_url() ."index.php/perfil"; ?>"><img alt="logo" src="<?php echo asset_url(); ?>/img/logo-shyme-branco.png" alt=""></a>
      <?php elseif(isset($_SESSION['id_parceiro']) && !isset($_SESSION['id'])): ?>
      <a class="navbar-brand" href="<?php echo base_url() ."index.php/parceiros"; ?>"><img alt="logo" src="<?php echo asset_url(); ?>/img/logo-shyme-branco.png" alt=""></a>
    <?php endif; ?>
    </div>

      <?php if(!isset($_SESSION['id_parceiro']) && !isset($_SESSION['id'])): ?>

      <?php else: ?>
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="colapso">
      
      <form class="navbar-form navbar-left pesquisa" action="<?php echo base_url() .'index.php/busca'; ?>" id="desktop">
        <div class="form-group barra-pesquisa">
          	<input type="text" class="form-control" name="q" placeholder="Pesquisar">
        	<button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></button>
        </div>
      </form>


      <ul class="nav navbar-nav navbar-right">
      <?php if(!isset($_SESSION['id_parceiro'])): ?>
      	<li id="logo"><a class="navbar-brand" href="<?php echo base_url() ."index.php/perfil"; ?>"><img src="<?php echo asset_url(); ?>/img/logo-shyme-branco.png" alt=""></a></li>
      	<li class="moedas"><img src="<?php echo asset_url(); ?>/img/coins.png"><p><?php echo $_SESSION['moeda']; ?></p></li>
        
        <?php $this->load->view("elements/notificacoes-header"); ?>


        <?php $pontuacao = $pontuacao->fetchAll(); ?>

        <li class="dropdow pontos">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="caret"></span><?php if(isset($pontuacao[0]['total'])) echo $pontuacao[0]['total']; else echo 0; ?>pts</a>
          <ul class="dropdown-menu" id="pontuacao">
            <li><p> Semanal </p><?php if(isset($pontuacao[0]['semanal'])) echo $pontuacao[0]['semanal']; else echo 0; ?>pts</li>
            <li><p> Mensal </p><?php if(isset($pontuacao[0]['mensal'])) echo $pontuacao[0]['mensal']; else echo 0; ?>pts</li>
          	<div id="pontuacao-total"><p> Total </p><?php if(isset($pontuacao[0]['total'])) echo $pontuacao[0]['total']; else echo 0; ?>pts</div>
          	<div id="pontos-more"><a href="#">Veja Mais</a></div>
          </ul>
        </li>
      <?php endif; ?>
        
        <li><a href="<?php echo base_url() ."index.php/logout"; ?>"><span id="itens-direita" class="glyphicon glyphicon-log-out logout"> </a></li>
      </ul>


      <form class="navbar-form navbar-left pesquisa"  action="<?php echo base_url() .'index.php/busca'; ?>"  id="mobile">
        <div class="form-group barra-pesquisa">
          	<input type="text" name="q"  class="form-control" placeholder="Pesquisar">
        	<button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></button>
        </div>
      </form>
    </div><!-- /.navbar-collapse -->
      <?php endif; ?>
  </div>
  </div><!-- /.container-fluid -->
</nav>


