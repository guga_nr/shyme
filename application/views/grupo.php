<?php $this->load->view("elements/head");  ?>


<div class="container-fluid">
<?php $this->load->view("elements/info-grupo");  ?>


        <div class="col-lg-offset-1 col-lg-10 col-md-offset-1 col-md-10 col-sm-12 col-xs-12 shyme-box" id="postagens-grupo">
        <div class="btn-posts col-md-12 col-sm-12 col-xs-12">
                <div class="col-md-2 col-sm-3 col-xs-12 ">
                    <button id="comun" onclick="comun()">Comunicado</button>
                </div>
                <div class="col-md-2 col-sm-3 col-xs-12 ">
                    <button id="duvid" onclick="duvid()">Dúvida</button>
                </div>
                <div class="col-md-2 col-sm-3 col-xs-12 ">
                    <button id="mater" onclick="mater()">Material</button>
                </div>
        </div>
        <div class="form-posts">
            <form  method="post" accept-charset="utf-8" enctype="multipart/form-data" >
                <textarea id="txt_content_post" name="txt_content_post"></textarea>
                <div class=" col-md-12 col-sm-12 col-xs-12  form-acao">
                    <div class=" col-md-3 col-sm-3 col-xs-12 ">
                        <div class="col-md-3 col-sm-6 col-xs-6">
                            <input type="file" name="imagem_post"  id="form_imagem_post" style="position: absolute; top: -1000px">
                            <span>
                                <img alt="logo" id="imagem_post" src="<?php echo asset_url(); ?>img/picture.png">
                            </span>
                        </div>
                        <div class="col-md-9 col-sm-6 col-xs-6 upload_form" >
                            <input type="file" name="material_post" id="form_material_post"  style="position: absolute; top: -1000px"> 
                            <span>
                                <img alt="logo"  id="material_post"  src="<?php echo asset_url(); ?>img/attachment.png">
                            </span>
                        </div>
                    </div>
                    <div class=" col-md-6 col-sm-6 col-xs-12 " id="radio-duvida">
                        <div class=" col-md-4 col-sm-4 col-xs-4  moeda_duvida">
                            <div>5<input type="radio" name="moeda_duvida" id="cinco" value=29></div>
                        </div>
                        <div class=" col-md-4 col-sm-4 col-xs-4  moeda_duvida">
                            <div>10<input type="radio" name="moeda_duvida" id="dez" value=30></div>
                        </div>
                        <div class=" col-md-4 col-sm-4 col-xs-4 moeda_duvida">
                            <div>15<input type="radio" name="moeda_duvida" id="quinze" value=31></div>
                        </div>
                    </div>
                    <div class=" col-md-3 col-sm-3 col-xs-12 ">
                        <button type="submit" name="post" class="btn-modal">Postar!</button>
                    </div>
                </div>
            </form>    
        </div>
        <?php 
            $qtd_postagens = 0;
            if(isset($resultadoP)):
                $tipo = '';                              
                $vl_duvida = '';                              
                $primeira = 0;
                foreach ($resultadoP as $pp) :  
                
                if($pp['tipo_postagem_cd_tipo_postagem'] == 33 || $pp['tipo_postagem_cd_tipo_postagem'] == 32)
                    $tipo = "Comunicado";
                else if($pp['tipo_postagem_cd_tipo_postagem'] == 26 || $pp['tipo_postagem_cd_tipo_postagem'] == 27){
                    if ($pp['cd_vl_duvida'] == 29)                         
                        $vl_duvida = 5;  
                    else if ($pp['cd_vl_duvida'] == 30)                        
                        $vl_duvida = 10;   
                    elseif ($pp['cd_vl_duvida'] == 31)                         
                        $vl_duvida = 15;  
                    $tipo = "Dúvida valendo: " .  $vl_duvida . " pontos";
                }
                else if($pp['tipo_postagem_cd_tipo_postagem'] >= 10 && $pp['tipo_postagem_cd_tipo_postagem'] <= 15) 
                    $tipo = "Material";
        ?>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  shyme-box postagem-perfil" <?php if($primeira == 0) echo 'id="primeira-postagem"'; ?>>
                 <?php if($pp['cd_matricula'] == $_SESSION['id'] || $administrador){ ?>
                    <a class="remover-post" href="Grupo?id=<?php echo $_GET['id']; ?>&remover=<?php echo $pp['cd_postagem']; ?>"><span class="glyphicon glyphicon-remove" "></a>
                <?php } ?>
                <div>
                    <div class="box-foto" id="last-post">
                        <img src="<?php echo asset_url()  . $pp['img_aluno']; ?>" alt="">
                    </div>
                    <p><a href="<?php echo base_url() . 'index.php/perfil/aluno/'. $pp['perfil_aluno']; ?>"><?php echo $pp['nm_aluno']; ?></a></p>
                </div>
                <div class="col-lg-11 col-md-11 col-sm-11 col-xs-12 ">
                    <p><?php echo $pp['ds_postagem']; ?></p>
                    <img class="postagem-perfil-img" src="<?php echo asset_url() . $pp['img_postagem']; ?>" alt="">
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 final-post">
                    <p>
                    <form  method="post" accept-charset="utf-8"> 
                    <?php echo  Date('d/m/y', StrToTime($pp['dt_postagem'])); ?>
                    <?php if($pp['tipo_postagem_cd_tipo_postagem'] == 26 || $pp['tipo_postagem_cd_tipo_postagem'] == 27): ?> 
                    <!-- - <a href="#" data-toggle="modal"  data-target="#ModalRespostas<?php echo $pp['cd_postagem']; ?>"><span class="shyme-box">Ver Respostas</span></a> -->
                    - <a href="<?php echo base_url() . 'index.php/duvida?id='. $pp['cd_postagem']; ?>"><span class="shyme-box">Ver Respostas</span></a>
                    <?php elseif($pp['tipo_postagem_cd_tipo_postagem'] > 9 && $pp['tipo_postagem_cd_tipo_postagem'] < 16 && $pp['cd_matricula'] != $_SESSION['id'] ): ?> 
                    -   
                        <input type="hidden" name="arquivo" value="<?php echo  $pp['arquivo_postagem']; ?>  ">
                        <input type="hidden" name="aluno_upload" value="<?php echo  $pp['cd_matricula']; ?>  ">
                        <input type="hidden" name="idPost" value="<?php echo  $pp['cd_postagem']; ?>  ">
                        <input type="submit" class="btn-shyme shyme-box" name="download_material" value="Baixar">
                    <?php endif; ?> 
                    </form>
                    </p>
                </div>
                <span class="tipo-postagem"><?php echo $tipo; ?></span>
            </div>




            <div class="modal" id="ModalRespostas<?php echo $pp['cd_postagem']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" id="modal-respostas" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Respostas</h4>
                        </div>
                        <div class="modal-body"></div>
                        <a href="<?php echo base_url() . 'index.php/duvida?id=' . $pp['cd_postagem']; ?>">Ir para dúvida</a>
                        <div class="campo-resposta" id="pergunta-modal">
                            <div class="info-post">
                                <div class="box-foto" id="last-post">
                                    <img src="<?php echo asset_url()  . $pp['img_aluno']; ?>" alt="">
                                </div>
                                <a href="<?php echo base_url() . 'index.php/perfil/aluno/'. $pp['perfil_aluno']; ?>">
                                    <?php echo $pp['nm_aluno']; ?>
                                </a>
                            </div>
                            
                            <div class="conteudo-postagem" id="respostas">
                                <p><?php echo $pp['ds_postagem']; ?></p>
                            </div>

                            <?php if(!empty($pp['img_resposta'])): ?>
                            <img src="<?php echo asset_url() . $pp['img_resposta']; ?>" alt="">
                            <?php endif; ?>
                        </div>
                        <div class="todas-respostas-modal">
                    <?php 
                        for ($i=1; $i < sizeof($respostas); $i++) : 
                            for ($j=0; $j < sizeof($respostas[$i]); $j++) : 
                                if($respostas[$i][$j]['cd_postagem'] == $pp['cd_postagem']): ?>
                          <div class="campo-resposta" <?php if($j == sizeof($respostas[$i]) - 1) echo 'id="ultima-resposta"'; ?>>
                            <div class="box-foto" id="last-post">
                                <img src="<?php echo asset_url()  . $respostas[$i][$j]['img_aluno']; ?>" alt="">
                            </div>
                                <a href="<?php echo base_url() . 'index.php/perfil/aluno/'. $respostas[$i][$j]['perfil_aluno']; ?>">
                                    <?php echo $respostas[$i][$j]['nm_aluno']; ?>
                                </a>
                            <p>
                                    <?php echo $respostas[$i][$j]['ds_resposta']; ?>
                                    <?php if(!empty($respostas[$i][$j]['img_resposta'])): ?>
                                        <img src="<?php echo asset_url() . $respostas[$i][$j]['img_resposta']; ?>" alt="">
                                    <?php endif; ?>
                            </p>
                          </div>
                    <?php       
                                endif;
                            endfor;
                        endfor;
                    ?>  
                        </div>
                        <div class="campo-responder">
                            <form id="responder" method="post" enctype="multipart/form-data" >
                                <div class="form-resposta">
                                            <textarea class="form-control" placeholder="Digite sua resposta" id="ds_resposta" name="txt_content_post" rows="3"></textarea>
                                </div>
                                <input type="hidden" name="id_duvida" value="<?php echo $pp['cd_postagem']; ?>" id="id_duvida">
                                <input type="file" class="btn-modal" name="img_resposta" id="img_resposta">
                                <button name="submitR" class="btn-modal responder" type="submit">Responder</button>
                             </form>
                        </div>
                                
                        </div>
                    </div>
                </div>


            <?php 
                    $qtd_postagens++;
                    $primeira++;
                endforeach; 
            endif;  
            if ($qtd_postagens == 0)
                echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="text-align: center;font-size: 24px;margin: 60px 0;color: #0f8086;"> Sem postagens...</div>';

            ?>


            <?php 
                $pagina = $paginacao->fetchAll();
                if(!isset($_GET['p']))
                    $_GET['p'] = 1;
                $goku = $pagina[0]["count(*)"] / 10;
                if(str_replace('.', ' ', $goku) != $goku)
                    $pagina= (int) $goku + 1;
                else
                    $pagina= (int) $goku;

                if($_GET['p'] == $pagina)
                    $proxima = (int) $_GET['p'];
                else
                    $proxima = (int) $_GET['p'] + 1;
                if($_GET['p'] == 1)
                    $anterior = (int) $_GET['p'];
                else
                    $anterior = (int) $_GET['p'] - 1;
            ?>
            <div class="paginacao-shyme">
                <?php if($pagina > 0): ?>
                <a href="<?php echo base_url() . '/index.php/Grupo?id=' . $_GET['id'] . '&p='. $anterior; ?>">
                    <span class="glyphicon glyphicon-triangle-left"></span>
                </a>
                <?php endif; ?>
                <span class="pagina-atual"><?php if(isset($_GET['p']) && $_GET['p'] > 1) echo $_GET['p']; else echo 1;?></span> de <?php if($pagina == 0) echo 1; else echo $pagina; ?>    
                <?php if($pagina > 0): ?>
                <a href="<?php echo base_url() . '/index.php/Grupo?id=' . $_GET['id'] . '&p='. $proxima; ?>">
                    <span class="glyphicon glyphicon-triangle-right"></span>
                </a>
                <?php endif; ?>
            </div>
    </div>
</div>
<?php $this->load->view("elements/footer");  ?>



