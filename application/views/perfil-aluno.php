<?php $this->load->view("elements/head");  ?>

<div class="container-fluid">
    <div class="row" id="body-mobile">
        <div class="col-lg-offset-1 col-lg-10 col-md-offset-1 col-md-10  col-sm-offset-1 col-sm-10 col-xs-12 shyme-box" id="perfil-box">

        <?php foreach ($perfil as $p): ?>

            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4">
                <div class="box-foto" id="perfil-foto">
                    <img src="<?php echo asset_url() . $p['img_aluno']; ?>" style="border:0px;" alt=""> 
                </div>
            </div>
            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-8 no-padd shyme-info">
                <div id="perfil-info">
                    <p class="titulo"><?php echo $p['nm_aluno']; ?></p>
                    <p class="descricao"><?php echo $p['curso_aluno']; ?></p>
                </div>
            </div>
        <?php endforeach; ?>
        </div>
    </div>

    <div class="row" id="body-mobile" style="margin-top: 30px">
        <?php $this->load->view("elements/side-grupos");  ?>
                <div class="col-lg-3 col-md-3  col-sm-10 col-xs-12 ultimas-postagens">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <h4>Últimas Postagens</h4>  
            </div>
        </div>
        <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12 shyme-box" style="margin: 10px 0 30px">
        <?php 
            if(isset($postPerfil)):
                $primeira = 0;
                foreach ($postPerfil as $pp) :                                 
        ?>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  shyme-box postagem-perfil" <?php if($primeira == 0) echo 'id="primeira-postagem"'; ?>>
                <h4><a href="<?php echo base_url() . 'index.php/grupo?id=' . $pp['cd_grupo']; ?>"><?php echo $pp['nm_grupo']; ?></a></h4>
                <div>
                    <div class="box-foto" id="last-post">
                        <img src="<?php echo asset_url()  . $pp['img_aluno']; ?>" alt="">
                    </div>
                    <p><a href="<?php echo base_url() . 'index.php/perfil/aluno/'. $pp['perfil_aluno']; ?>"><?php echo $pp['nm_aluno']; ?></a></p>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                    <p><?php echo $pp['ds_postagem']; ?></p>
                    <img style="width: 80%;" src="<?php echo asset_url() . $pp['img_postagem']; ?>">
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 final-post">
                    <p>
                    <?php echo  Date('d/m/y', StrToTime($pp['dt_postagem'])); ?>
                    
                    </p>
                </div>
            </div>


            <?php 
                    $primeira++;
                endforeach; 
            endif;                                
            ?>



        </div>
    </div>
</div>
            <?php  $this->load->view("elements/footer");  ?>



  <script type="text/javascript">
        
    $(document).ready(function(){
                $('#btn-change-group').click(function() {
                    var grupri = "";
                    var grusec = "";
                    //Executa Loop entre todas as Radio buttons com o name de valor
                    $('input:radio[id=gp]').each(function() {
                        //Verifica qual está selecionado
                        if ($(this).is(':checked'))
                            grupri = parseInt($(this).val());
                    })

                        $('input:radio[id=gs]').each(function() {
                        //Verifica qual está selecionado
                        if ($(this).is(':checked'))
                            grusec = parseInt($(this).val());
                    })


                    var data = "gp="+grupri+"&gs="+grusec;
                    $.ajax({
                    type: 'GET',
                    url: '../index.php/Perfil',
                    data: data,
                    dataType: 'json',
                    success: function (data) {
                        
                    }
                });


        });

    $('#btn-create-group').click(function() {
                    var privacidade = "";
                    //Executa Loop entre todas as Radio buttons com o name de valor
                     $('input:radio[id=publico]').each(function() {
                        //Verifica qual está selecionado
                        if ($(this).is(':checked'))
                            privacidade =parseInt($(this).val());
                    })

                        $('input:radio[id=privado]').each(function() {
                        //Verifica qual está selecionado
                        if ($(this).is(':checked'))
                            privacidade = parseInt($(this).val());
                    })

                    var nome1 = $('#nome_grupo').val();
                    var ds1 = $('#ds_grupo').val();
                    var data1 = "nmgrupo="+nome1+"&dsgrupo="+ds1+"&privacidade="+privacidade;
                    var url1  = '../index.php/Perfil?'+data1;
                    alert(url1);

                    $.ajax({
                    type: 'GET',
                    url: url1,
                    data: data1,
                    dataType: 'json',
                    success: function (data) {
                        
                    }
                   
        });
        });

    });



                
    </script>
</html>