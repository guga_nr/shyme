<!DOCTYPE html>
<html lang="pt-BR">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width,initial-scale=1">
        <title>Perfil - busca</title>
        <script src="../js/jquery.min.js"></script>
        <script src="../js/grupo.js"></script>
        <link rel="stylesheet" href="<?php echo asset_url(); ?>css/bootstrap.min.css" />
        <link rel="stylesheet" href="<?php echo asset_url(); ?>css/grupo.css" />

        <style type="text/css">
            
            .media-object {
                display: block;
                max-width: 50px;
                max-height: 50px;
            }

            .post-objeto {
                position: relative;
                padding: 20px;
                margin-top: 15px;
                margin-bottom: 15px;
                background-color: #EDEFF1;
                background-color: rgba(255, 255, 255, 1);
                border-radius: 7px;
                border: 1px solid #ccc;
            }
        </style>
    </head>
    <body>
        <div class="navbar navbar-fixed-top">
        <?php include("header.php"); ?>
        </div>
        <center><h3>Comentários</h3></center>
    <div class="post-objeto col-md-offset-1 col-md-10">

        <?php foreach ($curto as $cu) { ?>
        <div class="media-left">
            <img class="media-object" src="" alt="Icone usuario">
        </div>
        <div class="media-body">
                <h4 class="media-heading"><?php echo $cu['nick_spotted']; ?></h4> 
            <p><?php echo $cu['ds_postagem_spotted']; ?></p>
            <p><img src'<?php echo $cu['img_postagem_spotted']; ?>' style="max-width: 400px; max-height: auto;" ></p>
            <?php } ?>
            </div>
                <?php foreach($comentario as $c){ ?>
                    <form method="post">
                        <div class="post-resposta col-md-offset-2 col-md-10" style="margin-top:30px">
                            <div class="media-left">
                                <img class="media-object" src="<?php echo $c['img_aluno']; ?>" alt="Icone usuario">
                            </div>
                            <div class="media-body">
                                <a href="#">
                                    <h4 class="media-heading"><?php echo $c['nm_aluno']; ?></h4>
                                </a>
                                <p><?php echo $c['ds_comentario_spotted']; ?>
                                </p>
                            
                            <!-- </alteracao> -->

                            <!-- <button class="btn-shyme-avaliar">-</button> -->
                            </div>
                            <div class="media-right">
                            </div>
                        </div>
                    </form>
                   
            <?php } ?>
            <div class="col-md-offset-1 col-md-11">
                <center>
            </div>
                <div class="post-responder col-md-offset-0 col-md-12">
                    <form method="post">
                        <div class="form-group">
                                    <textarea class="form-control" placeholder="Digite seu comentario" name="txt_content_post" rows="3"></textarea>
                        </div>
                    </div>
                    
                        <button name="submitR" class="btn btn-primary" type="submit">Comentar</button>
                </form>
    </div>

    </div>


            <div class="col-md-12 " style="margin-top:50px">
                <div class="row">
                    <?php include("footer.php"); ?>
                </div>
            </div>
        </div>
    </div>
    <script src="<?php echo asset_url(); ?>js/bootstrap.min.js"></script>
    <script src="<?php echo asset_url(); ?>js/grupo.js"></script>
    </body>
</html>