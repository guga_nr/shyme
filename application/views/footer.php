<head>
    <link rel="stylesheet" href="<?php echo asset_url(); ?>css/footer.css" />
    
</head>

<footer> <!-- Aqui e a area do footer -->
            <div class="container">
                <div class="row">
                    <div id="logoFooter" class="col-md-offset-0 col-md-3 col-xs-offset-3 col-xs-9">
                        <h2 id="logo">Shyme</h2>
                    </div> <!-- Aqui e a area da logo do rodape -->
                    <div id="linksImportantes" class="col-md-offset-1 col-md-3  col-xs-6">
                        <h4>Links Importantes</h4>
                        <ul>
                            <li><a href="https://www.sigacentropaulasouza.com.br/aluno/login.aspx">SIGA</a></li>
                            <li><a href="http://www.fatecpg.com.br/default.aspx">Intranet</a></li>
                            <li><a href="Perfil/Notas">Notas</a></li>
                            <li><a href="Spotted">Spotted</a></li>
                            <li><a href="Manual">Manual</a></li>
                        </ul>
                    </div> <!-- Aqui e a area dos links importantes -->
                    <div id="redesSociais" class="col-md-3  col-xs-6">
                        <h4>Redes Sociais</h4>
                        <ul>
                            <li><a href="#">Facebook</a></li>
                            <li><a href="#">Googleplus</a></li>
                            <li><a href="#">Twitter</a></li>
                        </ul>
                    </div> <!-- Aqui e a area das redes sociais -->
                </div>
            </div>
        <div class="copyright">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <p>Shyme &copy; 2016</p>
                    </div>
                </div>
            </div>
        </div>
</footer>