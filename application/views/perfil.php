<?php $this->load->view("elements/head");  ?>

<div class="container-fluid">
	<div class="row" id="body-mobile">
		<div class="col-lg-offset-1 col-lg-10 col-md-offset-1 col-md-10  col-sm-offset-1 col-sm-10 col-xs-12 shyme-box" id="perfil-box">
			<div class="col-lg-3 col-md-3 col-sm-3 col-xs-4">
				<div class="box-foto" id="perfil-foto">
					<img  alt="perfil-foto" src="<?php echo asset_url() . $_SESSION['imagem']; ?>" style="border:0px;" alt="">	
                    <div class="alterar-imagem" data-alterarimagem="hide">
                        <a href="#" data-toggle="modal" data-target="#myModal">Alterar Imagem</a>
                    </div>
				</div>
			</div>

            <!-- Modal -->
            <div class="modal" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" name="img-perfil" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Upload foto de perfil</h4>
                        </div>
                        <div class="modal-body">
                              <?php include("elements/modals/upload-foto-perfil.php"); ?>
                        </div>
                          
                        <div class="modal-footer">
				            <input class="btn-modal" type="submit" value="Salvar"/>
				          </form>  
                        </div>
                    </div> <!-- Modal content -->
                </div><!-- Modal dialog -->
            </div>
			<div class="col-lg-9 col-md-9 col-sm-9 col-xs-8 no-padd shyme-info">
				<div id="perfil-info">
					<p class="titulo"><?php echo $_SESSION['nome']; ?></p>
					<p class="descricao"><?php echo $_SESSION['curso']; ?></p>
				</div>
			</div>
		</div>
	</div>

	<div class="row" id="body-mobile" style="margin-top: 30px">
		<?php $this->load->view("elements/side-grupos");  ?>
		
		
		<div class="col-lg-3 col-md-3  col-sm-10 col-xs-12 ultimas-postagens">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<h4>Últimas Postagens</h4>	
			</div>
		</div>
		<div class="col-lg-7 col-md-7 col-sm-12 col-xs-12 shyme-box" style="margin: 10px 0 30px">
		<?php 
	        if(isset($postPerfil)):
	        	$primeira = 0;
	            foreach ($postPerfil as $pp) :                                 
	    ?>
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  shyme-box postagem-perfil" <?php if($primeira == 0) echo 'id="primeira-postagem"'; ?>>
				<h4><a href="<?php echo base_url() . 'index.php/grupo?id=' . $pp['cd_grupo']; ?>"><?php echo $pp['nm_grupo']; ?></a></h4>
				<div>
            		<div class="box-foto" id="last-post">
            			<img src="<?php echo asset_url()  . $pp['img_aluno']; ?>" alt="">
            		</div>
					<p><a href="<?php echo base_url() . 'index.php/perfil/aluno/'. $pp['perfil_aluno']; ?>"><?php echo $pp['nm_aluno']; ?></a></p>
				</div>
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
					<p><?php echo $pp['ds_postagem']; ?></p>
                    <img style="width: 80%;" src="<?php echo asset_url() . $pp['img_postagem']; ?>">
				</div>
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 final-post">
					<p>
					<?php echo  Date('d/m/y', StrToTime($pp['dt_postagem'])); ?>
					<?php if($pp['tipo_postagem_cd_tipo_postagem'] == 26 || $pp['tipo_postagem_cd_tipo_postagem'] == 27): ?> 
					<!-- - <a href="#" data-toggle="modal"  data-target="#ModalRespostas<?php echo $pp['cd_postagem']; ?>"><span class="shyme-box">Ver Respostas</span></a> -->
					- <a href="<?php echo base_url() . 'index.php/duvida?id='. $pp['cd_postagem']; ?>"><span class="shyme-box">Ver Respostas</span></a>
					<?php elseif($pp['tipo_postagem_cd_tipo_postagem'] > 9 && $pp['tipo_postagem_cd_tipo_postagem'] < 16 && $pp['cd_matricula'] != $_SESSION['id'] ): ?> 
					- 
					<form  method="post" accept-charset="utf-8">   
                        <input type="hidden" name="arquivo" value="<?php echo  $pp['arquivo_postagem']; ?>  ">
                        <input type="hidden" name="aluno_upload" value="<?php echo  $pp['cd_matricula']; ?>  ">
                        <input type="hidden" name="idPost" value="<?php echo  $pp['cd_postagem']; ?>  ">
                        <input type="submit" class="btn-shyme shyme-box" id="btn-download" name="download_material" value="Baixar <?php echo  str_replace("img/material/upload/","",$pp['arquivo_postagem']); ?>  ">
                    </form>
					<?php endif; ?> 
					</p>
				</div>
			</div>




	    	<div class="modal" id="ModalRespostas<?php echo $pp['cd_postagem']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			    <div class="modal-dialog" id="modal-respostas" role="document">
			        <div class="modal-content">
			            <div class="modal-header">
			                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			                <h4 class="modal-title" id="myModalLabel">Respostas</h4>
			            </div>
			            <div class="modal-body"></div>
			            <div class="campo-resposta" id="pergunta-modal">
			            <div class="col-md-12 redirecionamento-duvida">
			            	<a href="<?php echo base_url() . 'index.php/duvida?id=' . $pp['cd_postagem']; ?>">Ir para a dúvida</a>
			            </div>
					        <div class="info-post">
						        <div class="box-foto" id="last-post">
			            			<img src="<?php echo asset_url()  . $pp['img_aluno']; ?>" alt="">
			            		</div>
								<a href="<?php echo base_url() . 'index.php/perfil/aluno/'. $pp['perfil_aluno']; ?>">
									<?php echo $pp['nm_aluno']; ?>
								</a>
					        </div>
							
							<div class="conteudo-postagem" id="respostas">
								<p><?php echo $pp['ds_postagem']; ?></p>
							</div>

							<?php if(!empty($pp['img_respostapostagem'])): ?>
							<img src="<?php echo asset_url() . $pp['img_postagem']; ?>" alt="">
							<?php endif; ?>
			            </div>
			            <div class="todas-respostas-modal">
			            <div class="load-respostas-modal">
	                <?php 
			            for ($i=1; $i < sizeof($respostas); $i++) : 
			                for ($j=0; $j < sizeof($respostas[$i]); $j++) : 
			                    if($respostas[$i][$j]['cd_postagem'] == $pp['cd_postagem']): ?>
			              <div class="campo-resposta" <?php if($j == sizeof($respostas[$i]) - 1) echo 'id="ultima-resposta"'; ?>>
					        <div class="box-foto" id="last-post">
		            			<img src="<?php echo asset_url()  . $respostas[$i][$j]['img_aluno']; ?>" alt="">
		            		</div>
								<a href="<?php echo base_url() . 'index.php/perfil/aluno/'. $respostas[$i][$j]['perfil_aluno']; ?>">
									<?php echo $respostas[$i][$j]['nm_aluno']; ?>
								</a>
							<p>
									<?php echo $respostas[$i][$j]['ds_resposta']; ?>
									<?php if(!empty($respostas[$i][$j]['img_resposta'])): ?>
										<img src="<?php echo asset_url() . $respostas[$i][$j]['img_resposta']; ?>" alt="">
									<?php endif; ?>
							</p>
			              </div>
					<?php   	
			        			endif;
			    			endfor;
			            endfor;
			        ?>	
			            </div>
			            </div>
				        <div class="campo-responder">
		                    <form id="responder" method="post" enctype="multipart/form-data" >
		                        <div class="form-resposta">
		                                    <textarea class="form-control ds_resposta" placeholder="Digite sua resposta" id="ds_resposta" name="ds_resposta" rows="3"></textarea>
		                        </div>
		                        <input type="hidden" name="id_duvida" value="<?php echo $pp['cd_postagem']; ?>" id="id_duvida">
		                        <input type="file" class="btn-modal" name="img_resposta" id="img_resposta">
		                        <button name="submitR" class="btn-modal responderperfil" type="submit">Responder</button>
		                     </form>
		                </div>
				            	
			            </div>
			        </div>
			    </div>


			<?php 
					$primeira++;
	        	endforeach; 
	        endif;                                
		    ?>



		</div>
	</div>
</div>
<?php $this->load->view("elements/footer");  ?>