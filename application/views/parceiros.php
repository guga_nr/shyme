<?php $this->load->view("elements/head");  ?>

            <?php 
$Sunday = Date('d/m/y', StrToTime("Next Sunday"));
$Monday = Date('d/m/y', StrToTime("Last Monday"));
$today = Date('d/m/y', StrToTime("Last Monday"));
$today = Date('D', StrToTime("Today"));
$ddate = "2017-10-25";
$date = new DateTime($ddate);
$semana =  $date->format("W");
// echo $date->format("W")."<br>";
// echo $Sunday;
// print_r($last);
            ?>

        </style>
        <div class="container-fluid">
            <div class="row" id="body-mobile">
                <div class="col-lg-offset-1 col-lg-10 col-md-offset-1 col-md-10  col-sm-offset-1 col-sm-10 col-xs-12 shyme-box" id="perfil-box">
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4">
                        <div class="box-foto" id="perfil-foto">
                            <img src="<?php echo asset_url() . $_SESSION['imagem_parceiro']; ?>" style="border:0px;" alt=""> 
                            <div class="alterar-imagem" data-alterarimagem="hide">
                                <a href="#" data-toggle="modal" data-target="#myModal">Alterar Imagem</a>
                            </div>
                        </div>
                    </div>

                    <!-- Modal -->
                    <div class="modal" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" name="img-perfil" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabel">Upload foto de perfil</h4>
                                </div>
                                <div class="modal-body">
                                      <?php include("elements/modals/upload-foto-perfil.php"); ?>
                                </div>
                                  
                                <div class="modal-footer">
                                    <input class="btn-modal" type="submit" value="Salvar"/>
                                  </form>  
                                </div>
                            </div> <!-- Modal content -->
                        </div><!-- Modal dialog -->
                    </div>
                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-8 no-padd shyme-info">
                        <div id="perfil-info">
                        <div id="editar-parceiro">
                            <p class="titulo">
                                <?php echo $_SESSION['nome_parceiro']; ?>
                                <span id="alter-name" class="glyphicon glyphicon-pencil"></span>
                                <input type="text" id="nome-form" style="display: none;">
                                <button style="display: none;" id="nome-save">Salvar</button>
                            </p>
                            <p class="descricao">
                                <?php echo $_SESSION['descricao_parceiro']; ?>
                                <span id="alter-ds" class="glyphicon glyphicon-pencil"></span>
                                <input type="text" id="ds-form" style="display: none;">
                                <button style="display: none;" id="ds-save">Salvar</button>
                            </p>
                            <p class="descricao">
                                <a href="<?php echo base_url(). 'index.php/parceiros/perfil/' .$_SESSION['perfil_parceiro']; ?>">Ir para perfil </a>
                            </p>
                        </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">


                <div id="lista-ranking" class="col-lg-offset-1 col-lg-3 col-md-offset-1 col-md-3  col-sm-offset-1 col-sm-10 col-xs-12 lista-grupos">
                    <div class="col-lg-12 col-md-12 col-sm-6 col-xs-6">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <h4>Ranking</h4>  
                        </div>
                        <?php
                            $i = 1;
                            foreach($ranking as $a): ?>
                        <div data-idalunoranking=<?php echo $a['cd_matricula']; ?> class="col-lg-12 col-md-12 col-sm-12 col-xs-12  shyme-box ranking-aluno" id="grupo">
                            <p ><a href="<?php echo base_url() .'index.php/Perfil/Aluno/'.$a['perfil_aluno']; ?>"><?php echo $i."° - ".$a['nm_aluno'].":  ".$a['soma']."pts"; ?></a></p> 

                            <div class="info-ranking-aluno col-md-12" >
                                <span style="display: none;"><?php echo $a['cd_matricula']; ?></span>
                                <div class="col-md-12 col-xs-12 membro-objeto">
                                    <div class="shyme-box membro-box" >
                                        <div class="box-foto" id="last-post">
                                            <img src="<?php echo asset_url()  . $a['img_aluno']; ?>" alt="">
                                        </div>
                                        <p><a href="<?php echo base_url() . 'index.php/perfil/aluno/'. $a['perfil_aluno']; ?>"><?php echo $a['nm_aluno']; ?></a></p>
                                        <p><?php echo $a['curso_aluno']; ?></p>
                                    </div>
                                </div>
                            </div>   
                        </div>
                        <?php 
                                $i++;
                            endforeach; ?>
                    </div>
                </div>
                



                <div class=" shyme-box col-md-7" style="margin-bottom: 30px">

                    <div style="margin-bottom: 30px; padding-bottom: 20px;" class="div-comunicado col-md-12">
                        <h3>Promoções</h3><a href="<?php echo base_url() . 'index.php/'; ?>Parceiros/publicacao"><button class="btn-modal" id="novaPromocao">Nova Promoção</button></a>
                        <?php if(isset($promocao)): ?> 
                            <div style="display: block;width: 95%; margin-top: 20px;">
                            <ul class="lista-promocoes titulo-table">
                                <li  id="dspromo">
                                    Descrição
                                <li style="width: 10%;">
                                    Data
                                </li>
                                <li style="width: 10%;">
                                    Imagem
                                </li>
                                <li  style="width: 5%;">
                                -
                                </li>
                                <li  style="width: 5%;">
                                    -
                                </li>

                            </ul>
                            <?php foreach ($promocao as $promo) : ?>
                            <ul class="lista-promocoes">
                                <li id="dspromo">
                                    <?php echo $promo['ds_promocao']; ?>
                                </li>
                                <li style="width: 10%;">
                                    <?php echo date("d/m/Y",strtotime($promo['dt_promocao'])); ?>
                                </li>
                                <li id="imagem-lista">
                                    <img src="<?php echo asset_url().  $promo['img_promocao']; ?>">
                                </li>
                                <a href="<?php echo base_url(). 'index.php/Parceiros/publicacao/' .$promo['cd_promocao'] ?>">
                                <li id="editar-promocao"">
                                    <span id="alter-promo" class="glyphicon glyphicon-pencil"></span>
                                </li>
                                </a>
                                <li id="excluir-promocao" onclick="excluirPromocao(<?php echo $promo['cd_promocao'] ?>)">
                                    X
                                </li>

                            </ul>
                            <?php  endforeach;  ?>
                            </div>
                        <?php  endif; ?>
                   
                    </div>

                    <div style="margin-bottom: 30px; padding-bottom: 20px;" class="div-comunicado col-md-12">
                        <h3>Posts</h3><a href="<?php echo base_url() . 'index.php/'; ?>Parceiros/publicacao"><button class="btn-modal" id="novaPromocao">Nova Publicação</button></a>
                        <?php if(isset($postagem)): 
                        ?>
                            <div style="display: block;width: 95%; margin-top: 20px;">
                            <ul class="lista-promocoes titulo-table">
                                <li  id="dspromo">
                                    Descrição
                                <li style="width: 10%;">
                                    Data
                                </li>
                                <li style="width: 10%;">
                                    Imagem
                                </li>
                                <li  style="width: 5%;">
                                -
                                </li>
                                <li  style="width: 5%;">
                                    -
                                </li>

                            </ul>
                            <?php foreach ($postagem as $promo) : ?>
                            <ul class="lista-promocoes">
                                <li id="dspromo">
                                    <?php echo $promo['ds_promocao']; ?>
                                </li>
                                <li style="width: 10%;">
                                    <?php echo date("d/m/Y",strtotime($promo['dt_promocao'])); ?>
                                </li>
                                <li id="imagem-lista">
                                    <img src="<?php echo asset_url().  $promo['img_promocao']; ?>">
                                </li>
                                <a href="<?php echo base_url(). 'index.php/Parceiros/publicacao/' .$promo['cd_promocao'] ?>">
                                <li id="editar-promocao"">
                                    <span id="alter-promo" class="glyphicon glyphicon-pencil"></span>
                                </li>
                                </a>
                                <li id="excluir-promocao" onclick="excluirPromocao(<?php echo $promo['cd_promocao'] ?>)">
                                    X
                                </li>

                            </ul>
                        <?php  endforeach;  ?>
                            </div>
                        <?php  endif; ?>
                   
                    </div>
                    
                    </div>
                </div>
            </div>           
        </div>

<?php $this->load->view("elements/footer");  ?>