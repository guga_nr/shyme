<!DOCTYPE html>
<html lang="pt-BR">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <?php 
    if (isset($infoN)) {
    foreach($infoN as $i){ $nomeGrupo = $i['nm_grupo']; ?>
        <title>Grupo - <?php echo $nomeGrupo; ?></title>
    <?php }}
    ?>
    <link rel="stylesheet" href="<?php echo asset_url(); ?>css/bootstrap.min.css" />
    <link rel="stylesheet" href="<?php echo asset_url(); ?>css/grupo.css" />

<body>
<div class="navbar navbar-fixed-top">
            <?php include("header.php"); ?>
        </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-offset-1 col-md-10 col-xs-12">
               <h3> <?php echo $nomeGrupo; ?></h3>
            </div>
            <div class="navbar-grupo">
                <nav class="navbar navbar-inverse  col-md-offset-1 col-md-10 col-xs-12">
                        <ul class="nav navbar-nav col-md-12 col-xs-12" >
                            
                        <li class="nav-item active col-md-6 col-xs-6" style="padding: 0px;width: 50%;float: left;">
                          <a class="nav-link" href="Grupo?id=<?php echo $_GET['id']; ?>">Postagens <span class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item col-md-6 col-xs-6" style="padding: 0px;width: 50%;float: left;">
                          <a class="nav-link " href="Grupo?id=<?php echo $_GET['id']; ?>&pg=2">Grupo</a>
                        </li>
                        </ul>
                </nav>
            <div>
                
            <section id="main-content">
                
            <?php include("elements/forms-posts.php"); ?>
            </div>
           
            <div class="row">
                <?php include ("elements/postagens.php"); ?>
            </div>

              
            </section>
        </div>
    </div>

    <div class="container-fluid footer">
        <div class="row">
            <div class="col-md-12 col-xd-12 col-lg-12 ">
                <div class="row">
                    <?php include("footer.php"); ?>
                </div>
            </div>
        </div>
    </div>
    <script src="<?php echo asset_url(); ?>js/bootstrap.min.js"></script>
    <script src="<?php echo asset_url(); ?>js/jquery.min.js"></script>
    <script src="<?php echo asset_url(); ?>js/grupo.js"></script>
</body>
</html>