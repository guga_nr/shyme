<?php $this->load->view("elements/head");  ?>

<div class="container-fluid">

<?php $this->load->view("elements/info-grupo");  ?>


	<div class="row">
		<div class="col-md-offset-1 col-md-10 col-xs-12 shyme-box">
			<div class="col-md-6 col-xs-12 btn-membro">
			<?php if($administrador): ?>
				<div class="col-md-6 col-sm-5 col-xs-12">
					<button style="width: 100%" class="btn-modal" id="bt_add_membro" data-toggle="modal" data-target="#myModal3">Adicionar Membro</button>
				</div>
			<?php endif; ?>
			<?php if($administrador): ?>
				<div class="col-md-6 col-sm-5 col-xs-12">
				<button style="width: 100%" class="btn-modal" id="bt_tornar_adm" data-toggle="modal" data-target="#myModal4">Tornar Administrador</button>
				</div>
			<?php endif; ?>
			</div>
			<div class="col-md-6 col-xs-12">
				<span id="limpa-busca">X</span>
				<div class="pesquisa-membro">
					<input type="text" id="campo-busca-membro">
					<span data-statusbuscamembro="off" id="busca-membro" class="glyphicon glyphicon-search"></span>
				</div>
			</div>
			<div  id="lista-membros-grupo" >
				<div  id="load-membros" >
					<?php  foreach($membros as $membro): ?>

					<div class="col-lg-4 col-sm-4 col-xs-12 membro-objeto" id="aluno-<?php echo $membro['cd_matricula']; ?>">
						<div class="shyme-box membro-box" >
						<?php if($membro['adm_aluno_grupo'] !== null ){ ?>
		                	<img class="crown-adm" src="<?php echo asset_url(); ?>img/crown.png" style="height:40px;width: 40px;position:absolute;top:4px;left:-6px;"/>
		                <?php } ?>

				             <?php if($administrador &&  $membro['cd_matricula'] != $_SESSION['id']): ?>         
				                <div class="remover-membro"><span onclick="removerMembro(<?php echo $membro['cd_matricula']; ?>)" class="glyphicon glyphicon-remove"></div>
				            <?php endif; ?>
			        		<div class="box-foto" id="last-post">
			        			<img src="<?php echo asset_url()  . $membro['img_aluno']; ?>" alt="">
			        		</div>
							<p><a href="<?php echo base_url() . 'index.php/perfil/aluno/'. $membro['perfil_aluno']; ?>"><?php echo $membro['nm_aluno']; ?></a></p>
							<p><?php echo $membro['curso_aluno']; ?></p>
							<p style="display: none;" class="busca-nome-membro"> 
				                <?php echo strtolower($membro['nm_aluno']); ?>        
				            </p>
						</div>
					</div>
				
					<?php  endforeach; ?>
		
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal" id="myModal3" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Adicionar ao grupo</h4>
            </div>
            <div class="modal-body">
                <?php include("elements/modals/add_membro.php"); ?>
            </div>
        </div>
        <!-- Modal content -->
    </div>
    <!-- Modal dialog -->
</div>
<div class="modal" id="myModal4" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Clique sobre o novo Administrador</h4>
                        </div>
                        <div class="modal-body">
                            <?php include("elements/modals/tornar-adm.php"); ?>
                        </div>
                    </div>
                    <!-- Modal content -->
                </div>
                <!-- Modal dialog -->
            </div>
<?php $this->load->view("elements/footer");  ?>
