<?php $this->load->view("elements/head");  ?>

<?php 
    $Sunday = Date('d/m/y', StrToTime("Last Sunday"));
    $today = date("Y-m-d");
    $ddate = date("Y-m-d");
    $date = new DateTime($ddate);
    $semana =  $date->format("W");
    // echo Date('d/m/y',StrToTime('next sunday', StrToTime($ddate)))."<br>";
    // echo $date->format("W")."<br>";
    // echo $Sunday;
    // echo date("Y-m-d", date("W",strtotime("last sunday")));
    $a = 50;
    $day = date('w');
    $week_start = date('Y-m-d', strtotime('-'.$day.' days'));
    $week_end = date('Y-m-d', strtotime('+'.(6-$day).' days'));

    $teste = date('Y-m-d', strtotime("W"));
    // echo $day . "<br>";
    // echo $week_start . "<br>";
    // echo $week_end . "<br>";
    $date = new DateTime();

    $a = $date->setISODate(2017, 50, 7);
    // print_r($a);
    $date = new DateTime($week_end);
    $semana =  $date->format("W");
    // echo $teste;
?>

<?php 

$checkexp = false;$checkagd = false;$cd = '';$ds = '';$img = '';$tipo = 0;$agendamento[2] = '';$agendamento[1] = '';$expiracao[1] = '';$expiracao[2] = '';
    if (isset($infopromo)) {
        foreach ($infopromo as $ip) {
            $cd = $ip['cd_promocao'];
            $ds = $ip['ds_promocao'];
            $img = $ip['img_promocao'];
            $tipo = $ip['ic_principal'];
            $agendamento = $ip['dt_publicacao'];
            $expiracao = $ip['dt_expiracao'];
        }
        if(!empty($expiracao)){
            $checkexp = true;
            $expiracao = explode('-', $expiracao);
        }else{
            $expiracao[1] = 1;
            $expiracao[2] = 1;
        }
        if(!empty($agendamento)){
            $checkagd = true;
            $agendamento = explode('-', $agendamento);
        }else{
            $agendamento[1] = 1;
            $agendamento[2] = 1;
        }
    }
?>
        <div class="container-fluid corpo">
<div class="row">
<div class="col-md-12 col-sm-12"><a href="<?php echo base_url() . 'index.php/'; ?>Parceiros">Voltar</a></div>
    <form method="post" enctype="multipart/form-data">
    <div class="col-md-offset-1 col-md-7 col-sm-offset-1 col-sm-7">
        <textarea cols="60" name="txt_content_post" style="    width: 100%;" rows="10"><?php echo $ds; ?></textarea> 
    </div>   
    <div class="col-md-4 col-sm-4">
        <div class="opcoes-post">
            <div class="col-md-12 col-sm-12">
                Tipo de publicação: 
                <select id="tipo" name="tipo">
                    <option value="0" <?php if($tipo == 0) echo 'selected'; ?>>Postagem normal</option>
                    <option value="1" <?php if($tipo == 1) echo 'selected'; ?>>Promoção</option>
                </select>
            </div>
            <div class="col-md-12 col-sm-12 agendamento">
                Agendamento:
                <input type="checkbox" id="atv-agendar" name="atv-agendar" value='1' <?php if($checkagd) echo 'checked'; ?>><br><br>
                <span>Mês</span> <select <?php if(!$checkagd) echo 'disabled'; ?> id="mes-agendamento" name="mes-agendamento">
                    <?php for ($i=1; $i <= 12; $i++): ?>
                    <option value="<?php echo $i; ?>" id="mes-agd-<?php echo $i; ?>"  <?php if($agendamento['1'] == $i) echo 'selected'; ?>><?php if($i < 10) echo  0 . $i; else echo $i; ?></option>
                    <?php endfor; ?>
                </select>
                <span>Dia</span> <select <?php if(!$checkagd) echo 'disabled'; ?> name="dia-agendamento" id="dia-agendamento">
                    <?php for ($i=1; $i <= 31; $i++): ?>
                    <option value="<?php echo $i; ?>" id="dia-agd-<?php echo $i; ?>" <?php if($agendamento['2'] == $i) echo 'selected'; ?>><?php if($i < 10) echo  0 . $i; else echo $i; ?></option>
                    <?php endfor; ?>
                </select> 
            </div>
            <div class="col-md-12 col-sm-12 expiracao">
                Expirar:
                <input type="checkbox" id="atv-exp"  name="atv-exp" value="1"  <?php if($checkexp) echo 'checked'; ?>><br><br>
                 
                <span>Mês</span> <select <?php if(!$checkexp) echo 'disabled'; ?> name="mes-expiracao" id="mes-expiracao">
                    <?php for ($i=1; $i <= 12; $i++): ?>
                    <option value="<?php echo $i; ?>" id="mes-exp-<?php echo $i; ?>" <?php if($expiracao['1'] == $i) echo 'selected'; ?>><?php if($i < 10) echo  0 . $i; else echo $i; ?></option>
                    <?php endfor; ?>
                </select><span>Dia</span> <select  <?php if(!$checkexp) echo 'disabled'; ?> id="dia-expiracao" name="dia-expiracao">
                    <?php for ($i=1; $i <= 31; $i++): ?>
                    <option value="<?php echo $i; ?>" id="dia-exp-<?php echo $i; ?>" <?php if($expiracao['2'] == $i) echo 'selected'; ?>><?php if($i < 10) echo  0 . $i; else echo $i; ?></option>
                    <?php endfor; ?>
                </select>
            </div>


             <div class="col-md-12 col-sm-12 promocao-div">
                 
                <span>Semana</span> 
                <select name="data-promocao">
                <?php for($i = 0; $i < 22; $i = $i + 7): 
                    $day = date('w');
                    $week_start = date('Y-m-d', strtotime('+'.( $i - $day).' days'));
                    $week_end = date('Y-m-d', strtotime('+'.($i + 6-$day).' days'));


                    $date = new DateTime($week_end);
                    $semana =  $date->format("W");

                ?>
                
                    <option value="<?php echo $week_start .'+' . $week_end; ?>" ><?php  echo date('d/m/Y', strtotime($week_start)) . ' - ' . date('d/m/Y', strtotime($week_end)); ?></option> 
                <?php endfor; ?>
                </select>            
            </div>

        </div>                
    </div>   

                <div class="col-md-4 col-sm-4 col-md-offset-8 col-sm-offset-8">
                    <div class="opcoes-post">
                        <div class="col-md-12 col-sm-12">
                            <p id="imagem-post"><a href="#" data-toggle="modal" data-target="#myModal">Definir imagem da postagem</a></p>
                            
                            <div class="modal" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" name="img-perfil" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title" id="myModalLabel">Upload imagem do post</h4>
                                    </div>
                                    <div class="modal-body">
                                          <?php include("elements/modals/upload-foto-promocao.php"); ?>
                                    </div>
                                      
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default btn-shyme-default">Fechar</button>
                                    </div>
                                </div> <!-- Modal content -->
                            </div><!-- Modal dialog -->
                        </div> <!-- modal -->

                            <!-- <input id="fileUpload" type="file" name="promocao_imagem" class="btn btn-shyme-default btn-anexar file_customizada"> -->
                            <div class="image-holder">
                            <?php if(!empty($img)): ?>
                            <span onclick='removerImagem()' class='remove-foto'>x</span>
                                <img src="<?php echo asset_url().$img; ?>">
                            <?php endif; ?>
                            
                            </div>
                        </div>                 
                    </div>                 
                </div> 

                <div class="col-md-4 col-sm-4 col-md-offset-8 col-sm-offset-8">
                    <input type="submit" id="postar-promocao" name="postar-promocao" class="btn btn-shyme-default btn-anexar" value="Publicar">             
                </div>                 
                </form>
            </div>
        </div><span onclick='removerImagem()' class='remove-foto'>x</span>

<?php $this->load->view("elements/footer");  ?>