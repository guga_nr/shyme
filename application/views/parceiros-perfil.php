<?php $this->load->view("elements/head");  ?>

    <?php 
    if (isset($parceiro)) {
    foreach($parceiro as $i){ 
        $nome = $i['nm_parceiro'];
        $imagem = $i['img_parceiro'];
        $perfil = $i['pefil_parceiro'];
        $ds = $i['ds_parceiro'];  

    }} ?>
     <div class="container-fluid">
    <div class="row" id="body-mobile">
        <div class="col-lg-offset-1 col-lg-10 col-md-offset-1 col-md-10  col-sm-offset-1 col-sm-10 col-xs-12 shyme-box" id="perfil-box">


            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4">
                <div class="box-foto" id="perfil-foto">
                    <img src="<?php echo asset_url() . $imagem; ?>" style="border:0px;" alt=""> 
                </div>
            </div>
            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-8 no-padd shyme-info">
                <div id="perfil-info">
                    <p class="titulo"><?php echo $nome; ?></p>
                    <p class="descricao"><?php echo $ds; ?></p>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-offset-1 col-lg-10 col-md-offset-1 col-md-10 col-sm-12 col-xs-12 shyme-box" >
        <?php $this->load->view('elements/postagens-parceiros'); ?>
    </div>
    </div>
<?php $this->load->view("elements/footer");  ?>
