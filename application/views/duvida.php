<?php $this->load->view("elements/head");  ?>

<div class="container-fluid">
    <div class="row">
        <div class="col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-12 ">
            <div class="modal-content"  style="display: table;width: 100%;">
            <?php foreach ($resultadoP as $pp): ?>
                <div class="campo-resposta" id="pergunta-modal">
                    <div class="info-post">
                        <div class="box-foto" id="last-post">
                            <img src="<?php echo asset_url()  . $pp['img_aluno']; ?>" alt="">
                        </div>
                        <a href="<?php echo base_url() . 'index.php/perfil/aluno/'. $pp['perfil_aluno']; ?>">
                            <?php echo $pp['nm_aluno']; ?>
                        </a>
                    </div>
                    
                    <div class="conteudo-postagem" id="respostas">
                        <p><?php echo $pp['ds_postagem']; ?></p>
                    </div>

                    <?php if(!empty($pp['img_resposta'])): ?>
                    <img src="<?php echo asset_url() . $pp['img_resposta']; ?>" alt="">
                    <?php endif; ?>
                </div>
            <?php endforeach; ?>
                <div class="todas-respostas-modal">
                    <div id="load-duvida">
                    <?php $i = 0; ?>
                    <?php  foreach ($resultadoR as $respostas): ?>
                    
                        <div class="campo-resposta" <?php if($respostas['ic_resposta'] ==  1) echo 'style="background: #ace3e6;border: 3px solid #0f8086;"'; ?>>
                            <div class="box-foto" id="last-post">
                                <img src="<?php echo asset_url()  . $respostas['img_aluno']; ?>" alt="">
                            </div>
                            <a href="<?php echo base_url() . 'index.php/perfil/aluno/'. $respostas['perfil_aluno']; ?>">
                                <?php echo $respostas['nm_aluno']; ?>
                            </a>
                            <p>
                            <?php echo $respostas['ds_resposta']; ?>
                            <?php if(!empty($respostas['img_resposta'])): ?>
                                <img src="<?php echo asset_url() . $respostas['img_resposta']; ?>" alt="">
                            <?php endif; ?>
                            </p>
                            <?php if($escolha == 0 && $dono == 1 && $respostas['cd_resposta_aluno'] != $_SESSION['id']): ?>
                                <button data-idrespostaescolha="<?php echo $respostas['cd_resposta']; ?>" class="btn-modal escolher">Escolhar Resposta</button>
                            <?php endif; ?>
                        </div>
                    <?php $i++; ?>  
                    <?php endforeach; ?>
                    </div> 
                    
                </div>
                    <div class="col-md-offset-1 col-md-10">
                        <div class="form-posts" style="display: table;margin: 30px 0 50px;">
                            <form  method="post" accept-charset="utf-8" enctype="multipart/form-data" >
                                <textarea id="txt_content_post" name="txt_content_post"></textarea>
                                <div class=" col-md-12 col-sm-12 col-xs-12  form-acao">
                                    <div class=" col-md-3 col-sm-3 col-xs-12 ">
                                        <div class="col-md-3 col-sm-6 col-xs-6">
                                            <input type="file" name="img_resposta"  id="form_imagem_post" style="position: absolute; top: -100px; opacity: 0">
                                            <span>
                                                <img id="imagem_post" src="<?php echo asset_url(); ?>img/picture.png">
                                            </span>
                                        </div>
                                    </div>
                                    <div class=" col-md-6 col-sm-6 col-xs-12 " id="radio-duvida"></div>
                                    <div class=" col-md-3 col-sm-3 col-xs-12 ">
                                        <button type="submit" name="submitR" class="btn-modal">Responder</button>
                                    </div>
                                </div>
                            </form>    
                        </div>
                    </div>
            </div>
        </div>
    </div>
</div>
</div>
<?php $this->load->view("elements/footer");  ?>
