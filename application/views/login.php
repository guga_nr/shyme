<?php $this->load->view("elements/head");  ?>

<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="col-md-offset-2 col-md-8 col-sm-offset-2 col-sm-8 col-xs-12 login">
                <h2 class="titulo_login">Bem-vindo ao Shyme</h2>
                <?php if($tela == 'login'): ?>

                    <div class="col-md-12  nome-alunofake">
                        <form method="POST">
                            <input type="hidden"  name="txt_login" id="idfake" value="">
                            <input type="submit" class="btn-modal btn-logar" id='loginfake' value="logar" name="submit">
                        </form>
                    </div>
                            <a href="<?php echo base_url(). 'index.php/login/siga' ; ?>"><p>Você é aluno?</p></a>

                            <a href="<?php echo base_url(). 'index.php/login/parceiros' ; ?>"><p>Você é parceiro?</p></a>
                    <div class="col-md-12 shyme-box nome-alunofake">
                    <p></p>
                    </div>
                    <ul class="lista-fake">
                <?php foreach ($alunosfake as $a): ?>
                    <li>
                        <img class='imagemfake' src="<?php echo asset_url() . $a['img_aluno']; ?>" alt="<?php echo $a['nm_aluno']; ?>" data-idalunofake="<?php echo $a['cd_matricula']; ?>">
                    </li>

                <?php endforeach; ?>
                </ul>
                <?php else: ?>
                    <a href="<?php echo base_url(). 'index.php/login/' ; ?>"><p>Você não é nem parceiro. nem aluno?</p></a>
                    <form method="POST">
                        <div class="form-group">
                            <input type="text" id="txt_login" name="txt_login" placeholder="<?php if($tela == 'siga') echo 'Digite seu RG para logar com o SIGA'; else echo 'Digite seu login de parceiro' ?>" >
                        </div>
                        
                        <div class="form-group">
                            <input type="password" id="txt_senha" name="txt_senha" placeholder="Senha"/>
                        </div>
                        <?php if($tela == 'siga'): ?>
                            <a href="<?php echo base_url(). 'index.php/login/parceiros' ; ?>"><p>Você é parceiro?</p></a>
                        <?php elseif($tela == 'parceiros'): ?>    
                            <a href="<?php echo base_url(). 'index.php/login/siga' ; ?>"><p>Você é aluno?</p></a>
                        <?php endif; ?>

                        <button type="submit" class="btn-modal btn-logar" name="submit">Logar</button>
                    </form> 
                <?php endif; ?>
            </div> 
        </div> 
    </div> 
</div> 
        <script src="<?php echo asset_url(); ?>js/jquery.min.js"></script>
        <script src="<?php echo asset_url(); ?>js/bootstrap.min.js"></script>
        <script src="<?php echo asset_url(); ?>js/main.js"></script>
        <script src="<?php echo asset_url(); ?>js/grupo.js"></script>
        <script src="<?php echo asset_url(); ?>js/notificacao.js"></script>
        <script src="<?php echo asset_url(); ?>js/parceiros.js"></script>
        
    </body>
</html>