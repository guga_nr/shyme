<!DOCTYPE html>
<html lang="pt-BR">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width,initial-scale=1">
        <title>Perfil - Shyme</title>
        <script src="<?php echo asset_url(); ?>js/jquery.min.js"></script>
        <link rel="stylesheet" href="<?php echo asset_url(); ?>css/bootstrap.min.css" />
        <link rel="stylesheet" href="<?php echo asset_url(); ?>css/perfil.css" />
       
    </head>
    <body>
        <div class="container-fluid corpo" >
        <div class="navbar navbar-fixed-top">
            <?php $this->load->view("header"); 
            ?>
        </div>
            
            <div class="row">
                <div class="col-md-offset-1 col-md-10 base perfil_top">
                    <div class="row-img ">
                        <div class=" box-img-perfil col-md-2 col-xs-2">
                                <div class="contorno-img-perfil "><img class="img-perfil "   src="<?php echo asset_url() . $_SESSION['imagem']; ?>" style="border:0px;"/></div>
                                <div class="hoverzoom">
                                    <div class="retina">
                                        <a href="#" data-toggle="modal" data-target="#myModal">Alterar Imagem</a>
                                    </div>
                                </div>
                        </div>  
                        
                        <!-- Modal -->
                        <div class="modal" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" name="img-perfil" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title" id="myModalLabel">Upload foto de perfil</h4>
                                    </div>
                                    <div class="modal-body">
                                          <?php include("elements/modals/upload-foto-perfil.php"); ?>
                                    </div>
                                      
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default btn-shyme-default">Salvar</button>
                                    </div>
                                </div> <!-- Modal content -->
                            </div><!-- Modal dialog -->
                        </div> <!-- modal -->
                        
                        <div class="col-md-6 col-xs-6 usr-info">
                            <h2 ><?php echo $_SESSION['nome']; ?> </h2> <!-- banco -->
                            <h3><?php echo $_SESSION['curso']; ?></h3>
                            <!-- banco -->
                        </div>
                        <div class="col-md-3"> 
                        <div class=" pts-usr"> 
                            <table class="table-point">
                                <tr>
                                    <th >Semanal</th>
                                    <th>Mensal</th>
                                    <th>Total</th>
                                </tr>
                                <tr>
                                    <td  id="semanal"><?php if(isset($semanal) && !empty($semanal)){ foreach($semanal as $s){ echo $s['ponto']."pts  ";} }else{ echo "0pts"; } ?></td>
                                    <td id="mensal"><?php if(isset($mensal)){ foreach($mensal as $s){ echo $s['ponto']."pts  "; }}else{ echo "0pts";} ?></td>
                                    <td onclick="a();" id="total"><?php if(isset($total)){ foreach($total as $s){ echo $s['ponto']."pts  ";} }else{ echo "0pts";} ?> </td>
                                </tr>
                            </table>
                        </div>
                        </div>

                    </div>    
            
            </div>
            <div class="row">
                <!--Alteração-->
                <div class="col-md-offset-1 col-md-2  col-xs-offset-1 col-xs-2 list-group usr-group">
                    
            <?php $this->load->view("elements/side-grupos");  ?>
                </div>
                <!--Alteração-->
                
                <!-- Modal -->
                        <div class="modal" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title" id="myModalLabel">Gerenciar Grupos</h4>
                                    </div>
                                    <div class="modal-body">
                                          <?php include("elements/modals/gerenciar-grupos.php"); ?>
                                    </div>
                                      
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default btn-shyme-default" data-dismiss="modal" id="btn-change-group">Trocar</button>
                                    </div>
                                </div> <!-- Modal content -->
                            </div><!-- Modal dialog -->
                        </div> <!-- modal --> 

                        <div class="modal" id="myModaladd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title" id="myModalLabel">Criar Grupos</h4>
                                    </div>
                                    <div class="modal-body">
                                          <?php include("elements/modals/criar-grupo.php"); ?>
                                    </div>
                                      
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default btn-shyme-default" aria-label="Close" id="btn-group">Fechar</button>
                                    </div>
                                </div> <!-- Modal content -->
                            </div><!-- Modal dialog -->
                        </div> <!-- modal -->    
                
                <div class="col-md-8">
                    <!-- comunicados -->
                    
                    
                
                    <div class="div-noticias  col-md-12">
                        <!-- Noticias -->
                         <div class="col-md-12">
                        <h3>Ultimas Postagens</h3>
                        </div>
                        <?php 
                            if(isset($postPerfil)){
                                foreach ($postPerfil as $pp) {                                 
                        ?>
                        <div class="div-noticias-post col-md-12"> 
                            <div class=" col-md-12"> 
                            <a href="Grupo?id=<?php echo $pp['cd_grupo']; ?>"><h4><?php echo $pp['nm_grupo']; ?></h4></a><a href="Perfil/aluno/<?php echo $pp['perfil_aluno'] ;?>"><h5><?php echo $pp['nm_aluno']; ?></h5></a>
                            </div>
                            <div class=" col-md-10">
                            <p><?php echo $pp['ds_postagem']; ?></p>
                            </div>
                            <span class="span-tipo-post"><?php echo $pp['tipo_postagem_cd_tipo_postagem']; ?></span>
                        </div>
                        <?php }} ?>
                        
                        
                    
                    </div>
                  <div class=" col-md-12 div-noticias">
                        <h3>Comunicados</h3>
                        <img src="<?php echo base_url(); ?>assets/img/aviso-comunicados.png" style="width:350px;">
                       <!-- 
                        <div> 
                            <div class="img-perfil-teste">
                            <img src="<?php echo str_replace("C:\\xampp\\htdocs", "", $_SESSION['imagem']); ?>">
                            </div><a href="#"><h4><?php echo'*Titulo comunicado 1*'; ?></h4></a>
                            <p>
                            

                            </p>
                        </div>
-->

                    </div> 
                </div>
            </div>    
            <h3><?php print_r($semanal); ?></h3>       

                        <?php include("footer.php"); ?>
        </div> <!-- container- fluid -->


        <script src="<?php echo asset_url(); ?>js/bootstrap.min.js"></script>
        <script src="<?php echo asset_url(); ?>js/temas.js"></script>
        
    </body>



  <script type="text/javascript">
        
    $(document).ready(function(){
                $('#btn-change-group').click(function() {
                    var grupri = "";
                    var grusec = "";
                    //Executa Loop entre todas as Radio buttons com o name de valor
                    $('input:radio[id=gp]').each(function() {
                        //Verifica qual está selecionado
                        if ($(this).is(':checked'))
                            grupri = parseInt($(this).val());
                    })

                        $('input:radio[id=gs]').each(function() {
                        //Verifica qual está selecionado
                        if ($(this).is(':checked'))
                            grusec = parseInt($(this).val());
                    })


                    var data = "gp="+grupri+"&gs="+grusec;
                    $.ajax({
                    type: 'GET',
                    url: '../index.php/Perfil',
                    data: data,
                    dataType: 'json',
                    success: function (data) {
                        
                    }
                });


        });

  $(".glyphicon-log-out").click(function(){
        window.location.href = "<?php echo base_url(); ?>/logout";
  });
    $('#btn-create-group').click(function() {
        var privacidade = 10;
        //Executa Loop entre todas as Radio buttons com o name de valor
         $('input:radio[id=publico]').each(function() {
            //Verifica qual está selecionado
            if ($(this).is(':checked'))
                privacidade =parseInt($(this).val());
        })

            $('input:radio[id=privado]').each(function() {
            //Verifica qual está selecionado
            if ($(this).is(':checked'))
                privacidade = parseInt($(this).val());
        })

        var nome1 = $('#nome_grupo').val();
        var ds1 = $('#ds_grupo').val();

        var data1 = "nmgrupo="+nome1+"&dsgrupo="+ds1+"&privacidade="+privacidade;
        var url1  = '../index.php/Perfil?'+data1;

        if(nome1 == '' || ds1 == '' || privacidade > 2){
            alert(data1);
        }else{
            $.ajax({
            type: 'GET',
            url: url1,
            data: data1,
            dataType: 'json',
            success: function (data) {
                
            }                   
            });
           window.location.reload();
        }
        });

    });



                
    </script>
</html>